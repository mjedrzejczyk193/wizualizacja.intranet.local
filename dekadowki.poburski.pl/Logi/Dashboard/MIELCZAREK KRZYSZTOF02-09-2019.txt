02-09-2019 08:10:55
{
  "Brygada": [
    {
      "Nazwa": "MIELCZAREK KRZYSZTOF",
      "Id": 17,
      "Brygadzista": "WDOWIAK ANDREAS",
      "BrygadzistaId": 0,
      "Stanowisko": 0
    }
  ],
  "DniTygodnia": [
    {
      "data_pn": null,
      "data_wt": null,
      "data_sr": null,
      "data_cz": null,
      "data_pi": null,
      "data_so": null,
      "data_ni": null,
      "od_pn": "07.00",
      "od_wt": "00.00",
      "od_sr": "00.00",
      "od_cz": "00.00",
      "od_pi": "00.00",
      "od_so": "00.00",
      "od_ni": "00.00",
      "do_pn": "17.00",
      "do_wt": "00.00",
      "do_sr": "00.00",
      "do_cz": "00.00",
      "do_pi": "00.00",
      "do_so": "00.00",
      "do_ni": "00.00",
      "przestoje_pn": null,
      "przestoje_wt": null,
      "przestoje_sr": null,
      "przestoje_cz": null,
      "przestoje_pi": null,
      "przestoje_so": null,
      "przestoje_ni": null,
      "dojazd_pn": "1.5",
      "dojazd_wt": null,
      "dojazd_sr": null,
      "dojazd_cz": null,
      "dojazd_pi": null,
      "dojazd_so": null,
      "dojazd_ni": null,
      "dieta_pn": 1,
      "dieta_wt": 0,
      "dieta_sr": 0,
      "dieta_cz": 0,
      "dieta_pi": 0,
      "dieta_so": 0,
      "dieta_ni": 0
    }
  ],
  "Tydzien": {},
  "Brygadzista": {
    "Nazwa": "MIELCZAREK KRZYSZTOF",
    "Id": 17,
    "Brygadzista": "WDOWIAK ANDREAS",
    "BrygadzistaId": 0,
    "Stanowisko": 0
  }
}
:::Tydzień:::2019.35

:::Budowa:::[839] Panattoni Opole PZ0542n
***************************************
02-09-2019 08:14:38
{
  "Brygada": [
    {
      "Nazwa": "MIELCZAREK KRZYSZTOF",
      "Id": 17,
      "Brygadzista": "WDOWIAK ANDREAS",
      "BrygadzistaId": 0,
      "Stanowisko": 0
    }
  ],
  "DniTygodnia": [
    {
      "data_pn": null,
      "data_wt": null,
      "data_sr": null,
      "data_cz": null,
      "data_pi": null,
      "data_so": null,
      "data_ni": null,
      "od_pn": "00.00",
      "od_wt": "05.30",
      "od_sr": "00.00",
      "od_cz": "00.00",
      "od_pi": "00.00",
      "od_so": "00.00",
      "od_ni": "00.00",
      "do_pn": "00.00",
      "do_wt": "19.30",
      "do_sr": "00.00",
      "do_cz": "00.00",
      "do_pi": "00.00",
      "do_so": "00.00",
      "do_ni": "00.00",
      "przestoje_pn": null,
      "przestoje_wt": null,
      "przestoje_sr": null,
      "przestoje_cz": null,
      "przestoje_pi": null,
      "przestoje_so": null,
      "przestoje_ni": null,
      "dojazd_pn": null,
      "dojazd_wt": null,
      "dojazd_sr": null,
      "dojazd_cz": null,
      "dojazd_pi": null,
      "dojazd_so": null,
      "dojazd_ni": null,
      "dieta_pn": 0,
      "dieta_wt": 1,
      "dieta_sr": 0,
      "dieta_cz": 0,
      "dieta_pi": 0,
      "dieta_so": 0,
      "dieta_ni": 0
    }
  ],
  "Tydzien": {},
  "Brygadzista": {
    "Nazwa": "MIELCZAREK KRZYSZTOF",
    "Id": 17,
    "Brygadzista": "WDOWIAK ANDREAS",
    "BrygadzistaId": 0,
    "Stanowisko": 0
  }
}
:::Tydzień:::2019.35

:::Budowa:::[803] Hillwood Krosno Odrzańskie - HE Lubuskie 2 - Umowa Nr HEKO/01/01/02/2018
***************************************
02-09-2019 08:19:09
{
  "Brygada": [
    {
      "Nazwa": "MIELCZAREK KRZYSZTOF",
      "Id": 17,
      "Brygadzista": "WDOWIAK ANDREAS",
      "BrygadzistaId": 0,
      "Stanowisko": 0
    }
  ],
  "DniTygodnia": [
    {
      "data_pn": null,
      "data_wt": null,
      "data_sr": null,
      "data_cz": null,
      "data_pi": null,
      "data_so": null,
      "data_ni": null,
      "od_pn": "00.00",
      "od_wt": "00.00",
      "od_sr": "08.30",
      "od_cz": "07.00",
      "od_pi": "07.00",
      "od_so": "00.00",
      "od_ni": "00.00",
      "do_pn": "00.00",
      "do_wt": "00.00",
      "do_sr": "17.00",
      "do_cz": "17.00",
      "do_pi": "16.00",
      "do_so": "00.00",
      "do_ni": "00.00",
      "przestoje_pn": null,
      "przestoje_wt": null,
      "przestoje_sr": null,
      "przestoje_cz": null,
      "przestoje_pi": null,
      "przestoje_so": null,
      "przestoje_ni": null,
      "dojazd_pn": null,
      "dojazd_wt": null,
      "dojazd_sr": "3",
      "dojazd_cz": "1",
      "dojazd_pi": "2.5",
      "dojazd_so": null,
      "dojazd_ni": null,
      "dieta_pn": 0,
      "dieta_wt": 0,
      "dieta_sr": 1,
      "dieta_cz": 1,
      "dieta_pi": 1,
      "dieta_so": 0,
      "dieta_ni": 0
    }
  ],
  "Tydzien": {},
  "Brygadzista": {
    "Nazwa": "MIELCZAREK KRZYSZTOF",
    "Id": 17,
    "Brygadzista": "WDOWIAK ANDREAS",
    "BrygadzistaId": 0,
    "Stanowisko": 0
  }
}
:::Tydzień:::2019.35

:::Budowa:::[831] Panattoni Tychy - Etap I [Nr projektu 23/19 BAS 3230]
***************************************
02-09-2019 08:23:58
{
  "Brygada": [
    {
      "Nazwa": "MIELCZAREK KRZYSZTOF",
      "Id": 17,
      "Brygadzista": "WDOWIAK ANDREAS",
      "BrygadzistaId": 0,
      "Stanowisko": 0
    }
  ],
  "DniTygodnia": [
    {
      "data_pn": null,
      "data_wt": null,
      "data_sr": null,
      "data_cz": null,
      "data_pi": null,
      "data_so": null,
      "data_ni": null,
      "od_pn": "00.00",
      "od_wt": "05.30",
      "od_sr": "00.00",
      "od_cz": "00.00",
      "od_pi": "00.00",
      "od_so": "00.00",
      "od_ni": "00.00",
      "do_pn": "00.00",
      "do_wt": "19.30",
      "do_sr": "00.00",
      "do_cz": "00.00",
      "do_pi": "00.00",
      "do_so": "00.00",
      "do_ni": "00.00",
      "przestoje_pn": null,
      "przestoje_wt": null,
      "przestoje_sr": null,
      "przestoje_cz": null,
      "przestoje_pi": null,
      "przestoje_so": null,
      "przestoje_ni": null,
      "dojazd_pn": null,
      "dojazd_wt": null,
      "dojazd_sr": null,
      "dojazd_cz": null,
      "dojazd_pi": null,
      "dojazd_so": null,
      "dojazd_ni": null,
      "dieta_pn": 0,
      "dieta_wt": 1,
      "dieta_sr": 0,
      "dieta_cz": 0,
      "dieta_pi": 0,
      "dieta_so": 0,
      "dieta_ni": 0
    }
  ],
  "Tydzien": {},
  "Brygadzista": {
    "Nazwa": "MIELCZAREK KRZYSZTOF",
    "Id": 17,
    "Brygadzista": "WDOWIAK ANDREAS",
    "BrygadzistaId": 0,
    "Stanowisko": 0
  }
}
:::Tydzień:::2019.35

:::Budowa:::[803] Hillwood Krosno Odrzańskie - HE Lubuskie 2 - Umowa Nr HEKO/01/01/02/2018
***************************************
02-09-2019 08:27:06
{
  "Brygada": [
    {
      "Nazwa": "MIELCZAREK KRZYSZTOF",
      "Id": 17,
      "Brygadzista": "WDOWIAK ANDREAS",
      "BrygadzistaId": 0,
      "Stanowisko": 0
    }
  ],
  "DniTygodnia": [
    {
      "data_pn": null,
      "data_wt": null,
      "data_sr": null,
      "data_cz": null,
      "data_pi": null,
      "data_so": null,
      "data_ni": null,
      "od_pn": "00.00",
      "od_wt": "00.00",
      "od_sr": "08.30",
      "od_cz": "07.00",
      "od_pi": "07.00",
      "od_so": "00.00",
      "od_ni": "00.00",
      "do_pn": "00.00",
      "do_wt": "00.00",
      "do_sr": "17.00",
      "do_cz": "17.00",
      "do_pi": "16.00",
      "do_so": "00.00",
      "do_ni": "00.00",
      "przestoje_pn": null,
      "przestoje_wt": null,
      "przestoje_sr": null,
      "przestoje_cz": null,
      "przestoje_pi": null,
      "przestoje_so": null,
      "przestoje_ni": null,
      "dojazd_pn": null,
      "dojazd_wt": null,
      "dojazd_sr": "3",
      "dojazd_cz": "1",
      "dojazd_pi": "2.5",
      "dojazd_so": null,
      "dojazd_ni": null,
      "dieta_pn": 0,
      "dieta_wt": 0,
      "dieta_sr": 1,
      "dieta_cz": 1,
      "dieta_pi": 1,
      "dieta_so": 0,
      "dieta_ni": 0
    }
  ],
  "Tydzien": {},
  "Brygadzista": {
    "Nazwa": "MIELCZAREK KRZYSZTOF",
    "Id": 17,
    "Brygadzista": "WDOWIAK ANDREAS",
    "BrygadzistaId": 0,
    "Stanowisko": 0
  }
}
:::Tydzień:::2019.35

:::Budowa:::[831] Panattoni Tychy - Etap I [Nr projektu 23/19 BAS 3230]
***************************************
