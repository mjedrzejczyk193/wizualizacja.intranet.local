﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

namespace dekadowki.poburski.pl.Models
{
    public class VerifyPhoneNumberViewModel
    {
        [Display(Name = "Code")]
        [Required]
        public string Code
        {
            get;
            set;
        }

        [Display(Name = "Phone Number")]
        [Phone]
        [Required]
        public string PhoneNumber
        {
            get;
            set;
        }

        public VerifyPhoneNumberViewModel()
        {
        }
    }
}