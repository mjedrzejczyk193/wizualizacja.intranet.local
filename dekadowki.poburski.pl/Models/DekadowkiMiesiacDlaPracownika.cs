﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace dekadowki.poburski.pl.Models
{
    //dzien - projekt - od - do - przestoje - dojazd - dieta - 100% - 150% - 200% - nocne

    public class DekadowkiMiesiacDlaPracownika
    {
        public int Id
        {
            get;
            set;
        }

        public string Data
        {
            get;
            set;
        }

        public int Dzien
        {
            get;
            set;
        }

        public string DzienTygodnia
        {
            get;
            set;
        }


        public int DzienTygodniaInt
        {
            get;
            set;
        }

        public string NazwaProjektu
        {
            get;
            set;
        }

        public string RokTydzien
        {
            get;
            set;
        }

        public string Od
        {
            get;
            set;
        }

        public string Do
        {
            get;
            set;
        }

        public string Przestoje
        {
            get;
            set;
        }

        public string Dojazd
        {
            get;
            set;
        }

        public string Dieta
        {
            get;
            set;
        }

        public string Godziny100
        {
            get;
            set;
        }

        public string Godziny150
        {
            get;
            set;
        }

        public string Godziny200
        {
            get;
            set;
        }

        public string GodzinyNocne
        {
            get;
            set;
        }

        public string Rodzaj
        {
            get;
            set;
        }

        public string Paragony
        {
            get; set;
        }
    }
}