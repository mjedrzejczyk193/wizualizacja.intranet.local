﻿using System;
using System.Runtime.CompilerServices;

namespace dekadowki.poburski.pl.Models
{
    public class Tydzien
    {
        private int id
        {
            get;
            set;
        }

        private int rok
        {
            get;
            set;
        }

        private string rokTydzien
        {
            get;
            set;
        }

        private int zablokowany
        {
            get;
            set;
        }

        public Tydzien(int id, string rokTydzien, int zablokowany, int rok)
        {
            this.id = id;
            this.rokTydzien = rokTydzien;
            this.zablokowany = zablokowany;
            this.rok = rok;
        }

        public int getId()
        {
            return this.id;
        }

        public int getRok()
        {
            return this.rok;
        }

        public string getRokTydzien()
        {
            return this.rokTydzien;
        }

        public int getZablokowany()
        {
            return this.zablokowany;
        }
    }
}