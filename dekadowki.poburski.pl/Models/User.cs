﻿using System;
using System.Runtime.CompilerServices;

namespace dekadowki.poburski.pl.Models
{
    public class User
    {
        private string Name
        {
            get;
            set;
        }

        public User(string name)
        {
            this.Name = name;
        }

        public string GetUserName()
        {
            return this.Name;
        }
    }
}