﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace dekadowki.poburski.pl.Models
{
    public static class Help
    {
        private static string connectionString;

        public static string pustyPodpis;

        static Help()
        {
            Help.connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            Help.pustyPodpis = "iVBORw0KGgoAAAANSUhEUgAAASwAAACWCAYAAABkW7XSAAAEYklEQVR4Xu3UAQkAAAwCwdm/9HI83BLIOdw5AgQIRAQWySkmAQIEzmB5AgIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlACBB1YxAJfjJb2jAAAAAElFTkSuQmCC";
        }

        private static void BlockWeek(Tydzien tydzien)
        {
            using (SqlConnection connection = new SqlConnection(Help.connectionString))
            {
                SqlCommand sqlCommand = new SqlCommand(string.Format("UPDATE AspNetLata SET zablokowany = 1 WHERE idtygodnia = {0}", tydzien.getId()), connection);
                connection.Open();
                sqlCommand.ExecuteNonQuery();
            }
        }

        internal static Tydzien GetTydzien(DateTime? dzien)
        {
            int currentWeek = Help.GetIso8601WeekOfYear(Convert.ToDateTime(dzien));
            int currentYear = Convert.ToDateTime(dzien).Year;

            var tydzien = string.Format("{0}.{1}", currentYear, currentWeek.ToString().PadLeft(2, '0'));

            Tydzien week = null;
            using (SqlConnection connection = new SqlConnection(Help.connectionString))
            {
                SqlCommand sqlCommand = new SqlCommand(string.Format("SELECT * FROM AspNetLata WHERE rokTydzien = '{0}'", tydzien), connection);
                connection.Open();
                SqlDataReader reader = sqlCommand.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        week = new Tydzien(Convert.ToInt32(reader[0]), reader[1].ToString(), Convert.ToInt32(reader[2]), Convert.ToInt32(reader[3]));
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return week;
        }

        public static void BlockWeekIfNotNeeded()
        {
            if (DateTime.Now.DayOfWeek != DayOfWeek.Monday)
            {
                List<Tydzien> tygodnie = Help.GetTygodnie(DateTime.Now.Year);
                int currentWeek = Help.GetIso8601WeekOfYear(DateTime.Now);
                int currentYear = DateTime.Now.Year;
                foreach (Tydzien tydzien in tygodnie)
                {
                    if (!(tydzien.getRokTydzien() != string.Format("{0}.{1}", currentYear, currentWeek.ToString().PadLeft(2, '0'))) || !Help.IfWeekHasCurrentMonth(tydzien))
                    {
                        break;
                    }
                    if (tydzien.getZablokowany() != 0)
                    {
                        continue;
                    }
                    Help.BlockWeek(tydzien);
                }
            }
        }

        public static akord_godziny ConvertAspNetGodzinyToAkordGodziny(AspNetGodziny godziny, int dayOfWeek, int budowa)
        {
            float single;
            float single1;
            float single2;
            float single3;
            float single4;
            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
            string data = Help.GetDayOfWeekSql(godziny.rokTydzien, dayOfWeek);
            akord_godziny result = new akord_godziny()
            {
                id_dek = (
                    from g in dc.akord_godzinies
                    orderby g.id_dek descending
                    select g.id_dek).First<int>() + 1,
                Data = new DateTime?(DateTime.Parse(data)),
                rok = new int?(godziny.rok)
            };
            DateTime dateTime = DateTime.Parse(data);
            result.miesiac = new int?(dateTime.Month);
            dateTime = DateTime.Parse(data);
            result.dzien_miesiaca = new int?(dateTime.Day);
            result.tydzien = new int?(Convert.ToInt32(godziny.rokTydzien.Substring(5, 2)));
            result.dzien_tygodnia = new int?(dayOfWeek);
            result.id_prac = new int?(godziny.pracownikID);
            result.idbudowy = new int?((godziny.budowaID != 0 ? godziny.budowaID : budowa));
            result.zatwierdzone = "N";
            result.dataZatwierdzenia = new DateTime?(DateTime.Parse("1900-01-01"));
            result.ktoZatwierdzil = new int?(-1);
            result.rokTydzien = godziny.rokTydzien;
            result.idUsterki = new int?(0);
            result.czas_wpisu = new DateTime?(DateTime.Now);
            result.rodzaj_godziny = null;
            result.dzien_wolny = (godziny.obecny != null ? godziny.obecny : "brak");
            result.dodatek = new double?(0);
            result.premia = new double?(0);
            result.premia_extra = new double?(0);
            result.akord_hmmm = new double?(0);
            result.godz_deszcz = new float?(0f);
            double doGodz = 0;
            double odGodz = 0;
            double dojazd = 0;
            double przestoje = 0;
            double razem = 0;
            double razem2 = 0;
            double paragony = 0;
            if (godziny.obecny == "brak" || godziny.obecny == null)
            {
                switch (dayOfWeek)
                {
                    case 1:
                        {

                            doGodz = Help.OptimizeForIntranet(godziny.do_pn);
                            odGodz = Help.OptimizeForIntranet(godziny.od_pn);
                            dojazd = Help.OptimizeForIntranet(godziny.dojazd_pn);
                            przestoje = Help.OptimizeForIntranet(godziny.przestoje_pn);
                            paragony = Convert.ToDouble(godziny.paragony_pn);
                            result.godz_od = new double?(odGodz);
                            result.godz_do = new double?(doGodz);
                            if (doGodz == 0 && odGodz > 0)
                            {
                                doGodz = 24;
                            }
                            razem = doGodz - odGodz - przestoje;
                            razem2 = doGodz - odGodz + dojazd - przestoje;
                            result.dojazd = new double?(dojazd);
                            result.przestoje = new double?(przestoje);
                            result.dieta = new char?((Convert.ToInt32(godziny.dieta_pn) == 1 ? '1' : '0'));
                            result.dieta_eu = new char?((Convert.ToInt32(godziny.dieta_pn) == 2 ? '1' : '0'));
                            result.dzien_wolny = godziny.obecny_pn;
                            akord_godziny nullable = result;
                            if (razem2 > 0)
                            {
                                single = (razem2 > 8 ? 8f : Convert.ToSingle(razem2));
                            }
                            else
                            {
                                single = 0f;
                            }
                            nullable.godz_norm = new float?(single);
                            result.godz_50 = new float?((razem2 > 8 ? Convert.ToSingle(razem2 - 8) : 0f));
                            result.godz_100 = new float?(0f);
                            result.godz_akord = new double?((double)((razem > 0 ? Convert.ToSingle(razem) : 0f)));
                            result.wartosc_paragonow = paragony;
                            break;
                        }
                    case 2:
                        {
                            doGodz = Help.OptimizeForIntranet(godziny.do_wt);
                            odGodz = Help.OptimizeForIntranet(godziny.od_wt);
                            dojazd = Help.OptimizeForIntranet(godziny.dojazd_wt);
                            przestoje = Help.OptimizeForIntranet(godziny.przestoje_wt);
                            paragony = Convert.ToDouble(godziny.paragony_wt);
                            result.godz_od = new double?(odGodz);
                            result.godz_do = new double?(doGodz);
                            if (doGodz == 0 && odGodz > 0)
                            {
                                doGodz = 24;
                            }
                            razem = doGodz - odGodz - przestoje;
                            razem2 = doGodz - odGodz + dojazd - przestoje;
                            result.dojazd = new double?(dojazd);
                            result.przestoje = new double?(przestoje);
                            result.dieta = new char?((Convert.ToInt32(godziny.dieta_wt) == 1 ? '1' : '0'));
                            result.dieta_eu = new char?((Convert.ToInt32(godziny.dieta_wt) == 2 ? '1' : '0'));
                            result.dzien_wolny = godziny.obecny_wt;
                            akord_godziny akordGodziny = result;
                            if (razem2 > 0)
                            {
                                single1 = (razem2 > 8 ? 8f : Convert.ToSingle(razem2));
                            }
                            else
                            {
                                single1 = 0f;
                            }
                            akordGodziny.godz_norm = new float?(single1);
                            result.godz_50 = new float?((razem2 > 8 ? Convert.ToSingle(razem2 - 8) : 0f));
                            result.godz_100 = new float?(0f);
                            result.godz_akord = new double?((double)((razem > 0 ? Convert.ToSingle(razem) : 0f)));
                            result.wartosc_paragonow = paragony;
                            break;
                        }
                    case 3:
                        {
                            doGodz = Help.OptimizeForIntranet(godziny.do_sr);
                            odGodz = Help.OptimizeForIntranet(godziny.od_sr);
                            dojazd = Help.OptimizeForIntranet(godziny.dojazd_sr);
                            przestoje = Help.OptimizeForIntranet(godziny.przestoje_sr);
                            paragony = Convert.ToDouble(godziny.paragony_sr);
                            result.godz_od = new double?(odGodz);
                            result.godz_do = new double?(doGodz);
                            if (doGodz == 0 && odGodz > 0)
                            {
                                doGodz = 24;
                            }
                            razem = doGodz - odGodz - przestoje;
                            razem2 = doGodz - odGodz + dojazd - przestoje;
                            result.dojazd = new double?(dojazd);
                            result.przestoje = new double?(przestoje);
                            result.dieta = new char?((Convert.ToInt32(godziny.dieta_sr) == 1 ? '1' : '0'));
                            result.dieta_eu = new char?((Convert.ToInt32(godziny.dieta_sr) == 2 ? '1' : '0'));
                            result.dzien_wolny = godziny.obecny_sr;
                            akord_godziny nullable1 = result;
                            if (razem2 > 0)
                            {
                                single2 = (razem2 > 8 ? 8f : Convert.ToSingle(razem2));
                            }
                            else
                            {
                                single2 = 0f;
                            }
                            nullable1.godz_norm = new float?(single2);
                            result.godz_50 = new float?((razem2 > 8 ? Convert.ToSingle(razem2 - 8) : 0f));
                            result.godz_100 = new float?(0f);
                            result.godz_akord = new double?((double)((razem > 0 ? Convert.ToSingle(razem) : 0f)));
                            result.wartosc_paragonow = paragony;
                            break;
                        }
                    case 4:
                        {
                            doGodz = Help.OptimizeForIntranet(godziny.do_cz);
                            odGodz = Help.OptimizeForIntranet(godziny.od_cz);
                            dojazd = Help.OptimizeForIntranet(godziny.dojazd_cz);
                            przestoje = Help.OptimizeForIntranet(godziny.przestoje_cz);
                            paragony = Convert.ToDouble(godziny.paragony_cz);
                            result.godz_od = new double?(odGodz);
                            result.godz_do = new double?(doGodz);
                            if (doGodz == 0 && odGodz > 0)
                            {
                                doGodz = 24;
                            }
                            razem = doGodz - odGodz - przestoje;
                            razem2 = doGodz - odGodz + dojazd - przestoje;
                            result.dojazd = new double?(dojazd);
                            result.przestoje = new double?(przestoje);
                            result.dieta = new char?((Convert.ToInt32(godziny.dieta_cz) == 1 ? '1' : '0'));
                            result.dieta_eu = new char?((Convert.ToInt32(godziny.dieta_cz) == 2 ? '1' : '0'));
                            result.dzien_wolny = godziny.obecny_cz;
                            akord_godziny akordGodziny1 = result;
                            if (razem2 > 0)
                            {
                                single3 = (razem2 > 8 ? 8f : Convert.ToSingle(razem2));
                            }
                            else
                            {
                                single3 = 0f;
                            }
                            akordGodziny1.godz_norm = new float?(single3);
                            result.godz_50 = new float?((razem2 > 8 ? Convert.ToSingle(razem2 - 8) : 0f));
                            result.godz_100 = new float?(0f);
                            result.godz_akord = new double?((double)((razem > 0 ? Convert.ToSingle(razem) : 0f)));
                            result.wartosc_paragonow = paragony;
                            break;
                        }
                    case 5:
                        {
                            doGodz = Help.OptimizeForIntranet(godziny.do_pi);
                            odGodz = Help.OptimizeForIntranet(godziny.od_pi);

                            dojazd = Help.OptimizeForIntranet(godziny.dojazd_pi);
                            przestoje = Help.OptimizeForIntranet(godziny.przestoje_pi);
                            paragony = Convert.ToDouble(godziny.paragony_pi);
                            result.godz_od = new double?(odGodz);
                            result.godz_do = new double?(doGodz);
                            if (doGodz == 0 && odGodz > 0)
                            {
                                doGodz = 24;
                            }
                            razem = doGodz - odGodz - przestoje;
                            razem2 = doGodz - odGodz + dojazd - przestoje;
                            result.dojazd = new double?(dojazd);
                            result.przestoje = new double?(przestoje);
                            result.dieta = new char?((Convert.ToInt32(godziny.dieta_pi) == 1 ? '1' : '0'));
                            result.dieta_eu = new char?((Convert.ToInt32(godziny.dieta_pi) == 2 ? '1' : '0'));
                            result.dzien_wolny = godziny.obecny_pi;
                            akord_godziny nullable2 = result;
                            if (razem2 > 0)
                            {
                                single4 = (razem2 > 8 ? 8f : Convert.ToSingle(razem2));
                            }
                            else
                            {
                                single4 = 0f;
                            }
                            nullable2.godz_norm = new float?(single4);
                            result.godz_50 = new float?((razem2 > 8 ? Convert.ToSingle(razem2 - 8) : 0f));
                            result.godz_100 = new float?(0f);
                            result.godz_akord = new double?((double)((razem > 0 ? Convert.ToSingle(razem) : 0f)));
                            result.wartosc_paragonow = paragony;
                            break;
                        }
                    case 6:
                        {
                            doGodz = Help.OptimizeForIntranet(godziny.do_so);
                            odGodz = Help.OptimizeForIntranet(godziny.od_so);
                            dojazd = Help.OptimizeForIntranet(godziny.dojazd_so);
                            przestoje = Help.OptimizeForIntranet(godziny.przestoje_so);
                            paragony = Convert.ToDouble(godziny.paragony_so);
                            result.godz_od = new double?(odGodz);
                            result.godz_do = new double?(doGodz);
                            if (doGodz == 0 && odGodz > 0)
                            {
                                doGodz = 24;
                            }
                            razem = doGodz - odGodz - przestoje;
                            razem2 = doGodz - odGodz + dojazd - przestoje;
                            result.dojazd = new double?(dojazd);
                            result.przestoje = new double?(przestoje);
                            result.dieta = new char?((Convert.ToInt32(godziny.dieta_so) == 1 ? '1' : '0'));
                            result.dieta_eu = new char?((Convert.ToInt32(godziny.dieta_so) == 2 ? '1' : '0'));
                            result.godz_norm = new float?(0f);
                            result.godz_50 = new float?((razem2 > 0 ? Convert.ToSingle(razem2) : 0f));
                            result.godz_100 = new float?(0f);
                            result.godz_akord = new double?((double)((razem > 0 ? Convert.ToSingle(razem) : 0f)));
                            result.dzien_wolny = godziny.obecny_so;
                            result.wartosc_paragonow = paragony;
                            break;
                        }
                    case 7:
                        {
                            doGodz = Help.OptimizeForIntranet(godziny.do_ni);
                            odGodz = Help.OptimizeForIntranet(godziny.od_ni);
                            dojazd = Help.OptimizeForIntranet(godziny.dojazd_ni);
                            przestoje = Help.OptimizeForIntranet(godziny.przestoje_ni);
                            paragony = Convert.ToDouble(godziny.paragony_ni);
                            result.godz_od = new double?(odGodz);
                            result.godz_do = new double?(doGodz);
                            if (doGodz == 0 && odGodz > 0)
                            {
                                doGodz = 24;
                            }
                            razem = doGodz - odGodz - przestoje;
                            razem2 = doGodz - odGodz + dojazd - przestoje;
                            result.dojazd = new double?(dojazd);
                            result.przestoje = new double?(przestoje);
                            result.dieta = new char?((Convert.ToInt32(godziny.dieta_ni) == 1 ? '1' : '0'));
                            result.dieta_eu = new char?((Convert.ToInt32(godziny.dieta_ni) == 2 ? '1' : '0'));
                            result.godz_norm = new float?(0f);
                            result.godz_50 = new float?(0f);
                            result.godz_100 = new float?((razem2 > 0 ? Convert.ToSingle(razem2) : 0f));
                            result.godz_akord = new double?((double)((razem > 0 ? Convert.ToSingle(razem) : 0f)));
                            result.dzien_wolny = godziny.obecny_ni;
                            result.wartosc_paragonow = paragony;
                            break;
                        }
                }
            }
            else
            {
                result.godz_od = new double?(0);
                result.godz_do = new double?(0);
                result.dojazd = new double?(0);
                result.przestoje = new double?(0);
                result.dieta = new char?('0');
                result.dieta_eu = new char?('0');
                result.godz_norm = new float?(0f);
                result.godz_50 = new float?(0f);
                result.godz_100 = new float?(0f);
                result.godz_akord = new double?(0);
                result.wartosc_paragonow = 0;
                //result.dzien_wolny = godziny.obecny;

                switch (dayOfWeek)
                {
                    case 1:
                        result.dzien_wolny = godziny.obecny_pn;
                        break;
                    case 2:
                        result.dzien_wolny = godziny.obecny_wt;
                        break;
                    case 3:
                        result.dzien_wolny = godziny.obecny_sr;
                        break;
                    case 4:
                        result.dzien_wolny = godziny.obecny_cz;
                        break;
                    case 5:
                        result.dzien_wolny = godziny.obecny_pi;
                        break;
                    case 6:
                        result.dzien_wolny = godziny.obecny_so;
                        break;
                    case 7:
                        result.dzien_wolny = godziny.obecny_ni;
                        break;
                }
            }
            if (result.dzien_wolny == "CH")
            {
                result.idbudowy = 12535;
            }
            if (result.dzien_wolny == "U" || result.dzien_wolny == "UO" || result.dzien_wolny == "UB" || result.dzien_wolny == "OND" || result.dzien_wolny == "ONC")
            {
                result.idbudowy = 12535;
            }
            if (string.IsNullOrEmpty(result.dzien_wolny))
            {
                result.dzien_wolny = "brak";
            }
            return result;
        }

        public static double RozliczenieParagonow(string dieta, string g100, string g150, string g200, string paragony, string projekt)
        {
            //ustalenia 16-07-2024
            var d_g100 = Convert.ToDecimal(g100);
            var d_g150 = Convert.ToDecimal(g150);
            var d_g200 = Convert.ToDecimal(g200);
            var d_paragony = !string.IsNullOrEmpty(paragony) ? Convert.ToDouble(paragony) : 0;
            var akord = d_g100 + d_g150 + d_g200;
            if (projekt != "[9001] WARSZTAT")
            {
                if (akord <= 8)
                {
                    if (d_paragony >= 15)
                    {
                        return 15;
                    }
                    else
                    {
                        return d_paragony;
                    }
                }
                else if (akord < 10)
                {
                    if (d_paragony >= 50)
                    {
                        return 50;
                    }
                    else if (d_paragony <= 22.5 && d_paragony > 0)
                    {
                        return 22.5;
                    }
                    else
                    {
                        return d_paragony;
                    }
                }
                else if (akord >= 10)
                {
                    if (d_paragony >= 80)
                    {
                        if (dieta == "PL")
                        {
                            return 80;
                        }
                        else
                        {
                            return 50;
                        }
                    }
                    else if (d_paragony <= 22.5 && akord <= 12 && d_paragony > 0)
                    {
                        return 22.5;
                    }
                    else if (d_paragony <= 45 && akord > 12 && d_paragony > 0)
                    {
                        return 45;
                    }
                    else
                    {
                        return d_paragony;
                    }
                }
            }

            return 0;
        }

        internal static int GetBudowaPoNumerze(string v)
        {
            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
            return dc.projekt.Where(p => p.numer == Convert.ToInt32(v)).FirstOrDefault().idprojektu;
        }

        private static void DeleteNotReadedAndExpired(pobiNTRANETDataContext dc)
        {
            try
            {
                int currentMonth = DateTime.Now.Month;
                int currentYear = DateTime.Now.Year;
                foreach (AspNetMessages nam in (
                    from m in dc.AspNetMessages
                    where m.przeczytane == (bool?)false
                    select m).ToList<AspNetMessages>())
                {
                    bool delete = false;
                    DateTime date = DateTime.Parse(nam.data.ToString());
                    if (date.Year == currentYear && currentMonth - date.Month > 1 || date.Year < currentYear)
                    {
                        delete = true;
                    }
                    if (!delete)
                    {
                        continue;
                    }
                    dc.AspNetMessages.DeleteOnSubmit(nam);
                    dc.SubmitChanges();
                }
            }
            catch (Exception)
            {

            }
        }

        public static void generateAdminMessages()
        {
            DateTime date = DateTime.Now;
            DateTime dateTime = new DateTime(date.Year, date.Month, 1);
            DateTime dateTime1 = dateTime.AddMonths(1).AddDays(-1);
            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
            //List<Brygadzista> listaBrygadzistow = Help.GetBrygadzistow(true);
            //List<AspNetMessages> listaWiadomosci = new List<AspNetMessages>();
            //foreach (Brygadzista brygadzista in listaBrygadzistow)
            //{
            //    AspNetMessages aspNetMessage = new AspNetMessages()
            //    {
            //        tresc = "Proszę przesłać przebieg samochodu.",
            //        przeczytane = new bool?(false),
            //        data = new DateTime?(dateTime),
            //        id_pracownika = brygadzista.GetId()
            //    };
            //    //if (dc.AspNetMessages.Any<AspNetMessages>((AspNetMessages m) => m.id_pracownika == aspNetMessage.id_pracownika && (m.data == (DateTime?)variable.firstDayOfMonth)))
            //    //{
            //    //    break;
            //    //}
            //    listaWiadomosci.Add(aspNetMessage);
            //}
            //foreach (Brygadzista brygadzista in listaBrygadzistow)
            //{
            //    AspNetMessages aspNetMessage1 = new AspNetMessages()
            //    {
            //        tresc = "Proszę przesłać przebieg samochodu.",
            //        przeczytane = new bool?(false),
            //        data = new DateTime?(dateTime1),
            //        id_pracownika = brygadzista.GetId()
            //    };
            //    //if (dc.AspNetMessages.Any<AspNetMessages>((AspNetMessages m) => m.id_pracownika == aspNetMessage1.id_pracownika && (m.data == (DateTime?)variable.lastDayOfMonth)))
            //    //{
            //    //    break;
            //    //}
            //    listaWiadomosci.Add(aspNetMessage1);
            //}
            //dc.AspNetMessages.InsertAllOnSubmit<AspNetMessages>(listaWiadomosci);
            //dc.SubmitChanges();
            Help.DeleteNotReadedAndExpired(dc);
        }

        public static List<AspNetMessages> GetAdminMessages(int id_kierownika)
        {
            return (
                from l in (new pobiNTRANETDataContext()).AspNetMessages
                where l.id_pracownika == id_kierownika && l.przeczytane == (bool?)false && (l.data < (DateTime?)DateTime.Now)
                select l).ToList<AspNetMessages>();
        }

        public static List<Brygada> GetBrygada(int id, bool calosc)
        {
            List<Brygada> brygada = new List<Brygada>();
            using (SqlConnection connection = new SqlConnection(Help.connectionString))
            {
                string queryString = string.Format("SELECT DISTINCT nazwisko + ' ' + imie as nazwa, idpracownika, CASE stanowisko WHEN 'BRYGADZISTA/MAJSTER' THEN 1 WHEN 'ZASTĘPCA MAJSTRA' THEN 2 ELSE 3 END as stanowisko FROM pracownik WHERE pracuje = 1 AND (brygadzistaID = {0} OR idpracownika = {0})  order by 3 asc, 1", id);
                if (calosc)
                {
                    queryString = "SELECT nazwisko + ' ' + imie, idpracownika FROM pracownik WHERE pracuje = 1 AND brygadzistaId <> 0 order by 1";
                }
                SqlCommand sqlCommand = new SqlCommand(queryString, connection);
                connection.Open();
                SqlDataReader reader = sqlCommand.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        string nazwa = reader[0].ToString();
                        int idpracownika = Convert.ToInt32(reader[1]);
                        brygada.Add(new Brygada(nazwa, idpracownika, Help.GetBrygadzistaNazwa(idpracownika), Help.GetBrygadzistaID(idpracownika)));
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return brygada;
        }

        public static List<Brygada> GetBrygadaZGodzin(int brygadzistaID, Budowa currentBudowa, Tydzien tydzien)
        {
            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
            IQueryable<AspNetGodziny> aspNetGodzinies =
                from q in dc.AspNetGodzinies
                where q.brygadzistaID == brygadzistaID && q.budowaID == currentBudowa.Id && q.rokTydzien == tydzien.getRokTydzien()
                select q;
            List<Brygada> brygada = new List<Brygada>();
            foreach (AspNetGodziny aspNetGodziny in aspNetGodzinies)
            {
                dekadowki.poburski.pl.Models.pracownik pracownik = (
                    from p in dc.pracownik
                    where p.idpracownika == aspNetGodziny.pracownikID
                    select p).First<dekadowki.poburski.pl.Models.pracownik>();
                brygada.Add(new Brygada(string.Concat(pracownik.nazwisko, " ", pracownik.imie), pracownik.idpracownika, Help.GetBrygadzistaNazwa(pracownik.idpracownika), Help.GetBrygadzistaID(pracownik.idpracownika)));
            }
            return brygada;
        }

        public static List<Brygada> GetBrygadaZGodzin(Budowa currentBudowa, Tydzien tydzien)
        {
            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
            IQueryable<AspNetGodziny> aspNetGodzinies =
                from q in dc.AspNetGodzinies
                where q.budowaID == currentBudowa.Id && q.rokTydzien == tydzien.getRokTydzien()
                select q;
            List<Brygada> brygada = new List<Brygada>();
            foreach (AspNetGodziny aspNetGodziny in aspNetGodzinies)
            {
                dekadowki.poburski.pl.Models.pracownik pracownik = (
                    from p in dc.pracownik
                    where p.idpracownika == aspNetGodziny.pracownikID
                    select p).First<dekadowki.poburski.pl.Models.pracownik>();
                brygada.Add(new Brygada(string.Concat(pracownik.nazwisko, " ", pracownik.imie), pracownik.idpracownika, Help.GetBrygadzistaNazwa(pracownik.idpracownika), Help.GetBrygadzistaID(pracownik.idpracownika)));
            }
            return brygada;
        }

        public static int GetBrygadzistaID(int answer)
        {
            int brygadzista = 0;
            using (SqlConnection connection = new SqlConnection(Help.connectionString))
            {
                SqlCommand sqlCommand = new SqlCommand(string.Format("SELECT brygadzistaId FROM pracownik p WHERE idpracownika = {0}", answer), connection);
                connection.Open();
                SqlDataReader reader = sqlCommand.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        brygadzista = Convert.ToInt32(reader[0]);
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return brygadzista;
        }

        public static string GetBrygadzistaNazwa(int answer)
        {
            string brygadzista = "";
            using (SqlConnection connection = new SqlConnection(Help.connectionString))
            {
                SqlCommand sqlCommand = new SqlCommand(string.Format("SELECT (SELECT nazwisko + ' ' + imie FROM pracownik WHERE idpracownika = p.brygadzistaID) FROM pracownik p WHERE idpracownika = {0}", answer), connection);
                connection.Open();
                SqlDataReader reader = sqlCommand.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        brygadzista = reader[0].ToString();
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return brygadzista;
        }

        private static int GetBrygadzistaPracownikId(int id)
        {
            return (new pobiNTRANETDataContext()).pracownik.First<pracownik>((pracownik p) => p.idpracownika == id).idpracownika;
        }

        private static string GetBrygadzistaPracownikNazwa(int id)
        {
            dekadowki.poburski.pl.Models.pracownik pracownik = (new pobiNTRANETDataContext()).pracownik.First<dekadowki.poburski.pl.Models.pracownik>((dekadowki.poburski.pl.Models.pracownik p) => p.idpracownika == id);
            return string.Concat(pracownik.imie, " ", pracownik.nazwisko);
        }

        public static List<Brygadzista> GetBrygadzistow(bool calosc)
        {
            List<Brygadzista> listaBrygadzistow = new List<Brygadzista>();
            using (SqlConnection connection = new SqlConnection(Help.connectionString))
            {
                string queryString = string.Format("SELECT nazwisko + ' ' + imie, idpracownika FROM pracownik WHERE stanowisko != 'PRACOWNIK BUDOWNLANY' and NOT EXISTS(SELECT id_intranet FROM AspNetUsers where id_intranet = pracownik.idpracownika) and pracuje = 1 order by 1", new object[0]);
                if (calosc)
                {
                    queryString = string.Format("SELECT nazwisko + ' ' + imie, idpracownika FROM pracownik WHERE stanowisko != 'PRACOWNIK BUDOWNLANY' and pracuje = 1 order by 1", new object[0]);
                }
                SqlCommand sqlCommand = new SqlCommand(queryString, connection);
                connection.Open();
                SqlDataReader reader = sqlCommand.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        listaBrygadzistow.Add(new Brygadzista(Convert.ToInt32(reader[1]), reader[0].ToString()));
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return listaBrygadzistow;
        }

        public static List<Brygadzista> GetBrygadzistowDoAkceptacji()
        {
            List<Brygadzista> listaBrygadzistow = new List<Brygadzista>();
            foreach (int l in (
                from g in (new pobiNTRANETDataContext()).AspNetGodzinies
                where g.export != 1 && g.export != -1
                select g.brygadzistaID).Distinct<int>().ToList<int>())
            {
                Brygadzista brygadzista = new Brygadzista(Help.GetBrygadzistaPracownikId(l), Help.GetBrygadzistaPracownikNazwa(l));
                listaBrygadzistow.Add(brygadzista);
            }
            return listaBrygadzistow;
        }

        public static List<Brygadzista> GetBrygadzistowZGodzin(List<AspNetGodziny> godziny)
        {
            List<Brygadzista> listaBrygadzistow = new List<Brygadzista>();
            foreach (int obj in (
                from g in godziny
                orderby Help.GetBrygadzistaPracownikNazwa(g.brygadzistaID)
                select g.brygadzistaID).Distinct<int>().ToList<int>())
            {
                listaBrygadzistow.Add(new Brygadzista(obj, Help.GetNameAndSurname(obj)));
            }
            return listaBrygadzistow;
        }

        public static Budowa GetBudowa(int id)
        {
            var pustaBudowa = new Budowa(0, "", "");
            if (id == 0)
            {
                return pustaBudowa;
            }
            dekadowki.poburski.pl.Models.projekt projekt = (new pobiNTRANETDataContext()).projekt.FirstOrDefault<dekadowki.poburski.pl.Models.projekt>((dekadowki.poburski.pl.Models.projekt p) => p.idprojektu == id);
            if (projekt == null)
            {
                return pustaBudowa;
            }
            return new Budowa(projekt.idprojektu, string.Format("[{0}] {1}", projekt.numer, projekt.nazwa), projekt.numer.ToString());
        }

        public static List<Budowa> GetBudowy()
        {
            List<Budowa> listaBudow = new List<Budowa>();
            using (SqlConnection connection = new SqlConnection(Help.connectionString))
            {
                SqlCommand sqlCommand = new SqlCommand(string.Format("SELECT idprojektu, '[' + CAST(numer AS nvarchar) + '] ' + nazwa + ' ,' + ulica AS budowa, numer, LEN(numer) as dlugosc FROM projekt WHERE LEN(numer) <> 1 ORDER BY LEN(numer), numer DESC", new object[0]), connection);
                connection.Open();
                SqlDataReader reader = sqlCommand.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        Budowa budowa = new Budowa(Convert.ToInt32(reader[0]), reader[1].ToString(), reader[2].ToString());
                        listaBudow.Add(budowa);
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return listaBudow;
        }

        public static List<Budowa> GetBudowyTrwajace()
        {
            List<Budowa> listaBudow = new List<Budowa>();
            using (SqlConnection connection = new SqlConnection(Help.connectionString))
            {
                SqlCommand sqlCommand = new SqlCommand(string.Format("SELECT idprojektu, '[' + CAST(numer AS nvarchar) + '] ' + nazwa + ' ,' + ulica AS budowa, numer, LEN(numer) as dlugosc FROM projekt WHERE LEN(numer) <> 1 and p_zakonczony <> 1 ORDER BY LEN(numer), numer DESC", new object[0]), connection);
                connection.Open();
                SqlDataReader reader = sqlCommand.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        Budowa budowa = new Budowa(Convert.ToInt32(reader[0]), reader[1].ToString(), reader[2].ToString());
                        listaBudow.Add(budowa);
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return listaBudow;
        }

        public static List<Budowa> GetBudowyZGodzin(List<AspNetGodziny> godziny)
        {
            List<Budowa> listaBudow = new List<Budowa>();
            foreach (int obj in (
                from g in godziny
                orderby Help.GetBudowa(g.budowaID).Numer.Length, Help.GetBudowa(g.budowaID).Numer descending
                select g.budowaID).Distinct<int>().ToList<int>())
            {
                if (obj == 0)
                {
                    continue;
                }
                Budowa budowa = new Budowa(obj, Help.GetNazwaBudowy(obj, true), Help.GetNumerBudowy(obj));
                listaBudow.Add(budowa);
            }
            return listaBudow;
        }

        public static string GetCurrentWeek()
        {
            int num = DateTime.Now.Month;
            string month = num.ToString();
            num = DateTime.Now.Year;
            string year = num.ToString();
            num = DateTime.Now.Day;
            string day = num.ToString();
            string currentWeek = "";
            using (SqlConnection connection = new SqlConnection(Help.connectionString))
            {
                SqlCommand sqlCommand = new SqlCommand(string.Format("SELECT rokTydzien FROM lata where rok = {0} and miesiac = {1} and dzien_miesiaca = {2} ", year, month, day), connection);
                connection.Open();
                SqlDataReader reader = sqlCommand.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        currentWeek = reader[0].ToString();
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return currentWeek;
        }

        public static string GetDzienTygodnia(int d)
        {
            switch (d)
            {
                case 1: return "Poniedziałek";
                case 2: return "Wtorek";
                case 3: return "Środa";
                case 4: return "Czwartek";
                case 5: return "Piątek";
                case 6: return "Sobota";
                default: return "Niedziela";
            }
        }

        public static int GetDzienTygodniaInt(int d)
        {
            switch (d)
            {
                case 1: return 1;
                case 2: return 2;
                case 3: return 3;
                case 4: return 4;
                case 5: return 5;
                case 6: return 6;
                default: return 7;
            }
        }

        public static string GetDayOfWeek(string week, int dayOfWeek)
        {
            DateTime now;
            string str;
            string result = "";
            using (SqlConnection connection = new SqlConnection(Help.connectionString))
            {
                SqlCommand sqlCommand = new SqlCommand(string.Format("SELECT TOP 1 data, dzien_tygodnia FROM lata WHERE rokTydzien = '{0}' and dzien_tygodnia = {1} ORDER BY dzien_tygodnia desc", week, dayOfWeek), connection);
                connection.Open();
                SqlDataReader reader = sqlCommand.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        result = reader[0].ToString();
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            try
            {
                now = DateTime.Parse(result);
                str = now.ToString("dd-MM-yyyy");
            }
            catch (Exception)
            {
                if (!string.IsNullOrEmpty(week))
                {
                    str = string.Format("31-12-{0}", week.Substring(0, 4));
                }
                else
                {
                    now = DateTime.Now;
                    str = string.Concat("31-12-", now.Year);
                }
            }
            return str;
        }

        private static string GetDayOfWeekSql(string week, int dayOfWeek)
        {
            string str;
            string result = "";
            using (SqlConnection connection = new SqlConnection(Help.connectionString))
            {
                SqlCommand sqlCommand = new SqlCommand(string.Format("SELECT TOP 1 data, dzien_tygodnia FROM lata WHERE rokTydzien = '{0}' and dzien_tygodnia = {1} ORDER BY dzien_tygodnia desc", week, dayOfWeek), connection);
                connection.Open();
                SqlDataReader reader = sqlCommand.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        result = reader[0].ToString();
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            try
            {
                str = DateTime.Parse(result).ToString("yyyy-MM-dd");
            }
            catch (Exception)
            {
                str = DateTime.Now.ToString("yyyy-MM-dd");
            }
            return str;
        }

        public static string GetDieta(int value)
        {
            if (value == 1)
            {
                return "PL";
            }
            if (value == 2)
            {
                return "EU";
            }
            return "-";
        }

        public static int GetDietaInt(string value)
        {
            if (value == "PL")
            {
                return 1;
            }
            if (value == "EU")
            {
                return 2;
            }
            return 0;
        }

        public static List<DniTygodnia> GetDniTygodnia(int liczebnosc, Tydzien aktualnyTydzien, int idusera, List<Brygada> brygada, int budowa, bool czyInnaZmiana)
        {
            List<DniTygodnia> dniTygodnias;
            try
            {
                pobiNTRANETDataContext dataContext = new pobiNTRANETDataContext();
                List<DniTygodnia> listaDniTygodnia = new List<DniTygodnia>();
                for (int i = 0; i < liczebnosc; i++)
                {
                    DniTygodnia dniTygodnia = null;
                    dniTygodnia = (!dataContext.AspNetGodzinies.Any<AspNetGodziny>((AspNetGodziny g) => g.rokTydzien == aktualnyTydzien.getRokTydzien() && g.brygadzistaID == idusera && g.pracownikID == brygada[i].Id && g.budowaID == budowa && !czyInnaZmiana) ? Help.GetUserSettings(idusera) : Help.getWeekFromDatabase(Help.PoprawGodziny(dataContext.AspNetGodzinies.First<AspNetGodziny>((AspNetGodziny g) => g.rokTydzien == aktualnyTydzien.getRokTydzien() && g.brygadzistaID == idusera && g.pracownikID == brygada[i].Id && g.budowaID == budowa))));
                    dniTygodnia.data_pn = Help.GetDayOfWeek(aktualnyTydzien.getRokTydzien(), 1);
                    dniTygodnia.data_wt = Help.GetDayOfWeek(aktualnyTydzien.getRokTydzien(), 2);
                    dniTygodnia.data_sr = Help.GetDayOfWeek(aktualnyTydzien.getRokTydzien(), 3);
                    dniTygodnia.data_cz = Help.GetDayOfWeek(aktualnyTydzien.getRokTydzien(), 4);
                    dniTygodnia.data_pi = Help.GetDayOfWeek(aktualnyTydzien.getRokTydzien(), 5);
                    dniTygodnia.data_so = Help.GetDayOfWeek(aktualnyTydzien.getRokTydzien(), 6);
                    dniTygodnia.data_ni = Help.GetDayOfWeek(aktualnyTydzien.getRokTydzien(), 7);
                    listaDniTygodnia.Add(dniTygodnia);
                }
                dniTygodnias = listaDniTygodnia;
            }
            catch (Exception)
            {
                dniTygodnias = new List<DniTygodnia>();
            }
            return dniTygodnias;
        }

        public static List<AspNetGodziny> GetGodzinyDoPogladu(string user)
        {
            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
            int idIntranetFromDb = Help.GetIdIntranetFromDb(user);
            List<AspNetGodziny> godziny = (
                from q in dc.AspNetGodzinies
                where q.brygadzistaID == idIntranetFromDb
                select q).ToList<AspNetGodziny>();
            if (Help.SprawdzCzyAdmin(user))
            {
                godziny = dc.AspNetGodzinies.ToList<AspNetGodziny>();
            }
            return (
                from q in godziny
                orderby q.rokTydzien descending, q.brygadzistaID, q.budowaID
                select q).ToList<AspNetGodziny>();
        }

        public static int GetIdIntranetFromDb(string name)
        {
            int id_intranet = 0;
            using (SqlConnection connection = new SqlConnection(Help.connectionString))
            {
                SqlCommand sqlCommand = new SqlCommand(string.Concat("SELECT id_intranet FROM AspNetUsers WHERE email = '", name, "'"), connection);
                connection.Open();
                SqlDataReader reader = sqlCommand.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        id_intranet = Convert.ToInt32(reader[0]);
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return id_intranet;
        }

        public static int GetIso8601WeekOfYear(DateTime time)
        {
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }

        public static List<int> GetLata()
        {
            List<string> list = (new pobiNTRANETDataContext()).AspNetLata.Select(l => l.rok.ToString()).Distinct().ToList();
            //(
            //from l in (new pobiNTRANETDataContext()).AspNetLata
            //select l.rok).Distinct<string>().ToList<string>();
            List<int> lista = new List<int>();
            foreach (string rok in list)
            {
                lista.Add(Convert.ToInt32(rok));
            }
            return lista;
        }

        public static string GetNameAndSurname(int id)
        {
            string nazwa = "";
            using (SqlConnection connection = new SqlConnection(Help.connectionString))
            {
                SqlCommand sqlCommand = new SqlCommand(string.Concat("SELECT nazwisko + ' ' + imie FROM pracownik WHERE idpracownika = ", id), connection);
                connection.Open();
                SqlDataReader reader = sqlCommand.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        nazwa = reader[0].ToString();
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return nazwa;
        }

        public static string GetNazwaBudowy(int id, bool calosc)
        {
            string str;
            try
            {
                pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
                str = (!calosc ? (
                    from p in dc.projekt
                    where p.idprojektu == id
                    select string.Format("[{0}] {1}{2}", (object)p.numer, p.nazwa.Substring(0, 30), (p.nazwa.Length > 30 ? "..." : ""))).First<string>().ToString() : (
                    from p in dc.projekt
                    where p.idprojektu == id
                    select string.Format("[{0}] {1}", (object)p.numer, p.nazwa)).First<string>().ToString());
            }
            catch (Exception)
            {
                str = "BUDOWA SERWISOWA";
            }
            return str;
        }

        public static string GetNumerBudowy(int id)
        {
            try
            {

                return (
                    from p in (new pobiNTRANETDataContext()).projekt
                    where p.idprojektu == id
                    select string.Format("{0}", (object)p.numer)).First<string>().ToString();
            }
            catch (Exception)
            {
                return "0";
            }
        }

        public static string GetObecny(string obecny)
        {
            if (obecny == "KR")
            {
                return "Chorobowe";
            }
            if (obecny == "FU")
            {
                return "Nieob. nieuspr.";
            }
            if (obecny == "U")
            {
                return "Urlop";
            }
            if (obecny == "UU")
            {
                return "Urlop bezpł.";
            }
            if (obecny == "SU")
            {
                return "Urlop okol.";
            }
            return "Obecny";
        }

        public static dynamic GetPodpisy(DashboardViewModel dvm, Budowa budowa)
        {
            List<string> podpisy = new List<string>();
            for (int i = 0; i < dvm.Brygada.Count; i++)
            {
                pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
                try
                {
                    podpisy.Add(dc.AspNetGodzinies.First<AspNetGodziny>((AspNetGodziny g) => g.brygadzistaID == dvm.Brygadzista.Id && g.pracownikID == dvm.Brygada[i].Id && g.rokTydzien == dvm.Tydzien.getRokTydzien() && g.budowaID == budowa.Id && !g.podpis.Equals("") && g.podpis != null).podpis);
                }
                catch (Exception)
                {
                    podpisy.Add("");
                }
            }
            return podpisy;
        }

        public static dynamic GetPodpisy(List<AspNetGodziny> godziny)
        {
            List<string> podpisy = new List<string>();
            foreach (AspNetGodziny godz in godziny)
            {
                if (string.IsNullOrEmpty(godz.podpis))
                {
                    podpisy.Add("");
                }
                else
                {
                    podpisy.Add(godz.podpis);
                }
            }
            return podpisy;
        }

        public static int GetStanowisko(int id)
        {
            int num;
            int i = 0;
            using (SqlConnection connection = new SqlConnection(Help.connectionString))
            {
                SqlCommand sqlCommand = new SqlCommand(string.Format("select stanowisko from pracownik WHERE idpracownika = {0}", id), connection);
                connection.Open();
                SqlDataReader reader = sqlCommand.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        string stanowisko = reader[0].ToString();
                        if (stanowisko == "BRYGADZISTA/MAJSTER")
                        {
                            i = 1;
                        }
                        else if (stanowisko == "ZASTĘPCA MAJSTRA")
                        {
                            i = 2;
                        }
                        else if (stanowisko == "PRACOWNIK BUDOWLANY")
                        {
                            i = 3;
                        }
                    }
                }
                finally
                {
                    reader.Close();
                }
                num = i;
            }
            return num;
        }

        public static Tydzien GetTydzien(int tydzien)
        {
            if (tydzien == 0)
            {
                return new Tydzien(0, "", 0, 0);
            }
            Tydzien week = null;
            using (SqlConnection connection = new SqlConnection(Help.connectionString))
            {
                SqlCommand sqlCommand = new SqlCommand(string.Format("SELECT * FROM AspNetLata WHERE idtygodnia = {0}", tydzien), connection);
                connection.Open();
                SqlDataReader reader = sqlCommand.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        week = new Tydzien(Convert.ToInt32(reader[0]), reader[1].ToString(), Convert.ToInt32(reader[2]), Convert.ToInt32(reader[3]));
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return week;
        }

        public static Tydzien GetTydzien(string tydzien)
        {
            Tydzien week = null;
            using (SqlConnection connection = new SqlConnection(Help.connectionString))
            {
                SqlCommand sqlCommand = new SqlCommand(string.Format("SELECT * FROM AspNetLata WHERE rokTydzien = '{0}'", tydzien), connection);
                connection.Open();
                SqlDataReader reader = sqlCommand.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        week = new Tydzien(Convert.ToInt32(reader[0]), reader[1].ToString(), Convert.ToInt32(reader[2]), Convert.ToInt32(reader[3]));
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return week;
        }

        public static List<Tydzien> GetTygodnie(int year)
        {
            List<Tydzien> listaTygodni = new List<Tydzien>();
            using (SqlConnection connection = new SqlConnection(Help.connectionString))
            {
                SqlCommand sqlCommand = new SqlCommand(string.Format("SELECT * FROM AspNetLata WHERE zablokowany <> 1 and (rok = {0} or rok = {0} - 1)", year.ToString()), connection);
                connection.Open();
                SqlDataReader reader = sqlCommand.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        Tydzien tydzien = new Tydzien(Convert.ToInt32(reader[0]), reader[1].ToString(), Convert.ToInt32(reader[2]), Convert.ToInt32(reader[3]));
                        listaTygodni.Add(tydzien);
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return listaTygodni;
        }

        public static List<Tydzien> GetTygodnieCalosc(int year)
        {
            List<Tydzien> listaTygodni = new List<Tydzien>();
            using (SqlConnection connection = new SqlConnection(Help.connectionString))
            {
                SqlCommand sqlCommand = new SqlCommand(string.Format("SELECT * FROM AspNetLata WHERE rok = {0}", year.ToString()), connection);
                connection.Open();
                SqlDataReader reader = sqlCommand.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        Tydzien tydzien = new Tydzien(Convert.ToInt32(reader[0]), reader[1].ToString(), Convert.ToInt32(reader[2]), Convert.ToInt32(reader[3]));
                        listaTygodni.Add(tydzien);
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return listaTygodni;
        }

        public static List<Tydzien> GetTygodnieZablokowaneLubNie(int year)
        {
            List<Tydzien> listaTygodni = new List<Tydzien>();
            using (SqlConnection connection = new SqlConnection(Help.connectionString))
            {
                SqlCommand sqlCommand = new SqlCommand(string.Format("SELECT * FROM AspNetLata", new object[0]), connection);
                connection.Open();
                SqlDataReader reader = sqlCommand.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        Tydzien tydzien = new Tydzien(Convert.ToInt32(reader[0]), reader[1].ToString(), Convert.ToInt32(reader[2]), Convert.ToInt32(reader[3]));
                        listaTygodni.Add(tydzien);
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return listaTygodni;
        }

        public static List<Tydzien> GetTygodnieZGodzin(List<AspNetGodziny> godziny)
        {
            List<Tydzien> listaTygodni = new List<Tydzien>();
            foreach (string list in (
                from g in godziny
                orderby g.rokTydzien
                select g.rokTydzien).Distinct<string>().ToList<string>())
            {
                listaTygodni.Add(Help.GetTydzien(list));
            }
            return listaTygodni;
        }

        public static DniTygodnia GetUserSettings(int idusera)
        {
            DniTygodnia dniTygodnium;
            DniTygodnia dniTygodnia = new DniTygodnia();
            bool sciagnieto = false;
            using (SqlConnection connection = new SqlConnection(Help.connectionString))
            {
                SqlCommand sqlCommand = new SqlCommand(string.Format("select * from AspNetUserSettings WHERE Id = {0}", idusera), connection);
                connection.Open();
                SqlDataReader reader = sqlCommand.ExecuteReader();
                try
                {
                    try
                    {
                        while (reader.Read())
                        {
                            sciagnieto = true;
                            dniTygodnia.od_pn = Help.IsEmptyOd(reader[1].ToString());
                            dniTygodnia.od_wt = Help.IsEmptyOd(reader[2].ToString());
                            dniTygodnia.od_sr = Help.IsEmptyOd(reader[3].ToString());
                            dniTygodnia.od_cz = Help.IsEmptyOd(reader[4].ToString());
                            dniTygodnia.od_pi = Help.IsEmptyOd(reader[5].ToString());
                            dniTygodnia.od_so = Help.IsEmptyOd(reader[6].ToString());
                            dniTygodnia.od_ni = Help.IsEmptyOd(reader[7].ToString());
                            dniTygodnia.do_pn = Help.IsEmptyDo(reader[8].ToString());
                            dniTygodnia.do_wt = Help.IsEmptyDo(reader[9].ToString());
                            dniTygodnia.do_sr = Help.IsEmptyDo(reader[10].ToString());
                            dniTygodnia.do_cz = Help.IsEmptyDo(reader[11].ToString());
                            dniTygodnia.do_pi = Help.IsEmptyDo(reader[12].ToString());
                            dniTygodnia.do_so = Help.IsEmptyDo(reader[13].ToString());
                            dniTygodnia.do_ni = Help.IsEmptyDo(reader[14].ToString());
                            dniTygodnia.przestoje_pn = Help.IsEmptyPrzestoje(reader[15].ToString());
                            dniTygodnia.przestoje_wt = Help.IsEmptyPrzestoje(reader[16].ToString());
                            dniTygodnia.przestoje_sr = Help.IsEmptyPrzestoje(reader[17].ToString());
                            dniTygodnia.przestoje_cz = Help.IsEmptyPrzestoje(reader[18].ToString());
                            dniTygodnia.przestoje_pi = Help.IsEmptyPrzestoje(reader[19].ToString());
                            dniTygodnia.przestoje_so = Help.IsEmptyPrzestoje(reader[20].ToString());
                            dniTygodnia.przestoje_ni = Help.IsEmptyPrzestoje(reader[21].ToString());
                            dniTygodnia.dojazd_pn = Help.IsEmptyDojazd(reader[22].ToString());
                            dniTygodnia.dojazd_wt = Help.IsEmptyDojazd(reader[23].ToString());
                            dniTygodnia.dojazd_sr = Help.IsEmptyDojazd(reader[24].ToString());
                            dniTygodnia.dojazd_cz = Help.IsEmptyDojazd(reader[25].ToString());
                            dniTygodnia.dojazd_pi = Help.IsEmptyDojazd(reader[26].ToString());
                            dniTygodnia.dojazd_so = Help.IsEmptyDojazd(reader[27].ToString());
                            dniTygodnia.dojazd_ni = Help.IsEmptyDojazd(reader[28].ToString());
                            dniTygodnia.dieta_pn = Help.IsEmptyDieta(reader[29]);
                            dniTygodnia.dieta_wt = Help.IsEmptyDieta(reader[30]);
                            dniTygodnia.dieta_sr = Help.IsEmptyDieta(reader[31]);
                            dniTygodnia.dieta_cz = Help.IsEmptyDieta(reader[32]);
                            dniTygodnia.dieta_pi = Help.IsEmptyDieta(reader[33]);
                            dniTygodnia.dieta_so = Help.IsEmptyDieta(reader[34]);
                            dniTygodnia.dieta_ni = Help.IsEmptyDieta(reader[35]);
                        }
                    }
                    catch (Exception)
                    {
                    }
                }
                finally
                {
                    reader.Close();
                }
                if (!sciagnieto)
                {
                    dniTygodnia.od_pn = Help.IsEmptyOd("");
                    dniTygodnia.od_wt = Help.IsEmptyOd("");
                    dniTygodnia.od_sr = Help.IsEmptyOd("");
                    dniTygodnia.od_cz = Help.IsEmptyOd("");
                    dniTygodnia.od_pi = Help.IsEmptyOd("");
                    dniTygodnia.od_so = Help.IsEmptyOd("");
                    dniTygodnia.od_ni = Help.IsEmptyOd("");
                    dniTygodnia.do_pn = Help.IsEmptyDo("");
                    dniTygodnia.do_wt = Help.IsEmptyDo("");
                    dniTygodnia.do_sr = Help.IsEmptyDo("");
                    dniTygodnia.do_cz = Help.IsEmptyDo("");
                    dniTygodnia.do_pi = Help.IsEmptyDo("");
                    dniTygodnia.do_so = Help.IsEmptyDo("");
                    dniTygodnia.do_ni = Help.IsEmptyDo("");
                    dniTygodnia.przestoje_pn = Help.IsEmptyPrzestoje("");
                    dniTygodnia.przestoje_wt = Help.IsEmptyPrzestoje("");
                    dniTygodnia.przestoje_sr = Help.IsEmptyPrzestoje("");
                    dniTygodnia.przestoje_cz = Help.IsEmptyPrzestoje("");
                    dniTygodnia.przestoje_pi = Help.IsEmptyPrzestoje("");
                    dniTygodnia.przestoje_so = Help.IsEmptyPrzestoje("");
                    dniTygodnia.przestoje_ni = Help.IsEmptyPrzestoje("");
                    dniTygodnia.dojazd_pn = Help.IsEmptyDojazd("");
                    dniTygodnia.dojazd_wt = Help.IsEmptyDojazd("");
                    dniTygodnia.dojazd_sr = Help.IsEmptyDojazd("");
                    dniTygodnia.dojazd_cz = Help.IsEmptyDojazd("");
                    dniTygodnia.dojazd_pi = Help.IsEmptyDojazd("");
                    dniTygodnia.dojazd_so = Help.IsEmptyDojazd("");
                    dniTygodnia.dojazd_ni = Help.IsEmptyDojazd("");
                    dniTygodnia.dieta_pn = Help.IsEmptyDieta("");
                    dniTygodnia.dieta_wt = Help.IsEmptyDieta("");
                    dniTygodnia.dieta_sr = Help.IsEmptyDieta("");
                    dniTygodnia.dieta_cz = Help.IsEmptyDieta("");
                    dniTygodnia.dieta_pi = Help.IsEmptyDieta("");
                    dniTygodnia.dieta_so = Help.IsEmptyDieta("");
                    dniTygodnia.dieta_ni = Help.IsEmptyDieta("");
                }
                dniTygodnium = dniTygodnia;
            }
            return dniTygodnium;
        }

        private static DniTygodnia getWeekFromDatabase(AspNetGodziny godziny)
        {
            DniTygodnia dniTygodnium = new DniTygodnia();
            godziny = Help.PoprawGodziny(godziny);
            dniTygodnium.obecny = godziny.obecny;
            dniTygodnium.od_pn = godziny.od_pn;
            dniTygodnium.od_wt = godziny.od_wt;
            dniTygodnium.od_sr = godziny.od_sr;
            dniTygodnium.od_cz = godziny.od_cz;
            dniTygodnium.od_pi = godziny.od_pi;
            dniTygodnium.od_so = godziny.od_so;
            dniTygodnium.od_ni = godziny.od_ni;
            dniTygodnium.do_pn = godziny.do_pn;
            dniTygodnium.do_wt = godziny.do_wt;
            dniTygodnium.do_sr = godziny.do_sr;
            dniTygodnium.do_cz = godziny.do_cz;
            dniTygodnium.do_pi = godziny.do_pi;
            dniTygodnium.do_so = godziny.do_so;
            dniTygodnium.do_ni = godziny.do_ni;
            dniTygodnium.przestoje_pn = godziny.przestoje_pn;
            dniTygodnium.przestoje_wt = godziny.przestoje_wt;
            dniTygodnium.przestoje_sr = godziny.przestoje_sr;
            dniTygodnium.przestoje_cz = godziny.przestoje_cz;
            dniTygodnium.przestoje_pi = godziny.przestoje_pi;
            dniTygodnium.przestoje_so = godziny.przestoje_so;
            dniTygodnium.przestoje_ni = godziny.przestoje_ni;
            dniTygodnium.dojazd_pn = godziny.dojazd_pn;
            dniTygodnium.dojazd_wt = godziny.dojazd_wt;
            dniTygodnium.dojazd_sr = godziny.dojazd_sr;
            dniTygodnium.dojazd_cz = godziny.dojazd_cz;
            dniTygodnium.dojazd_pi = godziny.dojazd_pi;
            dniTygodnium.dojazd_so = godziny.dojazd_so;
            dniTygodnium.dojazd_ni = godziny.dojazd_ni;
            dniTygodnium.dieta_pn = Convert.ToInt32(godziny.dieta_pn);
            dniTygodnium.dieta_wt = Convert.ToInt32(godziny.dieta_wt);
            dniTygodnium.dieta_sr = Convert.ToInt32(godziny.dieta_sr);
            dniTygodnium.dieta_cz = Convert.ToInt32(godziny.dieta_cz);
            dniTygodnium.dieta_pi = Convert.ToInt32(godziny.dieta_pi);
            dniTygodnium.dieta_so = Convert.ToInt32(godziny.dieta_so);
            dniTygodnium.dieta_ni = Convert.ToInt32(godziny.dieta_ni);
            dniTygodnium.obecny_pn = godziny.obecny_pn;
            dniTygodnium.obecny_wt = godziny.obecny_wt;
            dniTygodnium.obecny_sr = godziny.obecny_sr;
            dniTygodnium.obecny_cz = godziny.obecny_cz;
            dniTygodnium.obecny_pi = godziny.obecny_pi;
            dniTygodnium.obecny_so = godziny.obecny_so;
            dniTygodnium.obecny_ni = godziny.obecny_ni;
            dniTygodnium.paragony_po = godziny.paragony_pn.ToString();
            dniTygodnium.paragony_wt = godziny.paragony_wt.ToString();
            dniTygodnium.paragony_sr = godziny.paragony_sr.ToString();
            dniTygodnium.paragony_cz = godziny.paragony_cz.ToString();
            dniTygodnium.paragony_pi = godziny.paragony_pi.ToString();
            dniTygodnium.paragony_so = godziny.paragony_so.ToString();
            dniTygodnium.paragony_ni = godziny.paragony_ni.ToString();
            return dniTygodnium;
        }

        private static bool IfWeekHasCurrentMonth(Tydzien tydzien)
        {
            string dayOfWeek = Help.GetDayOfWeek(tydzien.getRokTydzien(), 1);
            string lastDay = Help.GetDayOfWeek(tydzien.getRokTydzien(), 7);
            int num = Convert.ToInt32(dayOfWeek.Substring(3, 2));
            int monthLastDay = Convert.ToInt32(lastDay.Substring(3, 2));
            if (num <= DateTime.Now.Month + 1 && monthLastDay <= DateTime.Now.Month + 1)
            {
                return false;
            }
            return true;
        }

        public static int IsEmptyDieta(object v)
        {
            int num;
            try
            {
                num = Convert.ToInt32(v);
            }
            catch (Exception)
            {
                num = 0;
            }
            return num;
        }

        public static string IsEmptyDo(string v)
        {
            if (string.IsNullOrEmpty(v))
            {
                return "00.00";
            }
            return v;
        }

        public static string IsEmptyDojazd(string v)
        {
            if (string.IsNullOrEmpty(v) || v == "0")
            {
                return "";
            }
            return v.Replace(" ", "");
        }

        private static string IsEmptyOd(string v)
        {
            if (string.IsNullOrEmpty(v))
            {
                return "00.00";
            }
            return v;
        }

        public static string IsEmptyPrzestoje(string v)
        {
            if (string.IsNullOrEmpty(v) || v == "0")
            {
                return "";
            }
            return v.Replace(" ", "");
        }

        public static bool isSession(object session)
        {
            if (session == null || ((List<Brygada>)session).Count == 0)
            {
                return false;
            }
            return true;
        }

        public static string GetRodzajGodziny(string r)
        {
            string w = "Obecny";
            switch (r)
            {
                case "U":
                    w = "Urlop";
                    break;
                case "UB":
                    w = "Urlop bezpłatny";
                    break;
                case "UO":
                    w = "Urlop okolicznościowy";
                    break;
                case "CH":
                    w = "Choroba";
                    break;
                case "OND":
                    w = "Opieka na dziecko";
                    break;
                case "ONC":
                    w = "Opieka nad chorym";
                    break;
            }
            return w;
        }

        public static List<Brygada> NowaBrygadaDelete(int idpracownika, List<Brygada> brygada)
        {
            List<Brygada> nowaBrygada = new List<Brygada>();
            foreach (Brygada pracownik in brygada)
            {
                if (pracownik.getId() == idpracownika)
                {
                    continue;
                }
                nowaBrygada.Add(pracownik);
            }
            return nowaBrygada;
        }

        private static double OptimizeForIntranet(string godzina)
        {
            if (string.IsNullOrEmpty(godzina))
            {
                return 0;
            }
            if (string.IsNullOrEmpty(godzina.Replace(" ", "")))
            {
                return 0;
            }
            string str = "";
            try
            {
                str = godzina.ToString();
                if (str.Contains(".30") || str.Contains(".3") || str.Contains(",30") || str.Contains(",3"))
                {
                    str = string.Concat(str.Substring(0, 2), ".50").Replace("..", ".");
                }
                if (str.Contains(".15") || str.Contains(",15"))
                {
                    str = string.Concat(str.Substring(0, 2), ".25").Replace("..", ".");
                }
                if (str.Contains(".45") || str.Contains(",45"))
                {
                    str = string.Concat(str.Substring(0, 2), ".75").Replace("..", ".");
                }
            }
            catch (Exception)
            {
            }
            if (godzina == null)
            {
                return 0;
            }
            return Convert.ToDouble(str.Replace(" ", ""));
        }

        public static AspNetGodziny PoprawGodziny(AspNetGodziny godziny)
        {
            string str;
            string str1;
            string str2;
            string str3;
            string str4;
            string str5;
            string str6;
            string str7;
            string str8;
            string str9;
            string str10;
            string str11;
            string str12;
            string str13;
            string str14;
            string str15;
            string str16;
            string str17;
            string str18;
            string str19;
            string str20;
            string str21;
            string str22;
            string str23;
            string str24;
            string str25;
            string str26;
            string str27;
            AspNetGodziny aspNetGodziny = godziny;
            if (string.IsNullOrEmpty(godziny.od_pn))
            {
                str = null;
            }
            else
            {
                str = godziny.od_pn.Replace(" ", "");
            }
            aspNetGodziny.od_pn = str;
            AspNetGodziny aspNetGodziny1 = godziny;
            if (string.IsNullOrEmpty(godziny.od_wt))
            {
                str1 = null;
            }
            else
            {
                str1 = godziny.od_wt.Replace(" ", "");
            }
            aspNetGodziny1.od_wt = str1;
            AspNetGodziny aspNetGodziny2 = godziny;
            if (string.IsNullOrEmpty(godziny.od_sr))
            {
                str2 = null;
            }
            else
            {
                str2 = godziny.od_sr.Replace(" ", "");
            }
            aspNetGodziny2.od_sr = str2;
            AspNetGodziny aspNetGodziny3 = godziny;
            if (string.IsNullOrEmpty(godziny.od_cz))
            {
                str3 = null;
            }
            else
            {
                str3 = godziny.od_cz.Replace(" ", "");
            }
            aspNetGodziny3.od_cz = str3;
            AspNetGodziny aspNetGodziny4 = godziny;
            if (string.IsNullOrEmpty(godziny.od_pi))
            {
                str4 = null;
            }
            else
            {
                str4 = godziny.od_pi.Replace(" ", "");
            }
            aspNetGodziny4.od_pi = str4;
            AspNetGodziny aspNetGodziny5 = godziny;
            if (string.IsNullOrEmpty(godziny.od_so))
            {
                str5 = null;
            }
            else
            {
                str5 = godziny.od_so.Replace(" ", "");
            }
            aspNetGodziny5.od_so = str5;
            AspNetGodziny aspNetGodziny6 = godziny;
            if (string.IsNullOrEmpty(godziny.od_ni))
            {
                str6 = null;
            }
            else
            {
                str6 = godziny.od_ni.Replace(" ", "");
            }
            aspNetGodziny6.od_ni = str6;
            AspNetGodziny aspNetGodziny7 = godziny;
            if (string.IsNullOrEmpty(godziny.do_pn))
            {
                str7 = null;
            }
            else
            {
                str7 = godziny.do_pn.Replace(" ", "");
            }
            aspNetGodziny7.do_pn = str7;
            AspNetGodziny aspNetGodziny8 = godziny;
            if (string.IsNullOrEmpty(godziny.do_wt))
            {
                str8 = null;
            }
            else
            {
                str8 = godziny.do_wt.Replace(" ", "");
            }
            aspNetGodziny8.do_wt = str8;
            AspNetGodziny aspNetGodziny9 = godziny;
            if (string.IsNullOrEmpty(godziny.do_sr))
            {
                str9 = null;
            }
            else
            {
                str9 = godziny.do_sr.Replace(" ", "");
            }
            aspNetGodziny9.do_sr = str9;
            AspNetGodziny aspNetGodziny10 = godziny;
            if (string.IsNullOrEmpty(godziny.do_cz))
            {
                str10 = null;
            }
            else
            {
                str10 = godziny.do_cz.Replace(" ", "");
            }
            aspNetGodziny10.do_cz = str10;
            AspNetGodziny aspNetGodziny11 = godziny;
            if (string.IsNullOrEmpty(godziny.do_pi))
            {
                str11 = null;
            }
            else
            {
                str11 = godziny.do_pi.Replace(" ", "");
            }
            aspNetGodziny11.do_pi = str11;
            AspNetGodziny aspNetGodziny12 = godziny;
            if (string.IsNullOrEmpty(godziny.do_so))
            {
                str12 = null;
            }
            else
            {
                str12 = godziny.do_so.Replace(" ", "");
            }
            aspNetGodziny12.do_so = str12;
            AspNetGodziny aspNetGodziny13 = godziny;
            if (string.IsNullOrEmpty(godziny.do_ni))
            {
                str13 = null;
            }
            else
            {
                str13 = godziny.do_ni.Replace(" ", "");
            }
            aspNetGodziny13.do_ni = str13;
            AspNetGodziny aspNetGodziny14 = godziny;
            if (string.IsNullOrEmpty(godziny.przestoje_pn))
            {
                str14 = null;
            }
            else
            {
                str14 = godziny.przestoje_pn.Replace(" ", "");
            }
            aspNetGodziny14.przestoje_pn = str14;
            AspNetGodziny aspNetGodziny15 = godziny;
            if (string.IsNullOrEmpty(godziny.przestoje_wt))
            {
                str15 = null;
            }
            else
            {
                str15 = godziny.przestoje_wt.Replace(" ", "");
            }
            aspNetGodziny15.przestoje_wt = str15;
            AspNetGodziny aspNetGodziny16 = godziny;
            if (string.IsNullOrEmpty(godziny.przestoje_sr))
            {
                str16 = null;
            }
            else
            {
                str16 = godziny.przestoje_sr.Replace(" ", "");
            }
            aspNetGodziny16.przestoje_sr = str16;
            AspNetGodziny aspNetGodziny17 = godziny;
            if (string.IsNullOrEmpty(godziny.przestoje_cz))
            {
                str17 = null;
            }
            else
            {
                str17 = godziny.przestoje_cz.Replace(" ", "");
            }
            aspNetGodziny17.przestoje_cz = str17;
            AspNetGodziny aspNetGodziny18 = godziny;
            if (string.IsNullOrEmpty(godziny.przestoje_pi))
            {
                str18 = null;
            }
            else
            {
                str18 = godziny.przestoje_pi.Replace(" ", "");
            }
            aspNetGodziny18.przestoje_pi = str18;
            AspNetGodziny aspNetGodziny19 = godziny;
            if (string.IsNullOrEmpty(godziny.przestoje_so))
            {
                str19 = null;
            }
            else
            {
                str19 = godziny.przestoje_so.Replace(" ", "");
            }
            aspNetGodziny19.przestoje_so = str19;
            AspNetGodziny aspNetGodziny20 = godziny;
            if (string.IsNullOrEmpty(godziny.przestoje_ni))
            {
                str20 = null;
            }
            else
            {
                str20 = godziny.przestoje_ni.Replace(" ", "");
            }
            aspNetGodziny20.przestoje_ni = str20;
            AspNetGodziny aspNetGodziny21 = godziny;
            if (string.IsNullOrEmpty(godziny.dojazd_pn))
            {
                str21 = null;
            }
            else
            {
                str21 = godziny.dojazd_pn.Replace(" ", "");
            }
            aspNetGodziny21.dojazd_pn = str21;
            AspNetGodziny aspNetGodziny22 = godziny;
            if (string.IsNullOrEmpty(godziny.dojazd_wt))
            {
                str22 = null;
            }
            else
            {
                str22 = godziny.dojazd_wt.Replace(" ", "");
            }
            aspNetGodziny22.dojazd_wt = str22;
            AspNetGodziny aspNetGodziny23 = godziny;
            if (string.IsNullOrEmpty(godziny.dojazd_sr))
            {
                str23 = null;
            }
            else
            {
                str23 = godziny.dojazd_sr.Replace(" ", "");
            }
            aspNetGodziny23.dojazd_sr = str23;
            AspNetGodziny aspNetGodziny24 = godziny;
            if (string.IsNullOrEmpty(godziny.dojazd_cz))
            {
                str24 = null;
            }
            else
            {
                str24 = godziny.dojazd_cz.Replace(" ", "");
            }
            aspNetGodziny24.dojazd_cz = str24;
            AspNetGodziny aspNetGodziny25 = godziny;
            if (string.IsNullOrEmpty(godziny.dojazd_pi))
            {
                str25 = null;
            }
            else
            {
                str25 = godziny.dojazd_pi.Replace(" ", "");
            }
            aspNetGodziny25.dojazd_pi = str25;
            AspNetGodziny aspNetGodziny26 = godziny;
            if (string.IsNullOrEmpty(godziny.dojazd_so))
            {
                str26 = null;
            }
            else
            {
                str26 = godziny.dojazd_so.Replace(" ", "");
            }
            aspNetGodziny26.dojazd_so = str26;
            AspNetGodziny aspNetGodziny27 = godziny;
            if (string.IsNullOrEmpty(godziny.dojazd_ni))
            {
                str27 = null;
            }
            else
            {
                str27 = godziny.dojazd_ni.Replace(" ", "");
            }
            aspNetGodziny27.dojazd_ni = str27;
            AspNetGodziny dietaPn = godziny;
            dietaPn.dieta_pn = dietaPn.dieta_pn;
            AspNetGodziny dietaWt = godziny;
            dietaWt.dieta_wt = dietaWt.dieta_wt;
            AspNetGodziny dietaSr = godziny;
            dietaSr.dieta_sr = dietaSr.dieta_sr;
            AspNetGodziny dietaCz = godziny;
            dietaCz.dieta_cz = dietaCz.dieta_cz;
            AspNetGodziny dietaPi = godziny;
            dietaPi.dieta_pi = dietaPi.dieta_pi;
            AspNetGodziny dietaSo = godziny;
            dietaSo.dieta_so = dietaSo.dieta_so;
            AspNetGodziny dietaNi = godziny;
            dietaNi.dieta_ni = dietaNi.dieta_ni;
            return godziny;
        }

        public static AspNetRozliczeniaBudow PoprawRozliczenie(AspNetRozliczeniaBudow rozliczenie)
        {
            rozliczenie.powPob_pn = Help.StringDoubleConvertible(rozliczenie.powPob_pn);
            rozliczenie.powPob_wt = Help.StringDoubleConvertible(rozliczenie.powPob_wt);
            rozliczenie.powPob_sr = Help.StringDoubleConvertible(rozliczenie.powPob_sr);
            rozliczenie.powPob_cz = Help.StringDoubleConvertible(rozliczenie.powPob_cz);
            rozliczenie.powPob_pi = Help.StringDoubleConvertible(rozliczenie.powPob_pi);
            rozliczenie.powPob_so = Help.StringDoubleConvertible(rozliczenie.powPob_so);
            rozliczenie.powExtra_nazwa = Help.StringDoubleConvertible2(rozliczenie.powExtra_nazwa);
            rozliczenie.powExtra_pn = Help.StringDoubleConvertible(rozliczenie.powExtra_pn);
            rozliczenie.powExtra_wt = Help.StringDoubleConvertible(rozliczenie.powExtra_wt);
            rozliczenie.powExtra_sr = Help.StringDoubleConvertible(rozliczenie.powExtra_sr);
            rozliczenie.powExtra_cz = Help.StringDoubleConvertible(rozliczenie.powExtra_cz);
            rozliczenie.powExtra_pi = Help.StringDoubleConvertible(rozliczenie.powExtra_pi);
            rozliczenie.powExtra_so = Help.StringDoubleConvertible(rozliczenie.powExtra_so);
            rozliczenie.godzPob_pn = Help.StringDoubleConvertible(rozliczenie.godzPob_pn);
            rozliczenie.godzPob_wt = Help.StringDoubleConvertible(rozliczenie.godzPob_wt);
            rozliczenie.godzPob_sr = Help.StringDoubleConvertible(rozliczenie.godzPob_sr);
            rozliczenie.godzPob_cz = Help.StringDoubleConvertible(rozliczenie.godzPob_cz);
            rozliczenie.godzPob_pi = Help.StringDoubleConvertible(rozliczenie.godzPob_pi);
            rozliczenie.godzPob_so = Help.StringDoubleConvertible(rozliczenie.godzPob_so);
            rozliczenie.godzExtra1_nazwa = Help.StringDoubleConvertible2(rozliczenie.godzExtra1_nazwa);
            rozliczenie.godzExtra1_pn = Help.StringDoubleConvertible(rozliczenie.godzExtra1_pn);
            rozliczenie.godzExtra1_wt = Help.StringDoubleConvertible(rozliczenie.godzExtra1_wt);
            rozliczenie.godzExtra1_sr = Help.StringDoubleConvertible(rozliczenie.godzExtra1_sr);
            rozliczenie.godzExtra1_cz = Help.StringDoubleConvertible(rozliczenie.godzExtra1_cz);
            rozliczenie.godzExtra1_pi = Help.StringDoubleConvertible(rozliczenie.godzExtra1_pi);
            rozliczenie.godzExtra1_so = Help.StringDoubleConvertible(rozliczenie.godzExtra1_so);
            rozliczenie.godzExtra2_nazwa = Help.StringDoubleConvertible2(rozliczenie.godzExtra2_nazwa);
            rozliczenie.godzExtra2_pn = Help.StringDoubleConvertible(rozliczenie.godzExtra2_pn);
            rozliczenie.godzExtra2_wt = Help.StringDoubleConvertible(rozliczenie.godzExtra2_wt);
            rozliczenie.godzExtra2_sr = Help.StringDoubleConvertible(rozliczenie.godzExtra2_sr);
            rozliczenie.godzExtra2_cz = Help.StringDoubleConvertible(rozliczenie.godzExtra2_cz);
            rozliczenie.godzExtra2_pi = Help.StringDoubleConvertible(rozliczenie.godzExtra2_pi);
            rozliczenie.godzExtra2_so = Help.StringDoubleConvertible(rozliczenie.godzExtra2_so);
            rozliczenie.godzExtra3_nazwa = Help.StringDoubleConvertible2(rozliczenie.godzExtra3_nazwa);
            rozliczenie.godzExtra3_pn = Help.StringDoubleConvertible(rozliczenie.godzExtra3_pn);
            rozliczenie.godzExtra3_wt = Help.StringDoubleConvertible(rozliczenie.godzExtra3_wt);
            rozliczenie.godzExtra3_sr = Help.StringDoubleConvertible(rozliczenie.godzExtra3_sr);
            rozliczenie.godzExtra3_cz = Help.StringDoubleConvertible(rozliczenie.godzExtra3_cz);
            rozliczenie.godzExtra3_pi = Help.StringDoubleConvertible(rozliczenie.godzExtra3_pi);
            rozliczenie.godzExtra3_so = Help.StringDoubleConvertible(rozliczenie.godzExtra3_so);
            rozliczenie.godzExtra4_nazwa = Help.StringDoubleConvertible2(rozliczenie.godzExtra4_nazwa);
            rozliczenie.godzExtra4_pn = Help.StringDoubleConvertible(rozliczenie.godzExtra4_pn);
            rozliczenie.godzExtra4_wt = Help.StringDoubleConvertible(rozliczenie.godzExtra4_wt);
            rozliczenie.godzExtra4_sr = Help.StringDoubleConvertible(rozliczenie.godzExtra4_sr);
            rozliczenie.godzExtra4_cz = Help.StringDoubleConvertible(rozliczenie.godzExtra4_cz);
            rozliczenie.godzExtra4_pi = Help.StringDoubleConvertible(rozliczenie.godzExtra4_pi);
            rozliczenie.godzExtra4_so = Help.StringDoubleConvertible(rozliczenie.godzExtra4_so);
            rozliczenie.dzwig_pn = Help.StringDoubleConvertible(rozliczenie.dzwig_pn);
            rozliczenie.dzwig_wt = Help.StringDoubleConvertible(rozliczenie.dzwig_wt);
            rozliczenie.dzwig_sr = Help.StringDoubleConvertible(rozliczenie.dzwig_sr);
            rozliczenie.dzwig_cz = Help.StringDoubleConvertible(rozliczenie.dzwig_cz);
            rozliczenie.dzwig_pi = Help.StringDoubleConvertible(rozliczenie.dzwig_pi);
            rozliczenie.dzwig_so = Help.StringDoubleConvertible(rozliczenie.dzwig_so);
            rozliczenie.cat_pn = Help.StringDoubleConvertible(rozliczenie.cat_pn);
            rozliczenie.cat_wt = Help.StringDoubleConvertible(rozliczenie.cat_wt);
            rozliczenie.cat_sr = Help.StringDoubleConvertible(rozliczenie.cat_sr);
            rozliczenie.cat_cz = Help.StringDoubleConvertible(rozliczenie.cat_cz);
            rozliczenie.cat_pi = Help.StringDoubleConvertible(rozliczenie.cat_pi);
            rozliczenie.cat_so = Help.StringDoubleConvertible(rozliczenie.cat_so);
            rozliczenie.manitou_pn = Help.StringDoubleConvertible(rozliczenie.manitou_pn);
            rozliczenie.manitou_wt = Help.StringDoubleConvertible(rozliczenie.manitou_wt);
            rozliczenie.manitou_sr = Help.StringDoubleConvertible(rozliczenie.manitou_sr);
            rozliczenie.manitou_cz = Help.StringDoubleConvertible(rozliczenie.manitou_cz);
            rozliczenie.manitou_pi = Help.StringDoubleConvertible(rozliczenie.manitou_pi);
            rozliczenie.manitou_so = Help.StringDoubleConvertible(rozliczenie.manitou_so);
            rozliczenie.inne_pn = Help.StringDoubleConvertible(rozliczenie.inne_pn);
            rozliczenie.inne_wt = Help.StringDoubleConvertible(rozliczenie.inne_wt);
            rozliczenie.inne_sr = Help.StringDoubleConvertible(rozliczenie.inne_sr);
            rozliczenie.inne_cz = Help.StringDoubleConvertible(rozliczenie.inne_cz);
            rozliczenie.inne_pt = Help.StringDoubleConvertible(rozliczenie.inne_pt);
            rozliczenie.inne_so = Help.StringDoubleConvertible(rozliczenie.inne_so);
            rozliczenie.notatki_pn = Help.StringDoubleConvertible2(rozliczenie.notatki_pn);
            rozliczenie.notatki_wt = Help.StringDoubleConvertible2(rozliczenie.notatki_wt);
            rozliczenie.notatki_sr = Help.StringDoubleConvertible2(rozliczenie.notatki_sr);
            rozliczenie.notatki_cz = Help.StringDoubleConvertible2(rozliczenie.notatki_cz);
            rozliczenie.notatki_pi = Help.StringDoubleConvertible2(rozliczenie.notatki_pi);
            rozliczenie.notatki_so = Help.StringDoubleConvertible2(rozliczenie.notatki_so);
            return rozliczenie;
        }

        public static void SaveSettings(int userId, DniTygodnia s)
        {
            using (SqlConnection connection = new SqlConnection(Help.connectionString))
            {
                SqlCommand sqlCommand = new SqlCommand(string.Format("IF EXISTS (SELECT * FROM AspNetUserSettings WHERE Id = {7}) UPDATE AspNetUserSettings SET od_pn = '{0}', od_wt = '{1}', od_sr = '{2}', od_cz = '{3}', od_pi = '{4}', od_so = '{5}', od_ni = '{6}' WHERE Id = {7} ELSE INSERT INTO AspNetUserSettings(od_pn, od_wt, od_sr, od_cz, od_pi, od_so, od_ni, Id) VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}',{7})", new object[] { s.od_pn, s.od_wt, s.od_sr, s.od_cz, s.od_pi, s.od_so, s.od_ni, userId }), connection);
                connection.Open();
                sqlCommand.ExecuteNonQuery();
                connection.Close();
                SqlCommand sqlCommand1 = new SqlCommand(string.Format("IF EXISTS (SELECT * FROM AspNetUserSettings WHERE Id = {7}) UPDATE AspNetUserSettings SET do_pn = '{0}', do_wt = '{1}', do_sr = '{2}', do_cz = '{3}', do_pi = '{4}', do_so = '{5}', do_ni = '{6}' WHERE Id = {7} ELSE INSERT INTO AspNetUserSettings(do_pn, do_wt, do_sr, do_cz, do_pi, do_so, do_ni, Id) VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}',{7})", new object[] { s.do_pn, s.do_wt, s.do_sr, s.do_cz, s.do_pi, s.do_so, s.do_ni, userId }), connection);
                connection.Open();
                sqlCommand1.ExecuteNonQuery();
                connection.Close();
                SqlCommand sqlCommand2 = new SqlCommand(string.Format("IF EXISTS (SELECT * FROM AspNetUserSettings WHERE Id = {7}) UPDATE AspNetUserSettings SET przestoje_pn = '{0}', przestoje_wt = '{1}', przestoje_sr = '{2}', przestoje_cz = '{3}', przestoje_pi = '{4}', przestoje_so = '{5}', przestoje_ni = '{6}' WHERE Id = {7} ELSE INSERT INTO AspNetUserSettings(przestoje_pn, przestoje_wt, przestoje_sr, przestoje_cz, przestoje_pi, przestoje_so, przestoje_ni, Id) VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}',{7})", new object[] { s.przestoje_pn, s.przestoje_wt, s.przestoje_sr, s.przestoje_cz, s.przestoje_pi, s.przestoje_so, s.przestoje_ni, userId }), connection);
                connection.Open();
                sqlCommand2.ExecuteNonQuery();
                connection.Close();
                SqlCommand sqlCommand3 = new SqlCommand(string.Format("IF EXISTS (SELECT * FROM AspNetUserSettings WHERE Id = {7}) UPDATE AspNetUserSettings SET dojazd_pn = '{0}', dojazd_wt = '{1}', dojazd_sr = '{2}', dojazd_cz = '{3}', dojazd_pi = '{4}', dojazd_so = '{5}', dojazd_ni = '{6}' WHERE Id = {7} ELSE INSERT INTO AspNetUserSettings(dojazd_pn, dojazd_wt, dojazd_sr, dojazd_cz, dojazd_pi, dojazd_so, dojazd_ni, Id) VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}',{7})", new object[] { s.dojazd_pn, s.dojazd_wt, s.dojazd_sr, s.dojazd_cz, s.dojazd_pi, s.dojazd_so, s.dojazd_ni, userId }), connection);
                connection.Open();
                sqlCommand3.ExecuteNonQuery();
                connection.Close();
                SqlCommand sqlCommand4 = new SqlCommand(string.Format("IF EXISTS (SELECT * FROM AspNetUserSettings WHERE Id = {7}) UPDATE AspNetUserSettings SET dieta_pn = '{0}', dieta_wt = '{1}', dieta_sr = '{2}', dieta_cz = '{3}', dieta_pi = '{4}', dieta_so = '{5}', dieta_ni = '{6}' WHERE Id = {7} ELSE INSERT INTO AspNetUserSettings(dieta_pn, dieta_wt, dieta_sr, dieta_cz, dieta_pi, dieta_so, dieta_ni, Id) VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}',{7})", new object[] { s.dieta_pn, s.dieta_wt, s.dieta_sr, s.dieta_cz, s.dieta_pi, s.dieta_so, s.dieta_ni, userId }), connection);
                connection.Open();
                sqlCommand4.ExecuteNonQuery();
                connection.Close();
            }
        }

        public static void SaveWeekToDatabase(DashboardViewModel dvm, Budowa budowa, string path, List<string> imageData, string serverPath, Tydzien tydzienTmp, bool czyInnaZmiana)
        {
            DateTime now;
            try
            {
                string json = JsonConvert.SerializeObject(dvm, Formatting.Indented);
                string[] baseDirectory = new string[] { AppDomain.CurrentDomain.BaseDirectory, "Logi\\Dashboard\\", dvm.Brygadzista.getNazwa(), null, null };
                now = DateTime.Now;
                baseDirectory[3] = now.ToString("dd-MM-yyyy");
                baseDirectory[4] = ".txt";
                StreamWriter sw = new StreamWriter(string.Concat(baseDirectory), true);
                string[] str = new string[12];
                now = DateTime.Now;
                str[0] = now.ToString();
                str[1] = Environment.NewLine;
                str[2] = json.ToString();
                str[3] = Environment.NewLine;
                str[4] = ":::Tydzień:::";
                str[5] = tydzienTmp.getRokTydzien();
                str[6] = Environment.NewLine;
                str[7] = Environment.NewLine;
                str[8] = ":::Budowa:::";
                str[9] = budowa.Nazwa;
                str[10] = Environment.NewLine;
                str[11] = "***************************************";
                sw.WriteLine(string.Concat(str));
                sw.Close();
            }
            catch (Exception)
            {
            }
            pobiNTRANETDataContext dataContext = new pobiNTRANETDataContext();
            for (int i = 0; i < dvm.Brygada.Count; i++)
            {
                AspNetGodziny aspnetgodziny = new AspNetGodziny();
                bool insert = true;
                object[] id = new object[] { dvm.Brygadzista.Id, dvm.Brygada[i].Id, dvm.Tydzien.getRokTydzien(), null };
                now = DateTime.Now;
                id[3] = now.ToShortDateString();
                string fileName = string.Format("{0}-{1}-{2}-{3}.png", id);
                string fileNameWitPath = Path.Combine(path, fileName);
                string currentPodpis = null;
                try
                {
                    if (imageData != null && !string.IsNullOrEmpty(imageData[i]) && imageData[i] != Help.pustyPodpis)
                    {
                        using (FileStream fs = new FileStream(fileNameWitPath, FileMode.Create))
                        {
                            using (BinaryWriter bw = new BinaryWriter(fs))
                            {
                                bw.Write(Convert.FromBase64String(imageData[i]));
                                bw.Close();
                            }
                            fs.Close();
                        }
                        currentPodpis = fileName;
                    }
                }
                catch (Exception)
                {
                }
                string rokTydzien = dvm.Tydzien.getRokTydzien();
                Brygada item = dvm.Brygada[i];
                if (dataContext.AspNetGodzinies.Any<AspNetGodziny>((AspNetGodziny g) => g.rokTydzien == rokTydzien && g.pracownikID == item.Id && g.brygadzistaID == dvm.Brygadzista.Id && g.budowaID == budowa.Id && !czyInnaZmiana))
                {
                    insert = false;
                    aspnetgodziny = dataContext.AspNetGodzinies.First<AspNetGodziny>((AspNetGodziny g) => g.rokTydzien == rokTydzien && g.pracownikID == item.Id && g.brygadzistaID == dvm.Brygadzista.Id && g.budowaID == budowa.Id);
                }
                if (!insert)
                {
                    now = DateTime.Now.Date;
                    aspnetgodziny.data_modyfikacji = now.ToString("dd-MM-yyyy hh:mm");
                }
                else
                {
                    now = DateTime.Now.Date;
                    aspnetgodziny.data_wprowadzenia = now.ToString("dd-MM-yyyy hh:mm");
                    if (i != 0)
                    {
                        aspnetgodziny.brygadzistaID = dvm.Brygadzista.Id;
                    }
                    else
                    {
                        aspnetgodziny.brygadzistaID = item.Id;
                    }
                    aspnetgodziny.budowaID = budowa.Id;
                    aspnetgodziny.rok = dvm.Tydzien.getRok();
                    aspnetgodziny.podpis = "";
                    aspnetgodziny.rokTydzien = rokTydzien;
                    aspnetgodziny.pracownikID = dvm.Brygada[i].Id;
                }
                if (string.IsNullOrEmpty(aspnetgodziny.podpis))
                {
                    aspnetgodziny.podpis = currentPodpis;
                }
                aspnetgodziny.od_pn = dvm.DniTygodnia[i].od_pn;
                aspnetgodziny.od_wt = dvm.DniTygodnia[i].od_wt;
                aspnetgodziny.od_sr = dvm.DniTygodnia[i].od_sr;
                aspnetgodziny.od_cz = dvm.DniTygodnia[i].od_cz;
                aspnetgodziny.od_pi = dvm.DniTygodnia[i].od_pi;
                aspnetgodziny.od_so = dvm.DniTygodnia[i].od_so;
                aspnetgodziny.od_ni = dvm.DniTygodnia[i].od_ni;
                aspnetgodziny.do_pn = dvm.DniTygodnia[i].do_pn;
                aspnetgodziny.do_wt = dvm.DniTygodnia[i].do_wt;
                aspnetgodziny.do_sr = dvm.DniTygodnia[i].do_sr;
                aspnetgodziny.do_cz = dvm.DniTygodnia[i].do_cz;
                aspnetgodziny.do_pi = dvm.DniTygodnia[i].do_pi;
                aspnetgodziny.do_so = dvm.DniTygodnia[i].do_so;
                aspnetgodziny.do_ni = dvm.DniTygodnia[i].do_ni;
                aspnetgodziny.przestoje_pn = dvm.DniTygodnia[i].przestoje_pn;
                aspnetgodziny.przestoje_wt = dvm.DniTygodnia[i].przestoje_wt;
                aspnetgodziny.przestoje_sr = dvm.DniTygodnia[i].przestoje_sr;
                aspnetgodziny.przestoje_cz = dvm.DniTygodnia[i].przestoje_cz;
                aspnetgodziny.przestoje_pi = dvm.DniTygodnia[i].przestoje_pi;
                aspnetgodziny.przestoje_so = dvm.DniTygodnia[i].przestoje_so;
                aspnetgodziny.przestoje_ni = dvm.DniTygodnia[i].przestoje_ni;
                aspnetgodziny.dojazd_pn = dvm.DniTygodnia[i].dojazd_pn;
                aspnetgodziny.dojazd_wt = dvm.DniTygodnia[i].dojazd_wt;
                aspnetgodziny.dojazd_sr = dvm.DniTygodnia[i].dojazd_sr;
                aspnetgodziny.dojazd_cz = dvm.DniTygodnia[i].dojazd_cz;
                aspnetgodziny.dojazd_pi = dvm.DniTygodnia[i].dojazd_pi;
                aspnetgodziny.dojazd_so = dvm.DniTygodnia[i].dojazd_so;
                aspnetgodziny.dojazd_ni = dvm.DniTygodnia[i].dojazd_ni;
                int dietaPn = dvm.DniTygodnia[i].dieta_pn;
                aspnetgodziny.dieta_pn = dietaPn.ToString();
                dietaPn = dvm.DniTygodnia[i].dieta_wt;
                aspnetgodziny.dieta_wt = dietaPn.ToString();
                dietaPn = dvm.DniTygodnia[i].dieta_sr;
                aspnetgodziny.dieta_sr = dietaPn.ToString();
                dietaPn = dvm.DniTygodnia[i].dieta_cz;
                aspnetgodziny.dieta_cz = dietaPn.ToString();
                dietaPn = dvm.DniTygodnia[i].dieta_pi;
                aspnetgodziny.dieta_pi = dietaPn.ToString();
                dietaPn = dvm.DniTygodnia[i].dieta_so;
                aspnetgodziny.dieta_so = dietaPn.ToString();
                dietaPn = dvm.DniTygodnia[i].dieta_ni;
                aspnetgodziny.dieta_ni = dietaPn.ToString();
                aspnetgodziny.export = 0;
                aspnetgodziny.obecny = dvm.DniTygodnia[i].obecny;
                aspnetgodziny.obecny_pn = dvm.DniTygodnia[i].obecny_pn;
                aspnetgodziny.obecny_wt = dvm.DniTygodnia[i].obecny_wt;
                aspnetgodziny.obecny_sr = dvm.DniTygodnia[i].obecny_sr;
                aspnetgodziny.obecny_cz = dvm.DniTygodnia[i].obecny_cz;
                aspnetgodziny.obecny_pi = dvm.DniTygodnia[i].obecny_pi;
                aspnetgodziny.obecny_so = dvm.DniTygodnia[i].obecny_so;
                aspnetgodziny.obecny_ni = dvm.DniTygodnia[i].obecny_ni;
                aspnetgodziny.paragony_pn = Convert.ToDecimal(dvm.DniTygodnia[i].paragony_po);
                aspnetgodziny.paragony_wt = Convert.ToDecimal(dvm.DniTygodnia[i].paragony_wt);
                aspnetgodziny.paragony_sr = Convert.ToDecimal(dvm.DniTygodnia[i].paragony_sr);
                aspnetgodziny.paragony_cz = Convert.ToDecimal(dvm.DniTygodnia[i].paragony_cz);
                aspnetgodziny.paragony_pi = Convert.ToDecimal(dvm.DniTygodnia[i].paragony_pi);
                aspnetgodziny.paragony_so = Convert.ToDecimal(dvm.DniTygodnia[i].paragony_so);
                aspnetgodziny.paragony_ni = Convert.ToDecimal(dvm.DniTygodnia[i].paragony_ni);

                try
                {
                    string json = JsonConvert.SerializeObject(aspnetgodziny, Formatting.Indented);
                    string[] strArrays = new string[] { AppDomain.CurrentDomain.BaseDirectory, "Logi\\Dashboard\\", dvm.Brygadzista.getNazwa(), null, null };
                    now = DateTime.Now;
                    strArrays[3] = now.ToString("dd-MM-yyyy");
                    strArrays[4] = "-potwierdzenie.txt";
                    StreamWriter sw = new StreamWriter(string.Concat(strArrays), true);
                    string[] newLine = new string[12];
                    now = DateTime.Now;
                    newLine[0] = now.ToString();
                    newLine[1] = Environment.NewLine;
                    newLine[2] = json.ToString();
                    newLine[3] = Environment.NewLine;
                    newLine[4] = ":::Tydzień:::";
                    newLine[5] = tydzienTmp.getRokTydzien();
                    newLine[6] = Environment.NewLine;
                    newLine[7] = Environment.NewLine;
                    newLine[8] = ":::Budowa:::";
                    newLine[9] = budowa.Nazwa;
                    newLine[10] = Environment.NewLine;
                    newLine[11] = "***************************************";
                    sw.WriteLine(string.Concat(newLine));
                    sw.Close();
                }
                catch (Exception)
                {
                }
                if (budowa.Id == 0)
                {
                    aspnetgodziny.komentarz = budowa.Nazwa;
                }
                if (insert)
                {
                    dataContext.AspNetGodzinies.InsertOnSubmit(aspnetgodziny);
                }
                dataContext.SubmitChanges();
            }
        }

        public static IHtmlString SerializeObject(object value)
        {
            IHtmlString htmlString;
            using (StringWriter stringWriter = new StringWriter())
            {
                using (JsonTextWriter jsonWriter = new JsonTextWriter(stringWriter))
                {
                    JsonSerializer jsonSerializer = new JsonSerializer()
                    {
                        ContractResolver = new CamelCasePropertyNamesContractResolver()
                    };
                    jsonWriter.QuoteName = false;
                    jsonSerializer.Serialize(jsonWriter, value);
                    htmlString = new HtmlString(stringWriter.ToString());
                }
            }
            return htmlString;
        }

        public static bool SprawdzCzyAdmin(object user)
        {
            if (user == null)
            {
                return false;
            }
            if (!user.ToString().Contains("mjedrzejczyk@wizualizacja.com") && !user.ToString().Contains("bpawelec@wizualizacja.com") && !user.ToString().Contains("nsowa@wizualizacja.com"))
            {
                return false;
            }
            return true;
        }

        public static string StringDoubleConvertible(string wartosc)
        {
            if (string.IsNullOrEmpty(wartosc))
            {
                return "0";
            }
            return wartosc;
        }

        public static string StringDoubleConvertible2(string wartosc)
        {
            if (string.IsNullOrEmpty(wartosc))
            {
                return "";
            }
            return wartosc;
        }

        public static void StworzExcelIWyslij(AspNetRozliczeniaBudow update, string v, List<string> listaUserow, string from, string path)
        {
            using (ExcelPackage excel = new ExcelPackage())
            {
                string nazwaArkusza = "Rozliczenie budowy";
                excel.Workbook.Worksheets.Add(nazwaArkusza);
                ExcelWorksheet worksheet = excel.Workbook.Worksheets[nazwaArkusza];
                Color sumRowColumnColor = ColorTranslator.FromHtml("#B7DEE8");
                List<object[]> objArrays = new List<object[]>()
                {
                    new object[] { string.Format("Tydzień: {0}", Help.GetTydzien(update.tydzien_id).getRokTydzien()), string.Format("Budowa: {0}", Help.GetBudowa(update.budowa_id).Nazwa), string.Format("Brygadzista: {0}", Help.GetNameAndSurname(update.brygadzista_id)) }
                };
                List<object[]> headerRow = objArrays;
                string headerRange = string.Concat("A1:", char.ConvertFromUtf32((int)headerRow[0].Length + 64), "1");
                worksheet.Cells[headerRange].LoadFromArrays(headerRow);
                worksheet.Cells[headerRange].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[headerRange].Style.Fill.BackgroundColor.SetColor(sumRowColumnColor);
                objArrays = new List<object[]>()
                {
                    new object[] { "Powierzchnia zrobiona przez", "Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota", "SUMA" }
                };
                List<object[]> Row2 = objArrays;
                string row2Range = string.Concat("A2:", char.ConvertFromUtf32((int)Row2[0].Length + 64), "2");
                worksheet.Cells[row2Range].LoadFromArrays(Row2);
                worksheet.Cells[row2Range].Style.Font.Bold = true;
                worksheet.Cells[row2Range].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[row2Range].Style.Fill.BackgroundColor.SetColor(sumRowColumnColor);
                double powPob_suma = Convert.ToDouble(update.powPob_pn) + Convert.ToDouble(update.powPob_wt) + Convert.ToDouble(update.powPob_sr) + Convert.ToDouble(update.powPob_cz) + Convert.ToDouble(update.powPob_pi) + Convert.ToDouble(update.powPob_so);
                objArrays = new List<object[]>()
                {
                    new object[] { "WIZUALIZACJA", Convert.ToDouble(update.powPob_pn), Convert.ToDouble(update.powPob_wt), Convert.ToDouble(update.powPob_sr), Convert.ToDouble(update.powPob_cz), Convert.ToDouble(update.powPob_pi), Convert.ToDouble(update.powPob_so), powPob_suma }
                };
                List<object[]> Row3 = objArrays;
                string row3Range = string.Concat("A3:", char.ConvertFromUtf32((int)Row3[0].Length + 64), "3");
                worksheet.Cells[row3Range].LoadFromArrays(Row3);
                double powExtra_suma = Convert.ToDouble(update.powExtra_pn) + Convert.ToDouble(update.powExtra_wt) + Convert.ToDouble(update.powExtra_sr) + Convert.ToDouble(update.powExtra_cz) + Convert.ToDouble(update.powExtra_pi) + Convert.ToDouble(update.powExtra_so);
                objArrays = new List<object[]>()
                {
                    new object[] { update.powExtra_nazwa, Convert.ToDouble(update.powExtra_pn), Convert.ToDouble(update.powExtra_wt), Convert.ToDouble(update.powExtra_sr), Convert.ToDouble(update.powExtra_cz), Convert.ToDouble(update.powExtra_pi), Convert.ToDouble(update.powExtra_so), powExtra_suma }
                };
                List<object[]> Row4 = objArrays;
                string row4Range = string.Concat("A4:", char.ConvertFromUtf32((int)Row4[0].Length + 64), "4");
                worksheet.Cells[row4Range].LoadFromArrays(Row4);
                objArrays = new List<object[]>()
                {
                    new object[] { "SUMA", Convert.ToDouble(update.powPob_pn) + Convert.ToDouble(update.powExtra_pn), Convert.ToDouble(update.powPob_wt) + Convert.ToDouble(update.powExtra_wt), Convert.ToDouble(update.powPob_sr) + Convert.ToDouble(update.powExtra_sr), Convert.ToDouble(update.powPob_cz) + Convert.ToDouble(update.powExtra_cz), Convert.ToDouble(update.powPob_pi) + Convert.ToDouble(update.powExtra_pi), Convert.ToDouble(update.powPob_so) + Convert.ToDouble(update.powExtra_so) }
                };
                List<object[]> Row5 = objArrays;
                string row5Range = string.Concat("A5:", char.ConvertFromUtf32((int)Row5[0].Length + 64), "5");
                worksheet.Cells[row5Range].LoadFromArrays(Row5);
                worksheet.Cells[row5Range].Style.Font.Bold = true;
                objArrays = new List<object[]>()
                {
                    new object[] { "Godziny", "Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota", "SUMA" }
                };
                List<object[]> Row6 = objArrays;
                string row6Range = string.Concat("A6:", char.ConvertFromUtf32((int)Row6[0].Length + 64), "6");
                worksheet.Cells[row6Range].LoadFromArrays(Row6);
                worksheet.Cells[row6Range].Style.Font.Bold = true;
                worksheet.Cells[row6Range].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[row6Range].Style.Fill.BackgroundColor.SetColor(sumRowColumnColor);
                double godzPob_suma = Convert.ToDouble(update.godzPob_pn) + Convert.ToDouble(update.godzPob_wt) + Convert.ToDouble(update.godzPob_sr) + Convert.ToDouble(update.godzPob_cz) + Convert.ToDouble(update.godzPob_pi) + Convert.ToDouble(update.godzPob_so);
                objArrays = new List<object[]>()
                {
                    new object[] { "WIZUALIZACJA", Convert.ToDouble(update.godzPob_pn), Convert.ToDouble(update.godzPob_wt), Convert.ToDouble(update.godzPob_sr), Convert.ToDouble(update.godzPob_cz), Convert.ToDouble(update.godzPob_pi), Convert.ToDouble(update.godzPob_so), Convert.ToDouble(godzPob_suma) }
                };
                List<object[]> Row7 = objArrays;
                string row7Range = string.Concat("A7:", char.ConvertFromUtf32((int)Row7[0].Length + 64), "7");
                worksheet.Cells[row7Range].LoadFromArrays(Row7);
                double godzExtra1_suma = Convert.ToDouble(update.godzExtra1_pn) + Convert.ToDouble(update.godzExtra1_wt) + Convert.ToDouble(update.godzExtra1_sr) + Convert.ToDouble(update.godzExtra1_cz) + Convert.ToDouble(update.godzExtra1_pi) + Convert.ToDouble(update.godzExtra1_so);
                objArrays = new List<object[]>()
                {
                    new object[] { update.godzExtra1_nazwa, Convert.ToDouble(update.godzExtra1_pn), Convert.ToDouble(update.godzExtra1_wt), Convert.ToDouble(update.godzExtra1_sr), Convert.ToDouble(update.godzExtra1_cz), Convert.ToDouble(update.godzExtra1_pi), Convert.ToDouble(update.godzExtra1_so), godzExtra1_suma }
                };
                List<object[]> Row8 = objArrays;
                string row8Range = string.Concat("A8:", char.ConvertFromUtf32((int)Row8[0].Length + 64), "8");
                worksheet.Cells[row8Range].LoadFromArrays(Row8);
                double godzExtra2_suma = Convert.ToDouble(update.godzExtra2_pn) + Convert.ToDouble(update.godzExtra2_wt) + Convert.ToDouble(update.godzExtra2_sr) + Convert.ToDouble(update.godzExtra2_cz) + Convert.ToDouble(update.godzExtra2_pi) + Convert.ToDouble(update.godzExtra2_so);
                objArrays = new List<object[]>()
                {
                    new object[] { update.godzExtra2_nazwa, Convert.ToDouble(update.godzExtra2_pn), Convert.ToDouble(update.godzExtra2_wt), Convert.ToDouble(update.godzExtra2_sr), Convert.ToDouble(update.godzExtra2_cz), Convert.ToDouble(update.godzExtra2_pi), Convert.ToDouble(update.godzExtra2_so), godzExtra2_suma }
                };
                List<object[]> Row9 = objArrays;
                string row9Range = string.Concat("A9:", char.ConvertFromUtf32((int)Row9[0].Length + 64), "9");
                worksheet.Cells[row9Range].LoadFromArrays(Row9);
                double godzExtra3_suma = Convert.ToDouble(update.godzExtra3_pn) + Convert.ToDouble(update.godzExtra3_wt) + Convert.ToDouble(update.godzExtra3_sr) + Convert.ToDouble(update.godzExtra3_cz) + Convert.ToDouble(update.godzExtra3_pi) + Convert.ToDouble(update.godzExtra3_so);
                objArrays = new List<object[]>()
                {
                    new object[] { update.godzExtra3_nazwa, Convert.ToDouble(update.godzExtra3_pn), Convert.ToDouble(update.godzExtra3_wt), Convert.ToDouble(update.godzExtra3_sr), Convert.ToDouble(update.godzExtra3_cz), Convert.ToDouble(update.godzExtra3_pi), Convert.ToDouble(update.godzExtra3_so), godzExtra3_suma }
                };
                List<object[]> Row10 = objArrays;
                string row10Range = string.Concat("A10:", char.ConvertFromUtf32((int)Row10[0].Length + 64), "10");
                worksheet.Cells[row10Range].LoadFromArrays(Row10);
                double godzExtra4_suma = Convert.ToDouble(update.godzExtra4_pn) + Convert.ToDouble(update.godzExtra4_wt) + Convert.ToDouble(update.godzExtra4_sr) + Convert.ToDouble(update.godzExtra4_cz) + Convert.ToDouble(update.godzExtra4_pi) + Convert.ToDouble(update.godzExtra4_so);
                objArrays = new List<object[]>()
                {
                    new object[] { update.godzExtra4_nazwa, Convert.ToDouble(update.godzExtra4_pn), Convert.ToDouble(update.godzExtra4_wt), Convert.ToDouble(update.godzExtra4_sr), Convert.ToDouble(update.godzExtra4_cz), Convert.ToDouble(update.godzExtra4_pi), Convert.ToDouble(update.godzExtra4_so), godzExtra4_suma }
                };
                List<object[]> Row11 = objArrays;
                string row11Range = string.Concat("A11:", char.ConvertFromUtf32((int)Row11[0].Length + 64), "11");
                worksheet.Cells[row11Range].LoadFromArrays(Row11);
                objArrays = new List<object[]>()
                {
                    new object[] { "SUMA", Convert.ToDouble(update.godzPob_pn) + Convert.ToDouble(update.godzExtra1_pn) + Convert.ToDouble(update.godzExtra2_pn) + Convert.ToDouble(update.godzExtra3_pn) + Convert.ToDouble(update.godzExtra4_pn), Convert.ToDouble(update.godzPob_wt) + Convert.ToDouble(update.godzExtra1_wt) + Convert.ToDouble(update.godzExtra2_wt) + Convert.ToDouble(update.godzExtra3_wt) + Convert.ToDouble(update.godzExtra4_wt), Convert.ToDouble(update.godzPob_sr) + Convert.ToDouble(update.godzExtra1_sr) + Convert.ToDouble(update.godzExtra2_sr) + Convert.ToDouble(update.godzExtra3_sr) + Convert.ToDouble(update.godzExtra4_sr), Convert.ToDouble(update.godzPob_cz) + Convert.ToDouble(update.godzExtra1_cz) + Convert.ToDouble(update.godzExtra2_cz) + Convert.ToDouble(update.godzExtra3_cz) + Convert.ToDouble(update.godzExtra4_cz), Convert.ToDouble(update.godzPob_pi) + Convert.ToDouble(update.godzExtra1_pi) + Convert.ToDouble(update.godzExtra2_pi) + Convert.ToDouble(update.godzExtra3_pi) + Convert.ToDouble(update.godzExtra4_pi), Convert.ToDouble(update.godzPob_so) + Convert.ToDouble(update.godzExtra1_so) + Convert.ToDouble(update.godzExtra2_so) + Convert.ToDouble(update.godzExtra3_so) + Convert.ToDouble(update.godzExtra4_so) }
                };
                List<object[]> Row12 = objArrays;
                string row12Range = string.Concat("A12:", char.ConvertFromUtf32((int)Row12[0].Length + 64), "12");
                worksheet.Cells[row12Range].LoadFromArrays(Row12);
                worksheet.Cells[row12Range].Style.Font.Bold = true;
                objArrays = new List<object[]>()
                {
                    new object[] { "Godziny sprzętu", "Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota", "SUMA" }
                };
                List<object[]> Row13 = objArrays;
                string row13Range = string.Concat("A13:", char.ConvertFromUtf32((int)Row13[0].Length + 64), "13");
                worksheet.Cells[row13Range].LoadFromArrays(Row13);
                worksheet.Cells[row13Range].Style.Font.Bold = true;
                worksheet.Cells[row13Range].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[row13Range].Style.Fill.BackgroundColor.SetColor(sumRowColumnColor);
                double dzwig_suma = Convert.ToDouble(update.dzwig_pn) + Convert.ToDouble(update.dzwig_wt) + Convert.ToDouble(update.dzwig_sr) + Convert.ToDouble(update.dzwig_cz) + Convert.ToDouble(update.dzwig_pi) + Convert.ToDouble(update.dzwig_so);
                objArrays = new List<object[]>()
                {
                    new object[] { "Dźwig", Convert.ToDouble(update.dzwig_pn), Convert.ToDouble(update.dzwig_wt), Convert.ToDouble(update.dzwig_sr), Convert.ToDouble(update.dzwig_cz), Convert.ToDouble(update.dzwig_pi), Convert.ToDouble(update.dzwig_so), dzwig_suma }
                };
                List<object[]> Row14 = objArrays;
                string row14Range = string.Concat("A14:", char.ConvertFromUtf32((int)Row14[0].Length + 64), "14");
                worksheet.Cells[row14Range].LoadFromArrays(Row14);
                double cat_suma = Convert.ToDouble(update.cat_pn) + Convert.ToDouble(update.cat_wt) + Convert.ToDouble(update.cat_sr) + Convert.ToDouble(update.cat_cz) + Convert.ToDouble(update.cat_pi) + Convert.ToDouble(update.cat_so);
                objArrays = new List<object[]>()
                {
                    new object[] { "Cat", Convert.ToDouble(update.cat_pn), Convert.ToDouble(update.cat_wt), Convert.ToDouble(update.cat_sr), Convert.ToDouble(update.cat_cz), Convert.ToDouble(update.cat_pi), Convert.ToDouble(update.cat_so), cat_suma }
                };
                List<object[]> Row15 = objArrays;
                string row15Range = string.Concat("A15:", char.ConvertFromUtf32((int)Row15[0].Length + 64), "15");
                worksheet.Cells[row15Range].LoadFromArrays(Row15);
                double manitou_suma = Convert.ToDouble(update.manitou_pn) + Convert.ToDouble(update.manitou_wt) + Convert.ToDouble(update.manitou_sr) + Convert.ToDouble(update.manitou_cz) + Convert.ToDouble(update.manitou_pi) + Convert.ToDouble(update.manitou_so);
                objArrays = new List<object[]>()
                {
                    new object[] { "Manitou", Convert.ToDouble(update.manitou_pn), Convert.ToDouble(update.manitou_wt), Convert.ToDouble(update.manitou_sr), Convert.ToDouble(update.manitou_cz), Convert.ToDouble(update.manitou_pi), Convert.ToDouble(update.manitou_so), manitou_suma }
                };
                List<object[]> Row16 = objArrays;
                string row16Range = string.Concat("A16:", char.ConvertFromUtf32((int)Row16[0].Length + 64), "16");
                worksheet.Cells[row16Range].LoadFromArrays(Row16);
                double inne_suma = Convert.ToDouble(update.inne_pn) + Convert.ToDouble(update.inne_wt) + Convert.ToDouble(update.inne_sr) + Convert.ToDouble(update.inne_cz) + Convert.ToDouble(update.inne_pt) + Convert.ToDouble(update.inne_so);
                objArrays = new List<object[]>()
                {
                    new object[] { "Inne", Convert.ToDouble(update.inne_pn), Convert.ToDouble(update.inne_wt), Convert.ToDouble(update.inne_sr), Convert.ToDouble(update.inne_cz), Convert.ToDouble(update.inne_pt), Convert.ToDouble(update.inne_so), inne_suma }
                };
                List<object[]> Row17 = objArrays;
                string row17Range = string.Concat("A17:", char.ConvertFromUtf32((int)Row17[0].Length + 64), "17");
                worksheet.Cells[row17Range].LoadFromArrays(Row17);
                objArrays = new List<object[]>()
                {
                    new object[] { "Inne jakie?", update.notatki_pn, update.notatki_wt, update.notatki_sr, update.notatki_cz, update.notatki_pi, update.notatki_so }
                };
                List<object[]> Row18 = objArrays;
                string row18Range = string.Concat("A18:", char.ConvertFromUtf32((int)Row18[0].Length + 64), "18");
                worksheet.Cells[row18Range].LoadFromArrays(Row18);
                objArrays = new List<object[]>()
                {
                    new object[] { "Link do podpisu: ", string.Concat("dekadowki.poburski.pl/Content/signatures/", update.podpis) }
                };
                List<object[]> Row19 = objArrays;
                string row19Range = string.Concat("A19:", char.ConvertFromUtf32((int)Row19[0].Length + 64), "19");
                worksheet.Cells[row19Range].LoadFromArrays(Row19);
                worksheet.Cells.AutoFitColumns();
                string allRange = "A1:H18";
                worksheet.Cells[allRange].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                worksheet.Cells[allRange].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                worksheet.Cells[allRange].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                worksheet.Cells[allRange].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                worksheet.Cells[allRange].Style.Border.Bottom.Color.SetColor(Color.Black);
                worksheet.Cells[allRange].Style.Border.Top.Color.SetColor(Color.Black);
                worksheet.Cells[allRange].Style.Border.Right.Color.SetColor(Color.Black);
                worksheet.Cells[allRange].Style.Border.Left.Color.SetColor(Color.Black);
                worksheet.Cells["H1:H18"].Style.Font.Bold = true;
                object[] shortDateString = new object[] { string.Concat("rozliczenie_bud", update.brygadzista_id), update.budowa_id, update.tydzien_id, null };
                shortDateString[3] = DateTime.Now.ToShortDateString();
                path = string.Concat(path, string.Format("{0}-{1}-{2}-{3}.xlsx", shortDateString));
                excel.SaveAs(new FileInfo(path));
                foreach (string To in listaUserow)
                {
                    string Subject = string.Concat("Rozliczenie budowy - ", Help.GetNameAndSurname(update.brygadzista_id));
                    string Body = string.Format("<html><body><p>W załączniku raport z budowy.</p><p>Budowa: {0}</p><p>Tydzień: {1}</p><p>Brygadzista: {2}</p></body><html>", Help.GetBudowa(update.budowa_id).Nazwa, Help.GetTydzien(update.tydzien_id).getRokTydzien(), Help.GetNameAndSurname(update.brygadzista_id));
                    MailMessage msg = new MailMessage("mjedrzejczyk@wizualizacja.com", To, Subject, Body)
                    {
                        IsBodyHtml = true
                    };
                    try
                    {
                        msg.Attachments.Add(new Attachment(path));
                    }
                    catch (Exception exception)
                    {
                        Console.WriteLine(exception);
                    }
                    SmtpClient client = new SmtpClient()
                    {
                        UseDefaultCredentials = false,
                        Credentials = new NetworkCredential("mjedrzejczyk@wizualizacja.com", "WizMJ2018"),
                        Port = 587,
                        Host = "smtp.office365.com",
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        EnableSsl = true
                    };
                    try
                    {
                        client.Send(msg);
                    }
                    catch (Exception)
                    {
                    }
                    client.Dispose();
                    msg.Dispose();
                }
            }
        }

        public static void ZmienGodziny(string staryTydzien, int tydzien, int budowa, int brygadzista)
        {
            using (SqlConnection connection = new SqlConnection(Help.connectionString))
            {
                SqlCommand sqlCommand = new SqlCommand(string.Format("update AspNetGodziny set rokTydzien = '{0}' where rokTydzien = '{1}' and brygadzistaID = {2} and budowaID = {3}", new object[] { Help.GetTydzien(tydzien).getRokTydzien(), staryTydzien, brygadzista, budowa }), connection);
                connection.Open();
                sqlCommand.ExecuteNonQuery();
            }
        }

        public static string GetMiesiac(int m)
        {
            switch (m)
            {
                case 1: return "Styczeń";
                case 2: return "Luty";
                case 3: return "Marzec";
                case 4: return "Kwiecień";
                case 5: return "Maj";
                case 6: return "Czerwiec";
                case 7: return "Lipiec";
                case 8: return "Sierpień";
                case 9: return "Wrzesień";
                case 10: return "Październik";
                case 11: return "Listopad";
                default: return "Grudzień";
            }
        }

        public static List<DekadowkiMiesiacDlaPracownika> GetGodzinyNaDzienLista(int dzien_tygodnia, int idpracownika, int miesiac, int rok, string rokTydzien, string data)
        {
            List<DekadowkiMiesiacDlaPracownika> lista = new List<DekadowkiMiesiacDlaPracownika>();

            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
            List<AspNetGodziny> godziny = dc.AspNetGodzinies.Where(g => g.pracownikID == idpracownika && g.rokTydzien == rokTydzien).ToList();
            foreach (var g in godziny)
            {
                DekadowkiMiesiacDlaPracownika d = new DekadowkiMiesiacDlaPracownika();
                d.NazwaProjektu = GetNazwaBudowy(g.budowaID, false);
                d.DzienTygodniaInt = dzien_tygodnia;
                d.RokTydzien = rokTydzien;
                d.Data = data;
                try
                {
                    d.Dzien = DateTime.Parse(data).Day;
                }
                catch (Exception)
                {

                }
                d.Id = g.Id;

                switch (dzien_tygodnia)
                {
                    case 1:
                        d.Od = g.od_pn;
                        d.Do = g.do_pn;
                        d.Dojazd = g.dojazd_pn;
                        d.Dieta = GetDieta(Convert.ToInt32(g.dieta_pn));
                        d.Przestoje = g.przestoje_pn;
                        d.Rodzaj = g.obecny_pn;
                        d.Paragony = g.paragony_pn.ToString();
                        break;
                    case 2:
                        d.Od = g.od_wt;
                        d.Do = g.do_wt;
                        d.Dojazd = g.dojazd_wt;
                        d.Dieta = GetDieta(Convert.ToInt32(g.dieta_wt));
                        d.Przestoje = g.przestoje_wt;
                        d.Rodzaj = g.obecny_wt;
                        d.Paragony = g.paragony_wt.ToString();
                        break;
                    case 3:
                        d.Od = g.od_sr;
                        d.Do = g.do_sr;
                        d.Dojazd = g.dojazd_sr;
                        d.Dieta = GetDieta(Convert.ToInt32(g.dieta_sr));
                        d.Przestoje = g.przestoje_sr;
                        d.Rodzaj = g.obecny_sr;
                        d.Paragony = g.paragony_sr.ToString();
                        break;
                    case 4:
                        d.Od = g.od_cz;
                        d.Do = g.do_cz;
                        d.Dojazd = g.dojazd_cz;
                        d.Dieta = GetDieta(Convert.ToInt32(g.dieta_cz));
                        d.Przestoje = g.przestoje_cz;
                        d.Rodzaj = g.obecny_cz;
                        d.Paragony = g.paragony_cz.ToString();
                        break;
                    case 5:
                        d.Od = g.od_pi;
                        d.Do = g.do_pi;
                        d.Dojazd = g.dojazd_pi;
                        d.Dieta = GetDieta(Convert.ToInt32(g.dieta_pi));
                        d.Przestoje = g.przestoje_pi;
                        d.Rodzaj = g.obecny_pi;
                        d.Paragony = g.paragony_pi.ToString();
                        break;
                    case 6:
                        d.Od = g.od_so;
                        d.Do = g.do_so;
                        d.Dojazd = g.dojazd_so;
                        d.Dieta = GetDieta(Convert.ToInt32(g.dieta_so));
                        d.Przestoje = g.przestoje_so;
                        d.Rodzaj = g.obecny_so;
                        d.Paragony = g.paragony_so.ToString();
                        break;
                    default:
                        d.Od = g.od_ni;
                        d.Do = g.do_ni;
                        d.Dojazd = g.dojazd_ni;
                        d.Dieta = GetDieta(Convert.ToInt32(g.dieta_ni));
                        d.Przestoje = g.przestoje_ni;
                        d.Rodzaj = g.obecny_ni;
                        d.Paragony = g.paragony_ni.ToString();
                        break;
                }

                d.Godziny100 = ObliczGodziny(100, d.Od, d.Do, dzien_tygodnia, d.Data, dc);      //normalne
                d.Godziny150 = ObliczGodziny(150, d.Od, d.Do, dzien_tygodnia, d.Data, dc);      //150%
                d.Godziny200 = ObliczGodziny(200, d.Od, d.Do, dzien_tygodnia, d.Data, dc);      //200%
                d.GodzinyNocne = ObliczGodziny(120, d.Od, d.Do, dzien_tygodnia, d.Data, dc);    //nocne

                d.Dojazd = d.Dojazd == null ? 0.ToString() : d.Dojazd;
                d.Przestoje = d.Przestoje == null ? 0.ToString() : d.Przestoje;

                lista.Add(d);
            }
            return lista;
        }

        private static string ObliczGodziny(int rodzaj_godzin, string od, string @do, int dzien_tygodnia, string data, pobiNTRANETDataContext dc)
        {
            string wartosc = "";
            float from = float.Parse(od.Replace(".3", ".5").Replace(".15", ".25").Replace(".45", ".75"));
            float to = float.Parse(@do.Replace(".3", ".5").Replace(".15", ".25").Replace(".45", ".75"));

            if (to == 0 && from != 0)
            {
                to = 24;
            }

            float roznica = to - from;

            var swieto = dc.lata.Where(l => l.data == DateTime.Parse(data)).First().swieto;
            bool czySwieto = swieto == null || swieto == false ? false : true;

            switch (rodzaj_godzin)
            {
                case 100:
                    if (czySwieto)
                    {
                        wartosc = "0";
                    }
                    else if (dzien_tygodnia < 6)
                    {
                        wartosc = roznica <= 8 ? Math.Round(roznica, 2).ToString() : "8";
                    }
                    else
                    {
                        wartosc = "0";
                    }
                    break;
                case 150:
                    if (czySwieto)
                    {
                        wartosc = "0";
                    }
                    else if (dzien_tygodnia < 6)
                    {
                        wartosc = roznica > 8 ? (Math.Round(roznica - 8, 2)).ToString() : "0";
                    }
                    else if (dzien_tygodnia == 6)
                    {
                        wartosc = Math.Round(roznica, 2).ToString();
                    }
                    else
                    {
                        wartosc = "0";
                    }
                    break;
                case 200:
                    if (dzien_tygodnia == 7 || czySwieto)
                    {
                        wartosc = Math.Round(roznica, 2).ToString();
                    }
                    else
                    {
                        wartosc = "0";
                    }
                    break;
                case 120:
                    float g_nocne = 0;
                    var parametry = dc.parametryPracownicies.Where(pp => DateTime.Parse(data) >= pp.od && DateTime.Parse(data) <= pp.@do).First();
                    float od_nocne = (float)parametry.czas_pracy_od;
                    float do_nocne = (float)parametry.czas_pracy_do;
                    if (from == to)
                    {
                        g_nocne = 0;
                    }
                    else
                    {
                        if (from < od_nocne)
                        {
                            g_nocne += od_nocne - from;
                            if (to < od_nocne)
                            {
                                g_nocne -= to;
                            }
                        }
                        if (to > do_nocne)
                        {
                            g_nocne += to - do_nocne;
                            if (from > do_nocne)
                            {
                                g_nocne += do_nocne - from;
                            }
                        }
                    }

                    wartosc = g_nocne.ToString();
                    break;
                default:
                    wartosc = "-";
                    break;
            }

            wartosc = wartosc.Replace(".3", ".5").Replace(".15", ".25").Replace(".45", ".75");

            return wartosc;
        }

        public static string GetGodzinyNocneTekst(string data)
        {
            try
            {
                pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
                parametryPracownicy p = dc.parametryPracownicies.Where(pp => DateTime.Parse(data) > pp.od && pp.@do > DateTime.Parse(data)).First();

                return string.Format("Godziny pracy od {0} do {1}. Poza {0}-{1} to godziny nocne.", p.czas_pracy_od, p.czas_pracy_do);
            }
            catch (Exception)
            {

            }
            return "";
        }

        public static bool CzySwieto(string data)
        {
            return new pobiNTRANETDataContext().lata.Where(l => l.data == DateTime.Parse(data) && l.swieto == true).Any();
        }

        public static lata GetSwieto(string data)
        {
            return new pobiNTRANETDataContext().lata.Where(l => l.data == DateTime.Parse(data) && l.swieto == true).First();
        }



        //internal static void SendContactFormEmail(string userName, string userEmail, string userPhone, string message, string locale)
        //{
        //    using (MailMessage mailMessage = new MailMessage())
        //    {
        //        mailMessage.From = new MailAddress("contact@knxenergy.eu");
        //        mailMessage.Subject = "Formularz kontaktowy KNX Energy";
        //        string content = string.Format(@"
        //            <img src='https://knxenergy.eu/wp-content/plugins/wsh/public/gfx/logo.png' width='198' height='30'/>
        //            <h2>Formularz kontaktowy KNX Energy - język: {4}</h2>
        //            <p>Email: {1}</p>
        //            <p>Imię i nazwisko: {0}</p>
        //            <p>Telefon: {2}</p>
        //            <p>Wiadomość: {3}</p>
        //        ", userName, userEmail, userPhone, message, locale);
        //        mailMessage.Body = content;
        //        mailMessage.IsBodyHtml = true;
        //        mailMessage.To.Add(new MailAddress("biuro@wizualizacja.com"));
        //        SmtpClient smtp = new SmtpClient()
        //        {
        //            Host = "knxenergy.eu",
        //            EnableSsl = true
        //        };
        //        NetworkCredential NetworkCred = new NetworkCredential()
        //        {
        //            UserName = "contact@knxenergy.eu",
        //            Password = "EJ16hwRlOSdB6vA"
        //        };
        //        smtp.UseDefaultCredentials = true;
        //        smtp.Credentials = NetworkCred;
        //        smtp.Port = 465;
        //        smtp.Send(mailMessage);
        //    }
        //}

        internal static void SendContactFormEmail(string data)
        {
            ContactFormFields m = JsonConvert.DeserializeObject<ContactFormFields>(data);
            var userName = m.userName;
            var userEmail = m.userEmail;
            var userPhone = m.userPhone;
            var message = m.message;
            var locale = m.locale;

            using (MailMessage mailMessage = new MailMessage())
            {
                mailMessage.From = new MailAddress("contact@knxenergy.eu");
                mailMessage.Subject = "Formularz kontaktowy KNX Energy";
                string content = string.Format(@"
                    <img src='https://knxenergy.eu/wp-content/plugins/wsh/public/gfx/logo.png' width='198' height='30'/>
                    <h2>Formularz kontaktowy KNX Energy - język: {4}</h2>
                    <p>Email: {1}</p>
                    <p>Imię i nazwisko: {0}</p>
                    <p>Telefon: {2}</p>
                    <p>Wiadomość: {3}</p>
                ", userName, userEmail, userPhone, message, locale);
                mailMessage.Body = content;
                mailMessage.IsBodyHtml = true;
                //mailMessage.To.Add(new MailAddress("biuro@wizualizacja.com"));
                mailMessage.To.Add(new MailAddress("mjedrzejczyk@poczta.wizualizacja.eu"));
                SmtpClient smtp = new SmtpClient()
                {
                    Host = "knxenergy.eu",
                    EnableSsl = false
                };
                NetworkCredential NetworkCred = new NetworkCredential()
                {
                    UserName = "contact@knxenergy.eu",
                    Password = "EJ16hwRlOSdB6vA"
                };
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = 587;
                smtp.Send(mailMessage);
            }
        }
        public class ContactFormFields
        {
            public string userName { get; set; }
            public string userEmail { get; set; }
            public string userPhone { get; set; }
            public string message { get; set; }
            public string locale { get; set; }
        }
    }
}