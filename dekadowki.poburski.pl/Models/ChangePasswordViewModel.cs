﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

namespace dekadowki.poburski.pl.Models
{
    public class ChangePasswordViewModel
    {
        [Compare("NewPassword", ErrorMessage = "Hasła się nie zgadzają.")]
        [DataType(DataType.Password)]
        [Display(Name = "Potwierdź hasło")]
        public string ConfirmPassword
        {
            get;
            set;
        }

        [DataType(DataType.Password)]
        [Display(Name = "Nowe hasło")]
        [Required]
        [StringLength(100, ErrorMessage = "{0} musi mieć conajmniej {2} długości.", MinimumLength = 6)]
        public string NewPassword
        {
            get;
            set;
        }

        [DataType(DataType.Password)]
        [Display(Name = "Hasło")]
        [Required]
        public string OldPassword
        {
            get;
            set;
        }

        public ChangePasswordViewModel()
        {
        }
    }
}