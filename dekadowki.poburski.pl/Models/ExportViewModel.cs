﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace dekadowki.poburski.pl.Models
{
    public class ExportViewModel
    {
        public List<Brygadzista> ListaBrygadzistow
        {
            get;
            set;
        }

        public List<string> TygodnieDoAkceptacji
        {
            get;
            set;
        }

        public ExportViewModel(List<Brygadzista> lista, List<string> tygodnie)
        {
            this.ListaBrygadzistow = lista;
            this.TygodnieDoAkceptacji = tygodnie;
        }
    }
}