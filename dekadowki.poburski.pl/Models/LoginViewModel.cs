﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

namespace dekadowki.poburski.pl.Models
{
    public class LoginViewModel
    {
        [Display(Name = "Email")]
        [EmailAddress]
        [Required]
        public string Email
        {
            get;
            set;
        }

        [DataType(DataType.Password)]
        [Display(Name = "Hasło")]
        [Required]
        public string Password
        {
            get;
            set;
        }

        [Display(Name = "Zapamiętaj mnie")]
        public bool RememberMe
        {
            get;
            set;
        }

        public LoginViewModel()
        {
        }
    }
}