﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace dekadowki.poburski.pl.Models
{
    public class DashboardViewModel
    {
        public List<Brygada> Brygada
        {
            get;
            set;
        }

        public Brygada Brygadzista
        {
            get;
            set;
        }

        public List<DniTygodnia> DniTygodnia
        {
            get;
            set;
        }

        public Tydzien Tydzien
        {
            get;
            set;
        }

        public DashboardViewModel()
        {
            this.Brygada = new List<Brygada>();
            this.DniTygodnia = new List<DniTygodnia>();
            this.Brygadzista = new Brygada();
        }
    }
}