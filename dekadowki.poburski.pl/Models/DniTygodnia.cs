﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

namespace dekadowki.poburski.pl.Models
{
    public class DniTygodnia
    {
        public string data_cz
        {
            get;
            set;
        }

        public string data_ni
        {
            get;
            set;
        }

        public string data_pi
        {
            get;
            set;
        }

        public string data_pn
        {
            get;
            set;
        }

        public string data_so
        {
            get;
            set;
        }

        public string data_sr
        {
            get;
            set;
        }

        public string data_wt
        {
            get;
            set;
        }

        [Display(Name = "Dieta(Czwartek)")]
        public int dieta_cz
        {
            get;
            set;
        }

        [Display(Name = "Dieta(Niedziela)")]
        public int dieta_ni
        {
            get;
            set;
        }

        [Display(Name = "Dieta(Piątek)")]
        public int dieta_pi
        {
            get;
            set;
        }

        [Display(Name = "Dieta(Poniedziałek)")]
        public int dieta_pn
        {
            get;
            set;
        }

        [Display(Name = "Dieta(Sobota)")]
        public int dieta_so
        {
            get;
            set;
        }

        [Display(Name = "Dieta(Środa)")]
        public int dieta_sr
        {
            get;
            set;
        }

        [Display(Name = "Dieta(Wtorek)")]
        public int dieta_wt
        {
            get;
            set;
        }

        [Display(Name = "Do(Czwartek)")]
        public string do_cz
        {
            get;
            set;
        }

        [Display(Name = "Do(Niedziela)")]
        public string do_ni
        {
            get;
            set;
        }

        [Display(Name = "Do(Piątek)")]
        public string do_pi
        {
            get;
            set;
        }

        [Display(Name = "Do(Poniedziałek)")]
        public string do_pn
        {
            get;
            set;
        }

        [Display(Name = "Do(Sobota)")]
        public string do_so
        {
            get;
            set;
        }

        [Display(Name = "Do(Środa)")]
        public string do_sr
        {
            get;
            set;
        }

        [Display(Name = "Do(Wtorek)")]
        public string do_wt
        {
            get;
            set;
        }

        [Display(Name = "Dojazd(Czwartek)")]
        public string dojazd_cz
        {
            get;
            set;
        }

        [Display(Name = "Dojazd(Niedziela)")]
        public string dojazd_ni
        {
            get;
            set;
        }

        [Display(Name = "Dojazd(Piątek)")]
        public string dojazd_pi
        {
            get;
            set;
        }

        [Display(Name = "Dojazd(Poniedziałek)")]
        public string dojazd_pn
        {
            get;
            set;
        }

        [Display(Name = "Dojazd(Sobota)")]
        public string dojazd_so
        {
            get;
            set;
        }

        [Display(Name = "Dojazd(Środa)")]
        public string dojazd_sr
        {
            get;
            set;
        }

        [Display(Name = "Dojazd(Wtorek)")]
        public string dojazd_wt
        {
            get;
            set;
        }

        [Display(Name = "Obecny")]
        public string obecny
        {
            get;
            set;
        }

        [Display(Name = "Od(Czwartek)")]
        public string od_cz
        {
            get;
            set;
        }

        [Display(Name = "Od(Niedziela)")]
        public string od_ni
        {
            get;
            set;
        }

        [Display(Name = "Od(Piątek)")]
        public string od_pi
        {
            get;
            set;
        }

        [Display(Name = "Od(Poniedziałek)")]
        public string od_pn
        {
            get;
            set;
        }

        [Display(Name = "Od(Sobota)")]
        public string od_so
        {
            get;
            set;
        }

        [Display(Name = "Od(Środa)")]
        public string od_sr
        {
            get;
            set;
        }

        [Display(Name = "Od(Wtorek)")]
        public string od_wt
        {
            get;
            set;
        }

        [Display(Name = "Przestoje(Czwartek)")]
        public string przestoje_cz
        {
            get;
            set;
        }

        [Display(Name = "Przestoje(Niedziela)")]
        public string przestoje_ni
        {
            get;
            set;
        }

        [Display(Name = "Przestoje(Piątek)")]
        public string przestoje_pi
        {
            get;
            set;
        }

        [Display(Name = "Przestoje(Poniedziałek)")]
        public string przestoje_pn
        {
            get;
            set;
        }

        [Display(Name = "Przestoje(Sobota)")]
        public string przestoje_so
        {
            get;
            set;
        }

        [Display(Name = "Przestoje(Środa)")]
        public string przestoje_sr
        {
            get;
            set;
        }

        [Display(Name = "Przestoje(Wtorek)")]
        public string przestoje_wt
        {
            get;
            set;
        }

        public string obecny_pn
        {
            get;
            set;
        }

        public string obecny_wt
        {
            get;
            set;
        }

        public string obecny_sr
        {
            get;
            set;
        }

        public string obecny_cz
        {
            get;
            set;
        }

        public string obecny_pi
        {
            get;
            set;
        }

        public string obecny_so
        {
            get;
            set;
        }

        public string obecny_ni
        {
            get;
            set;
        }

        public string paragony_po
        {
            get;
            set;
        }

        public string paragony_wt
        {
            get;
            set;
        }

        public string paragony_sr
        {
            get;
            set;
        }

        public string paragony_cz
        {
            get;
            set;
        }

        public string paragony_pi
        {
            get;
            set;
        }

        public string paragony_so
        {
            get;
            set;
        }

        public string paragony_ni
        {
            get;
            set;
        }

        public DniTygodnia()
        {
        }
    }
}