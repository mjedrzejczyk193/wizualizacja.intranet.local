﻿using System;
using System.Runtime.CompilerServices;

namespace dekadowki.poburski.pl.Models
{
    public class Brygada
    {
        public string Brygadzista
        {
            get;
            set;
        }

        public int BrygadzistaId
        {
            get;
            set;
        }

        public int Id
        {
            get;
            set;
        }

        public string Nazwa
        {
            get;
            set;
        }

        public int Stanowisko
        {
            get;
            set;
        }

        public Brygada()
        {
        }

        public Brygada(string nazwa, int id, string brygadzista, int brygadzistaId)
        {
            this.Nazwa = nazwa;
            this.Id = id;
            this.Brygadzista = brygadzista;
            this.BrygadzistaId = brygadzistaId;
            this.Stanowisko = Help.GetStanowisko(id);
        }

        public int getBrygadzistaId()
        {
            return this.BrygadzistaId;
        }

        public string getBrygadzistaNazwa()
        {
            return this.Brygadzista;
        }

        public int getId()
        {
            return this.Id;
        }

        public string getNazwa()
        {
            return this.Nazwa;
        }
    }
}