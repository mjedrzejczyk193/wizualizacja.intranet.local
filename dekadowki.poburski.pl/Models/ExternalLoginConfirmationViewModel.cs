﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

namespace dekadowki.poburski.pl.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Display(Name = "Email")]
        [Required]
        public string Email
        {
            get;
            set;
        }

        public ExternalLoginConfirmationViewModel()
        {
        }
    }
}