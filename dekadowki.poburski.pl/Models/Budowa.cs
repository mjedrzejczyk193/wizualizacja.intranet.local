﻿using System;
using System.Runtime.CompilerServices;

namespace dekadowki.poburski.pl.Models
{
    public class Budowa
    {
        public int Id
        {
            get;
            set;
        }

        public string Nazwa
        {
            get;
            set;
        }

        public string Numer
        {
            get;
            set;
        }

        public Budowa(int id, string nazwa, string numer)
        {
            this.Id = id;
            this.Nazwa = nazwa;
            this.Numer = numer;
        }
    }
}