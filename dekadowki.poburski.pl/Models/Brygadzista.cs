﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

namespace dekadowki.poburski.pl.Models
{
    public class Brygadzista
    {
        public int id
        {
            get;
            set;
        }

        [Display(Name = "Brygadzista")]
        public string nazwa
        {
            get;
            set;
        }

        public Brygadzista(int id, string nazwa)
        {
            this.id = id;
            this.nazwa = nazwa;
        }

        public int GetId()
        {
            return this.id;
        }

        public string GetName()
        {
            return this.nazwa;
        }
    }
}