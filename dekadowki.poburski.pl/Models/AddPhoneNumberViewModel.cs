﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

namespace dekadowki.poburski.pl.Models
{
    public class AddPhoneNumberViewModel
    {
        [Display(Name = "Phone Number")]
        [Phone]
        [Required]
        public string Number
        {
            get;
            set;
        }

        public AddPhoneNumberViewModel()
        {
        }
    }
}