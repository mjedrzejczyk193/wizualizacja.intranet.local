﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

namespace dekadowki.poburski.pl.Models
{
    public class ResetPasswordViewModel
    {
        public string Code
        {
            get;
            set;
        }

        [Compare("Password", ErrorMessage = "Hasła się nie zgadzają.")]
        [DataType(DataType.Password)]
        [Display(Name = "Potwierdź hasło")]
        public string ConfirmPassword
        {
            get;
            set;
        }

        [Display(Name = "Email")]
        [EmailAddress]
        [Required]
        public string Email
        {
            get;
            set;
        }

        [DataType(DataType.Password)]
        [Display(Name = "Hasło")]
        [Required]
        [StringLength(100, ErrorMessage = "{0} musi mieć conajmniej {2} długości.", MinimumLength = 4)]
        public string Password
        {
            get;
            set;
        }

        public ResetPasswordViewModel()
        {
        }
    }
}