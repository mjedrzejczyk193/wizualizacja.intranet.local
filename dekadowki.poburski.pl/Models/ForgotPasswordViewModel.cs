﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

namespace dekadowki.poburski.pl.Models
{
    public class ForgotPasswordViewModel
    {
        [Display(Name = "Email")]
        [EmailAddress]
        [Required]
        public string Email
        {
            get;
            set;
        }

        public ForgotPasswordViewModel()
        {
        }
    }
}