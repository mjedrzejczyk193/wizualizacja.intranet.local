﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

namespace dekadowki.poburski.pl.Models
{
    public class VerifyCodeViewModel
    {
        [Display(Name = "Kod")]
        [Required]
        public string Code
        {
            get;
            set;
        }

        [Required]
        public string Provider
        {
            get;
            set;
        }

        [Display(Name = "Zapamiętaj przeglądarkę")]
        public bool RememberBrowser
        {
            get;
            set;
        }

        public bool RememberMe
        {
            get;
            set;
        }

        public string ReturnUrl
        {
            get;
            set;
        }

        public VerifyCodeViewModel()
        {
        }
    }
}