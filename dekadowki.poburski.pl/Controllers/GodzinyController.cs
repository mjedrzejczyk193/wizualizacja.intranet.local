﻿using dekadowki.poburski.pl.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace dekadowki.poburski.pl.Controllers
{
    public class GodzinyController : Controller
    {
        // GET: Godziny
        public ActionResult Index()
        {
            return View();
        }
        [Authorize]
        public ActionResult GodzinySprzetu(string data, int? pracownik)
        {
            if (pracownik == null)
            {
                pracownik = Help.GetIdIntranetFromDb(User.Identity.Name);
            }

            if (string.IsNullOrEmpty(data))
            {
                data = DateTime.Now.ToString("MM-yyyy");
            }
            ViewBag.Data = data;
            ViewBag.Pracownik = pracownik;

            int miesiac = DateTime.Parse("01-" + data).Month;
            int rok = DateTime.Parse("01-" + data).Year;

            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
            List<lata> dni = dc.lata.Where(l => l.rok == rok && l.miesiac == miesiac).OrderBy(l => l.data).ToList();
            ViewBag.Dni = dni;

            List<dekadowki_godziny> dane = new List<dekadowki_godziny>();
            foreach (var d in dni)
            {
                dekadowki_godziny dg = new dekadowki_godziny();
                dg.data = d.data;
                dg.dzwig = 0;
                dg.godziny_poburski = 0;
                dg.idpracownika = Convert.ToInt32(pracownik);
                dg.kat = 0;
                dg.manitou = 0;
                dg.polaczenia = 0;
                dg.powierzchnia = 0;
                dg.przejscia = 0;
                dg.uwagi = "";
                dg.data = DateTime.Now;

                dane.Add(dg);
            }
            int edycja = 0;
            //Wczytanie z bazy
            try
            {
                List<dekadowki_godziny> baza = dc.dekadowki_godziny.Where(dg => dg.data.Value.Month == miesiac && dg.data.Value.Year == rok && dg.idpracownika == pracownik).ToList();
                if (baza.Count > 0)
                {
                    dane = baza;
                    edycja = 1;
                }
            }
            catch (Exception)
            {
            }

            ViewBag.Edycja = edycja;

            dekadowki_godziny pod = new dekadowki_godziny();
            pod.idpracownika = Convert.ToInt32(pracownik);
            pod.dzwig = dane.Sum(d => d.dzwig);
            pod.godziny_poburski = dane.Sum(d => d.godziny_poburski);
            pod.godziny_podwykonawcy = dane.Sum(d => d.godziny_podwykonawcy);
            pod.kat = dane.Sum(d => d.kat);
            pod.manitou = dane.Sum(d => d.manitou);
            pod.polaczenia = dane.Sum(d => d.polaczenia);
            pod.powierzchnia = dane.Sum(d => d.powierzchnia);
            pod.przejscia = dane.Sum(d => d.przejscia);
            pod.uwagi = "";
            pod.data = DateTime.Now;
            ViewBag.Podsumowanie = pod;

            ViewBag.Budowy = Help.GetBudowyTrwajace();
            ViewBag.Akord = dc.akord_godzinies.Where(a => a.rok == rok && a.miesiac == miesiac && a.id_prac == pracownik).ToList();

            GodzinyViewModel g = new GodzinyViewModel();
            g.dni = dane;
            return View(g);
        }

        [Authorize]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult DodajGodziny(GodzinyViewModel godziny, int? edycja)
        {
            if(edycja == null)
            {
                edycja = 0;
            }    
            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();

            var rok = godziny.dni[0].data.Value.Year;
            var miesiac = godziny.dni[0].data.Value.Month;
            var pracownik = godziny.dni[0].idpracownika;
            List<dekadowki_godziny> baza = dc.dekadowki_godziny.Where(dg => dg.data.Value.Month == miesiac && dg.data.Value.Year == rok && dg.idpracownika == pracownik).ToList();
            if (edycja == 1)
            {
                foreach (var d in baza)
                {
                    dekadowki_godziny biezace = godziny.dni.Where(g => g.data == d.data).FirstOrDefault();
                    d.dzwig = biezace.dzwig;
                    d.godziny_poburski = biezace.godziny_poburski;
                    d.godziny_podwykonawcy = biezace.godziny_podwykonawcy;
                    d.idbudowy = biezace.idbudowy;
                    d.kat = biezace.kat;
                    d.manitou = biezace.manitou;
                    d.polaczenia = biezace.polaczenia;
                    d.powierzchnia = biezace.powierzchnia;
                    d.przejscia = biezace.przejscia;
                    d.uwagi = biezace.uwagi;
                    d.idbudowy = biezace.idbudowy;
                    d.dataEdycji = DateTime.Now;
                }

                dc.SubmitChanges();
            }
            else
            {
                #region Jeśli istnieje = usuwamy
                try
                {
                    if (baza.Count > 0)
                    {
                        dc.dekadowki_godziny.DeleteAllOnSubmit(baza);
                        dc.SubmitChanges();
                    }
                }
                catch (Exception)
                {
                }
                #endregion

                foreach (var d in godziny.dni)
                {
                    d.dataWprowadzenia = DateTime.Now;
                }
                dc.dekadowki_godziny.InsertAllOnSubmit(godziny.dni);
                dc.SubmitChanges();
            }

            return RedirectToAction("GodzinySprzetu", "Godziny");
        }
    }
}