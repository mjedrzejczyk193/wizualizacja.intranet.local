﻿using dekadowki.poburski.pl.Models;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;

namespace dekadowki.poburski.pl.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public HomeController()
        {
        }

        [HttpPost]
        public ActionResult Add(int? answer)
        {
            HttpSessionStateBase session;
            try
            {
                int index = Convert.ToInt32(answer);
                if (index != 0)
                {
                    List<Brygada> brygada = (base.Session["NowaBrygada"] != null ? (List<Brygada>)base.Session["NowaBrygada"] : Help.GetBrygada(Help.GetBrygadzistaID(index), true));
                    brygada.Add(new Brygada(Help.GetNameAndSurname(index), index, Help.GetBrygadzistaNazwa(index), Help.GetBrygadzistaID(index)));
                    base.Session["NowaBrygada"] = (
                        from b in brygada
                        orderby b.Stanowisko, b.getNazwa()
                        select b).ToList<Brygada>();
                }
                else
                {
                    session = base.Session;
                    session["AdditionalMessage"] = string.Concat(session["AdditionalMessage"], string.Format("Pracownik nie istieje w bazie. Nie można wpisać pracownika z palca, trzeba go wybrać. Skontaktuj się z działem personalnym aby dodał tego pracownika do Twojej brygady.", new object[0]));
                }
            }
            catch (Exception)
            {
                session = base.Session;
                session["AdditionalMessage"] = string.Concat(session["AdditionalMessage"], string.Format("Pracownik nie istieje w bazie. Skontaktuj się z działem personalnym aby dodał tego pracownika do Twojej brygady.", new object[0]));
            }
            return base.RedirectToAction("Index");
        }

        public ActionResult Blockade(int year)
        {
            ((dynamic)base.ViewBag).Lata = Help.GetLata();
            return base.View(Help.GetTygodnieZablokowaneLubNie(year));
        }

        [HttpPost]
        public void ChangeWeek(int tydzien)
        {
            base.Session["Tydzien"] = Help.GetTydzien(tydzien);
            base.Response.Cookies.Add(new HttpCookie("Tydzien", Help.GetTydzien(tydzien).getRokTydzien()));
        }

        public ActionResult Delete(int idpracownika)
        {
            try
            {
                List<Brygada> brygada = Help.NowaBrygadaDelete(idpracownika, (base.Session["NowaBrygada"] != null ? (List<Brygada>)base.Session["NowaBrygada"] : Help.GetBrygada(Help.GetBrygadzistaID(idpracownika), true)));
                base.Session["NowaBrygada"] = (
                    from b in brygada
                    orderby b.Stanowisko, b.getNazwa()
                    select b).ToList<Brygada>();
            }
            catch (Exception)
            {
            }
            return base.RedirectToAction("Index");
        }

        public ActionResult DeleteAll()
        {
            List<Brygada> item;
            if (base.Session["NowaBrygada"] != null)
            {
                item = (List<Brygada>)base.Session["NowaBrygada"];
            }
            else
            {
                item = null;
            }
            List<Brygada> brygada = item;
            if (brygada != null)
            {
                List<Brygada> brygadaLista = new List<Brygada>()
                {
                    brygada[0]
                };
                base.Session["NowaBrygada"] = brygadaLista;
            }
            return base.RedirectToAction("Index");
        }

        public ActionResult DeleteGodziny(int id)
        {
            try
            {
                pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
                AspNetGodziny aspnetgodziny = dc.AspNetGodzinies.First<AspNetGodziny>((AspNetGodziny g) => g.Id == id);
                dc.AspNetGodzinies.DeleteOnSubmit(aspnetgodziny);
                dc.SubmitChanges();
            }
            catch (Exception)
            {

            }
            return base.RedirectToAction("Preview", "Home");
        }

        public ActionResult DeleteGodzinyNew(int id)
        {
            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
            AspNetGodziny aspnetgodziny = dc.AspNetGodzinies.First<AspNetGodziny>((AspNetGodziny g) => g.Id == id);
            dc.AspNetGodzinies.DeleteOnSubmit(aspnetgodziny);
            dc.SubmitChanges();
            return base.RedirectToAction("PreviewNew", "Home");
        }

        public ActionResult DeleteGodzinyMonth(int id, int dzien_tygodnia, DateTime data, int idpracownika)
        {
            try
            {
                pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
                AspNetGodziny g = dc.AspNetGodzinies.First<AspNetGodziny>((AspNetGodziny godz) => godz.Id == id);
                switch (dzien_tygodnia)
                {
                    case 1:
                        g.dieta_pn = "0";
                        g.dojazd_pn = "0";
                        g.do_pn = "00.00";
                        g.obecny_pn = "brak";
                        g.przestoje_pn = "0";
                        g.od_pn = "00.00";
                        break;
                    case 2:
                        g.dieta_wt = "0";
                        g.dojazd_wt = "0";
                        g.do_wt = "00.00";
                        g.obecny_wt = "brak";
                        g.przestoje_wt = "0";
                        g.od_wt = "00.00";
                        break;
                    case 3:
                        g.dieta_sr = "0";
                        g.dojazd_sr = "0";
                        g.do_sr = "00.00";
                        g.obecny_sr = "brak";
                        g.przestoje_sr = "0";
                        g.od_sr = "00.00";
                        break;
                    case 4:
                        g.dieta_cz = "0";
                        g.dojazd_cz = "0";
                        g.do_cz = "00.00";
                        g.obecny_cz = "brak";
                        g.przestoje_cz = "0";
                        g.od_cz = "00.00";
                        break;
                    case 5:
                        g.dieta_pi = "0";
                        g.dojazd_pi = "0";
                        g.do_pi = "00.00";
                        g.obecny_pi = "brak";
                        g.przestoje_pi = "0";
                        g.od_pi = "00.00";
                        break;
                    case 6:
                        g.dieta_so = "0";
                        g.dojazd_so = "0";
                        g.do_so = "00.00";
                        g.obecny_so = "brak";
                        g.przestoje_so = "0";
                        g.od_so = "00.00";
                        break;
                    case 7:
                        g.dieta_ni = "0";
                        g.dojazd_ni = "0";
                        g.do_ni = "00.00";
                        g.obecny_ni = "brak";
                        g.przestoje_ni = "0";
                        g.od_ni = "00.00";
                        break;
                    default:
                        break;
                }
                dc.SubmitChanges();
            }
            catch (Exception)
            {

            }
            
            return base.RedirectToAction("ShowMonth", "Dashboard", new { idp = idpracownika, m = data.Month, r = data.Year });
        }

        public ActionResult EditGodziny(int id, bool? print, bool? czyInnaZmiana, int idpracownika = 0, int dzien_tygodnia = 0, int tabela = 0, string dzien = null)
        {
            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
            AspNetGodziny aspNetGodziny = dc.AspNetGodzinies.First<AspNetGodziny>((AspNetGodziny godz) => godz.Id == id);
            Session["CurrentId"] = id;
            DashboardViewModel dvm = new DashboardViewModel()
            {
                Tydzien = Help.GetTydzien(aspNetGodziny.rokTydzien)
            };
            pracownik brygadzista = dc.pracownik.FirstOrDefault<pracownik>((pracownik p) => p.idpracownika == aspNetGodziny.pracownikID);
            dvm.Brygadzista = new Brygada(Help.GetNameAndSurname(brygadzista.idpracownika), brygadzista.idpracownika, "", 0);
            Budowa currentBudowa = Help.GetBudowa(aspNetGodziny.budowaID);
            dvm.Brygada = Help.GetBrygadaZGodzin(aspNetGodziny.brygadzistaID, currentBudowa, dvm.Tydzien);
            dvm.DniTygodnia = Help.GetDniTygodnia(dvm.Brygada.Count, dvm.Tydzien, dvm.Brygada[0].Id, dvm.Brygada, currentBudowa.Id, Convert.ToBoolean(czyInnaZmiana));
            ((dynamic)base.ViewBag).WybranaBudowa = currentBudowa;
            ((dynamic)base.ViewBag).Budowy = Help.GetBudowy();
            ((dynamic)base.ViewBag).Podpisy = (dynamic)Help.GetPodpisy(dvm, currentBudowa);
            base.Session["Tydzien"] = dvm.Tydzien;
            base.Response.Cookies.Add(new HttpCookie(dvm.Tydzien.getRokTydzien()));
            base.Session["DVM"] = dvm;
            base.Session["Budowa"] = currentBudowa;
            base.Session["NowaBrygada"] = dvm.Brygada;
            string ms = "";
            string cms = "";
            string s = "";

            Session["Pracownik"] = idpracownika;
            Session["DzienTygodnia"] = dzien_tygodnia;
            return base.RedirectToAction("Index2", "Dashboard", new { message = ms, colorMessage = cms, serwis = s, drukuj = print, tabela = tabela, dzien = dzien });
        }

        public ActionResult EksportGodziny(int id)
        {
            int num;
            int num1;
            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
            AspNetGodziny aspnetgodziny = dc.AspNetGodzinies.First<AspNetGodziny>((AspNetGodziny g) => g.Id == id);
            AspNetGodziny aspNetGodziny = aspnetgodziny;
            if (aspNetGodziny.export == 0)
            {
                int num2 = 1;
                num = num2;
                aspnetgodziny.export = num2;
                num1 = num;
            }
            else
            {
                int num3 = 0;
                num = num3;
                aspnetgodziny.export = num3;
                num1 = num;
            }
            aspNetGodziny.export = num1;
            dc.SubmitChanges();
            base.Session["Zmiana"] = "Eksport";
            base.Session["ZmienioneID"] = id;
            return base.RedirectToAction("Preview", "Home");
        }

        public ActionResult EksportGodzinyNew(int id)
        {
            int num;
            int num1;
            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
            AspNetGodziny aspnetgodziny = dc.AspNetGodzinies.First<AspNetGodziny>((AspNetGodziny g) => g.Id == id);
            AspNetGodziny aspNetGodziny = aspnetgodziny;
            if (aspNetGodziny.export == 0)
            {
                int num2 = 1;
                num = num2;
                aspnetgodziny.export = num2;
                num1 = num;
            }
            else
            {
                int num3 = 0;
                num = num3;
                aspnetgodziny.export = num3;
                num1 = num;
            }
            aspNetGodziny.export = num1;
            dc.SubmitChanges();
            return base.RedirectToAction("PreviewNew", "Home");
        }

        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult Filtrowanie()
        {
            return base.View();
        }

        [HttpPost]
        public ActionResult FiltrujPodglad(int budowa, int brygadzista, int tydzien)
        {
            ((dynamic)base.ViewBag).Preview = this.GetGodzinyDoPogladu(budowa, brygadzista, tydzien);
            base.Session["FiltrBudowa"] = budowa;
            base.Session["FiltrBrygadzista"] = brygadzista;
            base.Session["FiltrTydzien"] = tydzien;
            return base.View("Preview");
        }

        private List<AspNetGodziny> GetGodzinyDoPogladu()
        {
            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
            int idIntranetFromDb = Help.GetIdIntranetFromDb(base.User.Identity.Name.ToString());
            List<AspNetGodziny> godziny = (
                from q in dc.AspNetGodzinies
                where q.brygadzistaID == idIntranetFromDb
                select q).ToList<AspNetGodziny>();
            if (Help.SprawdzCzyAdmin(base.User.Identity.Name.ToString()))
            {
                godziny = dc.AspNetGodzinies.ToList<AspNetGodziny>();
            }
            return (
                from q in godziny
                orderby q.rokTydzien descending, q.brygadzistaID, q.budowaID
                select q).ToList<AspNetGodziny>();
        }

        private List<AspNetGodziny> GetGodzinyDoPogladu(int budowa_filtr, int brygadzista_filtr, int tydzien_filtr)
        {
            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
            int idIntranetFromDb = Help.GetIdIntranetFromDb(base.User.Identity.Name.ToString());
            List<AspNetGodziny> godziny = (
                from q in dc.AspNetGodzinies
                where q.brygadzistaID == idIntranetFromDb
                select q).ToList<AspNetGodziny>();
            if (Help.SprawdzCzyAdmin(base.User.Identity.Name.ToString()))
            {
                godziny = dc.AspNetGodzinies.ToList<AspNetGodziny>();
            }
            if (budowa_filtr != 0)
            {
                godziny = (
                    from g in godziny
                    where g.budowaID == budowa_filtr
                    select g).ToList<AspNetGodziny>();
            }
            if (brygadzista_filtr != 0)
            {
                godziny = (
                    from g in godziny
                    where g.brygadzistaID == brygadzista_filtr
                    select g).ToList<AspNetGodziny>();
            }
            if (tydzien_filtr != 0)
            {
                godziny = (
                    from g in godziny
                    where g.rokTydzien == Help.GetTydzien(tydzien_filtr).getRokTydzien()
                    select g).ToList<AspNetGodziny>();
            }
            return (
                from q in godziny
                orderby q.rokTydzien descending, q.brygadzistaID, q.budowaID
                select q).ToList<AspNetGodziny>();
        }

        private List<AspNetGodziny> GetGodzinyDoPogladu100()
        {
            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
            int idIntranetFromDb = Help.GetIdIntranetFromDb(base.User.Identity.Name.ToString());
            List<AspNetGodziny> godziny = (
                from q in dc.AspNetGodzinies
                where q.brygadzistaID == idIntranetFromDb
                orderby q.rokTydzien descending, q.brygadzistaID, q.budowaID
                select q).Take<AspNetGodziny>(100).ToList<AspNetGodziny>();
            if (Help.SprawdzCzyAdmin(base.User.Identity.Name.ToString()))
            {
                godziny = (
                    from q in dc.AspNetGodzinies.Take<AspNetGodziny>(100)
                    orderby q.rokTydzien descending, q.brygadzistaID, q.budowaID
                    select q).ToList<AspNetGodziny>();
            }
            return godziny;
        }

        public ActionResult GoBack()
        {
            List<Brygada> item;
            if (base.Session["NowaBrygada"] != null)
            {
                item = (List<Brygada>)base.Session["NowaBrygada"];
            }
            else
            {
                item = null;
            }
            List<Brygada> brygada = item;
            if (brygada != null)
            {
                base.Session["NowaBrygada"] = Help.GetBrygada(brygada[0].Id, false);
            }
            return base.RedirectToAction("Index");
        }

        public ActionResult Index()
        {
            Help.generateAdminMessages();
            List<string> errorList = new List<string>();
            List<Brygada> brygada = new List<Brygada>();
            List<Tydzien> weeklist = new List<Tydzien>();
            List<Brygadzista> brygadzisci = new List<Brygadzista>();
            List<AspNetMessages> wiadomosci = new List<AspNetMessages>();
            try
            {
                int idIntranetFromDb = Help.GetIdIntranetFromDb(base.User.Identity.Name.ToString());
                brygada = Help.GetBrygada(idIntranetFromDb, false);
                wiadomosci = Help.GetAdminMessages(idIntranetFromDb);
            }
            catch (Exception exception)
            {
                Exception blad001 = exception;
                errorList.Add(string.Format("Nie można odtworzyć brygady użytkownika {0}. {1}", base.User.Identity.Name, blad001.Message));
            }
            try
            {
                brygadzisci = Help.GetBrygadzistow(true);
            }
            catch (Exception exception1)
            {
                errorList.Add(string.Format("Nie można pobrać listy brygadzistów. Brak możliwości dodania pracowników. {0}", exception1.Message));
            }
            //Help.BlockWeekIfNotNeeded();
            weeklist = Help.GetTygodnie(DateTime.Now.Year);
            string additionalMessages = (base.Session["AdditionalMessage"] != null ? base.Session["AdditionalMessage"].ToString() : "");
            if (!string.IsNullOrEmpty(additionalMessages))
            {
                errorList.Add(additionalMessages);
                base.Session["AdditionalMessage"] = null;
            }
            ((dynamic)base.ViewBag).ErrorList = errorList;
            ((dynamic)base.ViewBag).Brygada = brygada;
            ((dynamic)base.ViewBag).Brygadzisci = brygadzisci;
            ((dynamic)base.ViewBag).Tydzien = weeklist;
            ((dynamic)base.ViewBag).Wiadomosci = wiadomosci;
            if (!Help.isSession(base.Session["NowaBrygada"]))
            {
                base.Session["NowaBrygada"] = brygada;
            }
            else
            {
                ((dynamic)base.ViewBag).Brygada = (List<Brygada>)base.Session["NowaBrygada"];
            }
            base.Session["Tydzien"] = weeklist[0];
            return base.View();
        }

        public ActionResult Lock(int year, int id)
        {
            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
            dc.AspNetLata.First<AspNetLata>((AspNetLata l) => l.idtygodnia == id).zablokowany = 1;
            dc.SubmitChanges();
            return base.RedirectToAction("Blockade", "Home", new { year = year });
        }

        public ActionResult Preview()
        {
            base.Session.Clear();
            ((dynamic)base.ViewBag).Preview = this.GetGodzinyDoPogladu100();
            return base.View();
        }

        [HttpPost]
        public void ReadAllMessages(int brygadzistaid)
        {
            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
            (
                from m in dc.AspNetMessages
                where m.id_pracownika == brygadzistaid && (m.data < (DateTime?)DateTime.Now)
                select m).ToList<AspNetMessages>().ForEach((AspNetMessages p) => p.przeczytane = new bool?(true));
            dc.SubmitChanges();
        }

        public ActionResult Unlock(int year, int id)
        {
            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
            dc.AspNetLata.First<AspNetLata>((AspNetLata l) => l.idtygodnia == id).zablokowany = 2;
            dc.SubmitChanges();
            return base.RedirectToAction("Blockade", "Home", new { year = year });
        }
    }
}