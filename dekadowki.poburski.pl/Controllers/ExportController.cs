﻿using dekadowki.poburski.pl.Models;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Mvc;

namespace dekadowki.poburski.pl.Controllers
{
    public class ExportController : Controller
    {
        private pobiNTRANETDataContext dc = new pobiNTRANETDataContext();

        public ExportController()
        {
        }

        [Authorize]
        [HttpPost]
        public ActionResult Export(int brygadzista, string tydzien, int budowa, int budowaFiltr)
        {
            string brygadzistaNazwa = Help.GetNameAndSurname(brygadzista);
            List<AspNetGodziny> godziny = (
                from q in this.dc.AspNetGodzinies
                where q.brygadzistaID == brygadzista && q.rokTydzien == tydzien && q.export == 0
                select q).ToList<AspNetGodziny>();
            if (budowaFiltr != -1)
            {
                godziny = (
                    from g in godziny
                    where g.budowaID == budowa
                    select g).ToList<AspNetGodziny>();
            }
            List<AspNetGodziny> godzinyZaakceptowane = new List<AspNetGodziny>();
            string message = "";
            string color = "";
            string success = "";
            if (tydzien != null)
            {
                try
                {
                    foreach (AspNetGodziny aspNetGodziny in godziny)
                    {
                        akord_godziny akord_godziny_pn = Help.ConvertAspNetGodzinyToAkordGodziny(aspNetGodziny, 1, budowa);
                        akord_godziny akord_godziny_wt = Help.ConvertAspNetGodzinyToAkordGodziny(aspNetGodziny, 2, budowa);
                        akord_godziny akord_godziny_sr = Help.ConvertAspNetGodzinyToAkordGodziny(aspNetGodziny, 3, budowa);
                        akord_godziny akord_godziny_cz = Help.ConvertAspNetGodzinyToAkordGodziny(aspNetGodziny, 4, budowa);
                        akord_godziny akord_godziny_pi = Help.ConvertAspNetGodzinyToAkordGodziny(aspNetGodziny, 5, budowa);
                        akord_godziny akord_godziny_so = Help.ConvertAspNetGodzinyToAkordGodziny(aspNetGodziny, 6, budowa);
                        akord_godziny akord_godziny_ni = Help.ConvertAspNetGodzinyToAkordGodziny(aspNetGodziny, 7, budowa);
                        List<akord_godziny> lista = new List<akord_godziny>();

                        bool czy_jest_pn = this.sprawdzCzyIstnieje(akord_godziny_pn);
                        bool czy_jest_wt = this.sprawdzCzyIstnieje(akord_godziny_wt);
                        bool czy_jest_sr = this.sprawdzCzyIstnieje(akord_godziny_sr);
                        bool czy_jest_cz = this.sprawdzCzyIstnieje(akord_godziny_cz);
                        bool czy_jest_pi = this.sprawdzCzyIstnieje(akord_godziny_pi);
                        bool czy_jest_so = this.sprawdzCzyIstnieje(akord_godziny_so);
                        bool czy_jest_ni = this.sprawdzCzyIstnieje(akord_godziny_ni);

                        lista.Add(akord_godziny_pn);
                        lista.Add(akord_godziny_wt);
                        lista.Add(akord_godziny_sr);
                        lista.Add(akord_godziny_cz);
                        lista.Add(akord_godziny_pi);
                        lista.Add(akord_godziny_so);
                        lista.Add(akord_godziny_ni);
                        this.dc.akord_godzinies.InsertAllOnSubmit<akord_godziny>(lista);
                        this.dc.SubmitChanges();
                        try
                        {
                            foreach (AspNetGodziny list in (
                                from g in this.dc.AspNetGodzinies
                                where g.budowaID == aspNetGodziny.budowaID && g.pracownikID == aspNetGodziny.pracownikID && g.rokTydzien == aspNetGodziny.rokTydzien && g.export == 0
                                select g).ToList<AspNetGodziny>())
                            {
                                list.export = 1;
                                this.dc.SubmitChanges();
                            }
                        }
                        catch (Exception)
                        {
                            this.dc.AspNetGodzinies.First<AspNetGodziny>((AspNetGodziny g) => g.budowaID == aspNetGodziny.budowaID && g.pracownikID == aspNetGodziny.pracownikID && g.rokTydzien == aspNetGodziny.rokTydzien && g.export == 0).export = 1;
                            this.dc.SubmitChanges();
                        }
                        godzinyZaakceptowane.Add(aspNetGodziny);
                    }
                    message = string.Format("Sukces. Pomyślnie eksportowano wszystkie godziny brygadzisty {0} z tygodnia {1} do intranetu.", brygadzistaNazwa, tydzien);
                    success = "1";
                    color = "green";
                }
                catch (Exception exception1)
                {
                    Exception e = exception1;
                    message = string.Format("Pojawił się błąd. Brygadzista: {0}, Tydzień: {1} Skontaktuj się z administratorem. Szczegóły błędu: {2}", brygadzistaNazwa, tydzien, e.Message);
                    success = "0";
                    color = "red";
                }
                base.Session["GodzinyZaakceptowane"] = godzinyZaakceptowane;
            }
            return base.RedirectToAction("Success", "Export", new { message = message, success = success, color = color, budowa = budowa });
        }

        [Authorize]
        public ActionResult Index(int id, string tydzien, int budowa)
        {
            int num = id;
            string item = tydzien;
            List<Brygadzista> brygadzistowDoAkceptacji = Help.GetBrygadzistowDoAkceptacji();
            List<string> tygodnieDoAkceptacji = new List<string>();
            if (num != -1)
            {
                tygodnieDoAkceptacji = (
                    from t in this.dc.AspNetGodzinies
                    where t.brygadzistaID == num && t.export == 0
                    select t.rokTydzien).Distinct<string>().ToList<string>();
            }
            else if (brygadzistowDoAkceptacji.Count != 0)
            {
                tygodnieDoAkceptacji = (
                    from t in this.dc.AspNetGodzinies
                    where t.brygadzistaID == brygadzistowDoAkceptacji[0].GetId() && t.export == 0
                    select t.rokTydzien).Distinct<string>().ToList<string>();
            }
            ExportViewModel model = new ExportViewModel(brygadzistowDoAkceptacji, tygodnieDoAkceptacji);
            List<AspNetGodziny> godziny = new List<AspNetGodziny>();
            if (item != "a")
            {
                if (num == -1)
                {
                    num = brygadzistowDoAkceptacji[0].GetId();
                }
                godziny = (
                    from q in this.dc.AspNetGodzinies
                    where q.brygadzistaID == num && q.rokTydzien == item && q.export == 0
                    select q).ToList<AspNetGodziny>();
            }
            else if (tygodnieDoAkceptacji.Count <= 0)
            {
                ((dynamic)base.ViewBag).Message = "Brak tygodnia do zaakceptowania. Proszę wprowadzić dane.";
            }
            else
            {
                if (num == -1)
                {
                    num = brygadzistowDoAkceptacji[0].GetId();
                }
                item = tygodnieDoAkceptacji[0];
                godziny = (
                    from q in this.dc.AspNetGodzinies
                    where q.brygadzistaID == num && q.rokTydzien == item && q.export == 0
                    select q).ToList<AspNetGodziny>();
            }
            if (budowa != -1)
            {
                godziny = (
                    from g in godziny
                    where g.budowaID == budowa
                    select g).ToList<AspNetGodziny>();
            }
            ((dynamic)base.ViewBag).Preview = godziny;
            ((dynamic)base.ViewBag).Budowy = Help.GetBudowy();
            return base.View(model);
        }

        private bool sprawdzCzyIstnieje(dekadowki.poburski.pl.Models.akord_godziny akord_godziny)
        {
            List<akord_godziny> ag = dc.akord_godzinies.Where(g => g.Data == akord_godziny.Data && g.id_prac == akord_godziny.id_prac).ToList();
            if (ag.Count > 0)
            {
                foreach (var a in ag)
                {
                    if (!a.dzien_wolny.Contains("brak") && a.godz_akord > 0)
                    {
                        return true;
                    }
                }
            }

            if (!this.dc.akord_godzinies.Any((akord_godziny g) => (g.Data == akord_godziny.Data) && g.idbudowy == akord_godziny.idbudowy && g.id_prac == akord_godziny.id_prac && g.godz_od == akord_godziny.godz_od && g.godz_do == akord_godziny.godz_do))
            {
                return false;
            }
            double? godzAkord = this.dc.akord_godzinies.FirstOrDefault((akord_godziny g) => (g.Data == akord_godziny.Data) && g.idbudowy == akord_godziny.idbudowy && g.id_prac == akord_godziny.id_prac && g.godz_od == akord_godziny.godz_od && g.godz_do == akord_godziny.godz_do).godz_akord;
            if ((godzAkord.GetValueOrDefault() <= 0 ? !godzAkord.HasValue : true))
            {
                this.dc.akord_godzinies.DeleteOnSubmit(this.dc.akord_godzinies.FirstOrDefault((akord_godziny g) => (g.Data == akord_godziny.Data) && g.idbudowy == akord_godziny.idbudowy && g.id_prac == akord_godziny.id_prac && g.godz_od == akord_godziny.godz_od && g.godz_do == akord_godziny.godz_do));
                this.dc.SubmitChanges();
                return true;
            }
            return false;
        }

        [Authorize]
        public ActionResult Success(string message, string success, string color, int? budowa)
        {
            ((dynamic)base.ViewBag).Message = message;
            ((dynamic)base.ViewBag).Color = color;
            ((dynamic)base.ViewBag).Success = success;
            List<AspNetGodziny> list = (List<AspNetGodziny>)base.Session["GodzinyZaakceptowane"];
            try
            {
                if (budowa != 0)
                {
                    foreach (AspNetGodziny aspNetGodziny in list)
                    {
                        aspNetGodziny.budowaID = Convert.ToInt32(budowa);
                    }
                }
            }
            catch (Exception)
            {

            }
            ((dynamic)base.ViewBag).Godziny = list;
            return base.View();
        }
    }
}