﻿using dekadowki.poburski.pl.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.Linq;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;

namespace dekadowki.poburski.pl.Controllers
{
    [Authorize]
    public class RozliczeniaBudowyController : Controller
    {
        private List<Budowa> budowy = Help.GetBudowy();

        private List<Tydzien> weeklist = Help.GetTygodnieCalosc(DateTime.Now.Year);

        private pobiNTRANETDataContext dc = new pobiNTRANETDataContext();

        public RozliczeniaBudowyController()
        {
        }

        public ActionResult Index()
        {
            base.Session.Clear();
            int idIntranetFromDb = Help.GetIdIntranetFromDb(base.User.Identity.Name.ToString());
            int num = 0;
            Budowa budowa = new Budowa(-1, "", "");
            Tydzien tydzien = Help.GetTydzien(Help.GetCurrentWeek());
            AspNetRozliczeniaBudow rozliczenie = new AspNetRozliczeniaBudow();
            if (string.IsNullOrEmpty(base.Request["roz"]))
            {
                if (!string.IsNullOrEmpty(base.Request["bud"]))
                {
                    try
                    {
                        int num1 = Convert.ToInt32(base.Request["bud"]);
                        budowa = (
                            from b in this.budowy
                            where b.Id == num1
                            select b).FirstOrDefault<Budowa>();
                    }
                    catch (Exception)
                    {
                    }
                }
                if (!string.IsNullOrEmpty(base.Request["tyd"]))
                {
                    try
                    {
                        int num2 = Convert.ToInt32(base.Request["tyd"]);
                        tydzien = (
                            from t in this.weeklist
                            where t.getId() == num2
                            select t).First<Tydzien>();
                    }
                    catch (Exception)
                    {
                    }
                }
                try
                {
                    if (!(
                        from rb in this.dc.AspNetRozliczeniaBudow
                        where rb.brygadzista_id == idIntranetFromDb && rb.budowa_id == budowa.Id && rb.tydzien_id == tydzien.getId()
                        select rb).Any<AspNetRozliczeniaBudow>())
                    {
                        this.SprobujSciagnacZDekadowek(budowa, tydzien, idIntranetFromDb, ref rozliczenie);
                    }
                    else
                    {
                        rozliczenie = (
                            from rb in this.dc.AspNetRozliczeniaBudow
                            where rb.brygadzista_id == idIntranetFromDb && rb.budowa_id == budowa.Id && rb.tydzien_id == tydzien.getId()
                            select rb).FirstOrDefault<AspNetRozliczeniaBudow>();
                    }
                }
                catch (Exception)
                {
                }
            }
            else
            {
                num = Convert.ToInt32(base.Request["roz"]);
                rozliczenie = (
                    from rb in this.dc.AspNetRozliczeniaBudow
                    where rb.Id == num
                    select rb).FirstOrDefault<AspNetRozliczeniaBudow>();
            }
            ((dynamic)base.ViewBag).Rozliczenie = rozliczenie;
            ((dynamic)base.ViewBag).Kierownik = idIntranetFromDb;
            ((dynamic)base.ViewBag).Budowy = this.budowy;
            ((dynamic)base.ViewBag).Tygodnie = this.weeklist;
            ((dynamic)base.ViewBag).WybranaBudowa = budowa;
            ((dynamic)base.ViewBag).WybranyTydzien = tydzien;
            return base.View();
        }

        [HttpPost]
        public ActionResult SaveRozliczenie(FormCollection collection)
        {
            string item = collection["budowa"];
            string str = collection["tydzien"];
            string podpis = collection["podpis"];
            int num = Convert.ToInt32(collection["kierownik"]);
            string path = base.Server.MapPath("~/Content/signatures");
            object[] shortDateString = new object[] { "rozliczenie", num, str, item, null };
            shortDateString[4] = DateTime.Now.ToShortDateString();
            string fileName = string.Format("{0}-{1}-{2}-{3}-{4}.png", shortDateString);
            string fileNameWitPath = Path.Combine(path, fileName);
            string currentPodpis = null;
            if (!string.IsNullOrEmpty(podpis) && podpis != Help.pustyPodpis)
            {
                using (FileStream fs = new FileStream(fileNameWitPath, FileMode.Create))
                {
                    using (BinaryWriter bw = new BinaryWriter(fs))
                    {
                        bw.Write(Convert.FromBase64String(podpis));
                        bw.Close();
                    }
                    fs.Close();
                }
                currentPodpis = fileName;
            }
            AspNetRozliczeniaBudow rozliczenie = new AspNetRozliczeniaBudow()
            {
                budowa_id = Convert.ToInt32(item),
                tydzien_id = Convert.ToInt32(str),
                brygadzista_id = num,
                podpis = currentPodpis,
                powPob_pn = collection["powPob_pn"],
                powPob_wt = collection["powPob_wt"],
                powPob_sr = collection["powPob_sr"],
                powPob_cz = collection["powPob_cz"],
                powPob_pi = collection["powPob_pi"],
                powPob_so = collection["powPob_so"],
                powExtra_nazwa = collection["powExtra_nazwa"],
                powExtra_pn = collection["powExtra_pn"],
                powExtra_wt = collection["powExtra_wt"],
                powExtra_sr = collection["powExtra_sr"],
                powExtra_cz = collection["powExtra_cz"],
                powExtra_pi = collection["powExtra_pi"],
                powExtra_so = collection["powExtra_so"],
                godzPob_pn = collection["godzPob_pn"],
                godzPob_wt = collection["godzPob_wt"],
                godzPob_sr = collection["godzPob_sr"],
                godzPob_cz = collection["godzPob_cz"],
                godzPob_pi = collection["godzPob_pi"],
                godzPob_so = collection["godzPob_so"],
                godzExtra1_nazwa = collection["godzExtra1_nazwa"],
                godzExtra1_pn = collection["godzExtra1_pn"],
                godzExtra1_wt = collection["godzExtra1_wt"],
                godzExtra1_sr = collection["godzExtra1_sr"],
                godzExtra1_cz = collection["godzExtra1_cz"],
                godzExtra1_pi = collection["godzExtra1_pi"],
                godzExtra1_so = collection["godzExtra1_so"],
                godzExtra2_nazwa = collection["godzExtra2_nazwa"],
                godzExtra2_pn = collection["godzExtra2_pn"],
                godzExtra2_wt = collection["godzExtra2_wt"],
                godzExtra2_sr = collection["godzExtra2_sr"],
                godzExtra2_cz = collection["godzExtra2_cz"],
                godzExtra2_pi = collection["godzExtra2_pi"],
                godzExtra2_so = collection["godzExtra2_so"],
                godzExtra3_nazwa = collection["godzExtra3_nazwa"],
                godzExtra3_pn = collection["godzExtra3_pn"],
                godzExtra3_wt = collection["godzExtra3_wt"],
                godzExtra3_sr = collection["godzExtra3_sr"],
                godzExtra3_cz = collection["godzExtra3_cz"],
                godzExtra3_pi = collection["godzExtra3_pi"],
                godzExtra3_so = collection["godzExtra3_so"],
                godzExtra4_nazwa = collection["godzExtra4_nazwa"],
                godzExtra4_pn = collection["godzExtra4_pn"],
                godzExtra4_wt = collection["godzExtra4_wt"],
                godzExtra4_sr = collection["godzExtra4_sr"],
                godzExtra4_cz = collection["godzExtra4_cz"],
                godzExtra4_pi = collection["godzExtra4_pi"],
                godzExtra4_so = collection["godzExtra4_so"],
                dzwig_pn = collection["dzwig_pn"],
                dzwig_wt = collection["dzwig_wt"],
                dzwig_sr = collection["dzwig_sr"],
                dzwig_cz = collection["dzwig_cz"],
                dzwig_pi = collection["dzwig_pi"],
                dzwig_so = collection["dzwig_so"],
                cat_pn = collection["cat_pn"],
                cat_wt = collection["cat_wt"],
                cat_sr = collection["cat_sr"],
                cat_cz = collection["cat_cz"],
                cat_pi = collection["cat_pi"],
                cat_so = collection["cat_so"],
                manitou_pn = collection["manitou_pn"],
                manitou_wt = collection["manitou_wt"],
                manitou_sr = collection["manitou_sr"],
                manitou_cz = collection["manitou_cz"],
                manitou_pi = collection["manitou_pi"],
                manitou_so = collection["manitou_so"],
                inne_pn = collection["inne_pn"],
                inne_wt = collection["inne_wt"],
                inne_sr = collection["inne_sr"],
                inne_cz = collection["inne_cz"],
                inne_pt = collection["inne_pi"],
                inne_so = collection["inne_so"],
                notatki_pn = collection["notatki_pn"],
                notatki_wt = collection["notatki_wt"],
                notatki_sr = collection["notatki_sr"],
                notatki_cz = collection["notatki_cz"],
                notatki_pi = collection["notatki_pi"],
                notatki_so = collection["notatki_so"]
            };
            if ((
                from rb in this.dc.AspNetRozliczeniaBudow
                where rb.budowa_id == Convert.ToInt32(item) && rb.tydzien_id == Convert.ToInt32(str) && rb.brygadzista_id == num
                select rb).Any<AspNetRozliczeniaBudow>())
            {
                AspNetRozliczeniaBudow aspNetRozliczeniaBudow = this.dc.AspNetRozliczeniaBudow.Single<AspNetRozliczeniaBudow>((AspNetRozliczeniaBudow rb) => rb.tydzien_id == Convert.ToInt32(str) && rb.budowa_id == Convert.ToInt32(item) && rb.brygadzista_id == num);
                aspNetRozliczeniaBudow.podpis = currentPodpis;
                AspNetRozliczeniaBudow aspNetRozliczeniaBudow1 = Help.PoprawRozliczenie(aspNetRozliczeniaBudow);
                this.dc.SubmitChanges();
                rozliczenie = aspNetRozliczeniaBudow1;
            }
            else
            {
                this.dc.AspNetRozliczeniaBudow.InsertOnSubmit(rozliczenie);
                this.dc.SubmitChanges();
                rozliczenie = Help.PoprawRozliczenie(rozliczenie);
            }
            List<string> listaUserow = new List<string>()
            {
                "kkowalska@poburski.pl",
                "mjedrzejczyk@wizualizacja.com"
            };
            Help.StworzExcelIWyslij(rozliczenie, "RozliczenieBudowy", listaUserow, base.User.Identity.Name.ToString(), base.Server.MapPath("~/Content/"));
            return base.RedirectToAction("Index", "RozliczeniaBudowy", new { bud = item, tyd = str });
        }

        private void SprobujSciagnacZDekadowek(Budowa wybranaBudowa, Tydzien wybranyTydzien, int id_kierownika, ref AspNetRozliczeniaBudow rozliczenie)
        {
            IQueryable<AspNetGodziny> godziny =
                from pn in this.dc.AspNetGodzinies
                where pn.brygadzistaID == id_kierownika && pn.rokTydzien == wybranyTydzien.getRokTydzien() && pn.budowaID == wybranaBudowa.Id
                select pn;
            if (godziny.Any<AspNetGodziny>())
            {
                double num = (
                    from g in godziny
                    select Convert.ToDouble(g.do_pn) - Convert.ToDouble(g.od_pn) + Convert.ToDouble(g.dojazd_pn)).Sum();
                rozliczenie.godzPob_pn = num.ToString();
                num = (
                    from g in godziny
                    select Convert.ToDouble(g.do_wt) - Convert.ToDouble(g.od_wt) + Convert.ToDouble(g.dojazd_wt)).Sum();
                rozliczenie.godzPob_wt = num.ToString();
                num = (
                    from g in godziny
                    select Convert.ToDouble(g.do_sr) - Convert.ToDouble(g.od_sr) + Convert.ToDouble(g.dojazd_sr)).Sum();
                rozliczenie.godzPob_sr = num.ToString();
                num = (
                    from g in godziny
                    select Convert.ToDouble(g.do_cz) - Convert.ToDouble(g.od_cz) + Convert.ToDouble(g.dojazd_cz)).Sum();
                rozliczenie.godzPob_cz = num.ToString();
                num = (
                    from g in godziny
                    select Convert.ToDouble(g.do_pi) - Convert.ToDouble(g.od_pi) + Convert.ToDouble(g.dojazd_pi)).Sum();
                rozliczenie.godzPob_pi = num.ToString();
                num = (
                    from g in godziny
                    select Convert.ToDouble(g.do_so) - Convert.ToDouble(g.od_so) + Convert.ToDouble(g.dojazd_so)).Sum();
                rozliczenie.godzPob_so = num.ToString();
            }
        }
    }
}