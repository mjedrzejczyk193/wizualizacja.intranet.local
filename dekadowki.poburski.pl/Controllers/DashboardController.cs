﻿using dekadowki.poburski.pl.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;

namespace dekadowki.poburski.pl.Controllers
{
    public class DashboardController : Controller
    {
        private List<Budowa> budowy = Help.GetBudowy();

        public DashboardController()
        {
        }

        [HttpPost]
        public void ChangeWeek(int tydzien, int budowa, int brygadzista, string staryTydzien)
        {
            base.Session["Tydzien"] = Help.GetTydzien(tydzien);
            base.Response.Cookies.Add(new HttpCookie("Tydzien", Help.GetTydzien(tydzien).getRokTydzien()));
            try
            {
                int id = (
                    from asg in (new pobiNTRANETDataContext()).AspNetGodzinies
                    where asg.brygadzistaID == brygadzista && asg.budowaID == budowa && asg.rokTydzien == staryTydzien
                    select asg).First<AspNetGodziny>().Id;
                Help.ZmienGodziny(staryTydzien, tydzien, budowa, brygadzista);
            }
            catch (Exception)
            {
            }
        }

        public ActionResult Index(int? idpracownika = null, string dzien = null, int tabela = 0)
        {
            ActionResult action;
            Tydzien item;
            List<Brygada> brygadas;
            Budowa currentBudowa = this.budowy[0];

            if (base.Session["Budowa"] != null)
            {
                currentBudowa = (Budowa)base.Session["Budowa"];
            }
            if (base.Session["Tydzien"] != null)
            {
                item = (Tydzien)base.Session["Tydzien"];
            }
            else
            {
                item = null;
            }
            Tydzien currentWeek = item;
            try
            {
                if (currentWeek == null)
                {
                    currentWeek = Help.GetTydzien(Convert.ToInt32(base.Response.Cookies["Tydzien"].Value));
                }
            }
            catch (Exception)
            {
                currentWeek = Help.GetTydzien(Help.GetCurrentWeek());
            }
            if (!string.IsNullOrEmpty(dzien))
            {
                currentWeek = Help.GetTydzien(Convert.ToDateTime(dzien));
                Session["DzienTygodnia"] = Help.GetDzienTygodniaInt((int)Convert.ToDateTime(dzien).DayOfWeek);
                ViewBag.Odblokuj = "true";
            }
            else
            {
                Session["DzienTygodnia"] = 0;
            }
            try
            {
                DashboardViewModel dvm = new DashboardViewModel();
                DashboardViewModel dashboardViewModel = dvm;
                if (base.Session["NowaBrygada"] != null)
                {
                    brygadas = (List<Brygada>)base.Session["NowaBrygada"];
                }
                else
                {
                    brygadas = null;
                }
                if (idpracownika != null)
                {
                    List<Brygada> b = Help.GetBrygada(Convert.ToInt32(idpracownika), true);
                    brygadas = b;
                    Session["NowaBrygada"] = b;
                    Session["Pracownik"] = idpracownika;
                    ViewBag.Odblokuj = "true";
                }
                else
                {
                    Session["Pracownik"] = 0;
                    ViewBag.Odblokuj = "true";
                }

                dashboardViewModel.Brygada = brygadas;
                dvm.DniTygodnia = Help.GetDniTygodnia(dvm.Brygada.Count, currentWeek, dvm.Brygada[0].Id, dvm.Brygada, currentBudowa.Id, false);
                dvm.Tydzien = currentWeek;
                DashboardViewModel item1 = dvm;
                item1.Brygadzista = item1.Brygada[0];
                ((dynamic)base.ViewBag).Message = "";
                ((dynamic)base.ViewBag).ColorMessage = "";
                ((dynamic)base.ViewBag).Budowy = this.budowy;
                ((dynamic)base.ViewBag).Podpisy = (dynamic)Help.GetPodpisy(dvm, currentBudowa);
                ((dynamic)base.ViewBag).Tydzien = currentWeek;
                base.Session["Tydzien"] = currentWeek;
                base.Session["DVM"] = dvm;
                base.Session["Budowa"] = currentBudowa;
                base.Response.Cookies["Tydzien"].Value = currentWeek.getRokTydzien();
                base.Response.Cookies["Tydzien"].Expires = DateTime.Now.AddMinutes(15);
                action = base.View(dvm);
            }
            catch (Exception)
            {
                action = base.RedirectToAction("Index", "Home");
            }

            if (Session["Pracownik"] != null)
            {
                ViewBag.Pracownik = Convert.ToInt32(Session["Pracownik"]);
            }
            else
            {
                ViewBag.Pracownik = 0;
            }

            if (Session["DzienTygodnia"] != null)
            {
                ViewBag.DzienTygodnia = Convert.ToInt32(Session["DzienTygodnia"]);
            }
            else
            {
                ViewBag.DzienTygodnia = 0;
            }

            return action;
        }

        public ActionResult Index2(string message, string colorMessage, string serwis, bool? drukuj, string czyInnaZmiana, string tabela = "0", string dzien = null)
        {
            string str;
            Session["Dzien"] = dzien;
            Budowa currentBudowa = this.budowy[0];
            if (base.Session["Budowa"] != null)
            {
                currentBudowa = (Budowa)base.Session["Budowa"];
            }
            Tydzien currentWeek = Help.GetTydzien(Help.GetCurrentWeek());
            if (base.Session["Tydzien"] == null)
            {
                try
                {
                    currentWeek = Help.GetTydzien(base.Response.Cookies["Tydzien"].Value);
                }
                catch (Exception)
                {
                    currentWeek = Help.GetTydzien(Help.GetCurrentWeek());
                }
            }
            else
            {
                currentWeek = (Tydzien)base.Session["Tydzien"];
            }

            if (!string.IsNullOrEmpty(dzien))
            {
                currentWeek = Help.GetTydzien(Convert.ToDateTime(dzien));
                Session["DzienTygodnia"] = Help.GetDzienTygodniaInt((int)Convert.ToDateTime(dzien).DayOfWeek);
                ViewBag.Odblokuj = "true";
            }

            DashboardViewModel dvm = new DashboardViewModel()
            {
                Brygada = Help.GetBrygada(Help.GetIdIntranetFromDb(base.User.Identity.Name), true)
            };
            if(tabela == "1")
            {
                dvm.Brygada = Help.GetBrygada(Help.GetIdIntranetFromDb(base.User.Identity.Name), false);
            }
            if (base.Session["NowaBrygada"] != null)
            {
                dvm.Brygada = (List<Brygada>)base.Session["NowaBrygada"];
            }
            dvm.DniTygodnia = Help.GetDniTygodnia(dvm.Brygada.Count, currentWeek, dvm.Brygada[0].Id, dvm.Brygada, currentBudowa.Id, Convert.ToBoolean(czyInnaZmiana));
            dvm.Tydzien = currentWeek;
            DashboardViewModel item = dvm;
            item.Brygadzista = item.Brygada[0];
            base.ViewBag.Message = (string.IsNullOrEmpty(message) ? "" : message);
            base.ViewBag.ColorMessage = (string.IsNullOrEmpty(colorMessage) ? "" : colorMessage);
            ((dynamic)base.ViewBag).Budowy = this.budowy;
            ((dynamic)base.ViewBag).WybranaBudowa = currentBudowa;
            dynamic viewBag = base.ViewBag;
            if (string.IsNullOrEmpty(serwis))
            {
                str = null;
            }
            else
            {
                str = serwis;
            }
            viewBag.Serwis = str;
            ((dynamic)base.ViewBag).Podpisy = (dynamic)Help.GetPodpisy(dvm, currentBudowa);
            ((dynamic)base.ViewBag).Print = drukuj;
            base.Session["Tydzien"] = currentWeek;
            base.Session["DVM"] = dvm;
            base.Session["Budowa"] = currentBudowa;

            if (Session["Pracownik"] != null)
            {
                ViewBag.Pracownik = Convert.ToInt32(Session["Pracownik"]);
            }
            else
            {
                ViewBag.Pracownik = 0;
            }

            if (Session["DzienTygodnia"] != null)
            {
                ViewBag.DzienTygodnia = Convert.ToInt32(Session["DzienTygodnia"]);
            }
            else
            {
                ViewBag.DzienTygodnia = 0;
            }

            if (currentWeek != null)
            {
                return base.View("Index", dvm);
            }
            return base.RedirectToAction("Index", "Home", new { tabela = tabela });
        }

        public ActionResult Print()
        {
            Budowa wybranaBudowa = (Budowa)base.Session["Budowa"];
            ((dynamic)base.ViewBag).WybranaBudowa = wybranaBudowa;
            ((dynamic)base.ViewBag).Tydzien = (Tydzien)base.Session["Tydzien"];
            DashboardViewModel dvm = (DashboardViewModel)base.Session["DVM"];
            ((dynamic)base.ViewBag).Podpisy = (dynamic)Help.GetPodpisy(dvm, wybranaBudowa);
            return base.View(dvm);
        }

        [HttpPost]
        public ActionResult SaveDashboard(DashboardViewModel dvm = null, List<string> imageData = null, FormCollection collection = null, bool? czyInnaZmiana = false, string budowa = "0", string tabela = "0")
        {
            Budowa currentBudowa;
            if (dvm == null || collection == null)
            {
                try
                {
                    string json = JsonConvert.SerializeObject(dvm, Formatting.Indented);
                    JsonConvert.SerializeObject(collection, Formatting.Indented);
                    JsonConvert.SerializeObject(imageData, Formatting.Indented);
                    string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
                    DateTime now = DateTime.Now;
                    StreamWriter sw = new StreamWriter(string.Concat(baseDirectory, "Logi\\Dashboard\\BlądDVM", now.ToString("dd-MM-yyyy"), ".txt"), true);
                    string[] str = new string[5];
                    now = DateTime.Now;
                    str[0] = now.ToString();
                    str[1] = Environment.NewLine;
                    str[2] = json.ToString();
                    str[3] = Environment.NewLine;
                    str[4] = "***************************************";
                    sw.WriteLine(string.Concat(str));
                    sw.Close();
                    base.Session["AdditionalMessage"] = "Coś poszło nie tak. Logi zapisane.";
                }
                catch (Exception)
                {
                }
                return base.RedirectToAction("Index", "Home");
            }
            string str1 = budowa;
            string komenda = collection["komenda"].ToString();
            string serwis = "";
            try
            {
                currentBudowa = this.budowy.FirstOrDefault<Budowa>((Budowa m) => m.Id == Convert.ToInt32(str1));
            }
            catch (Exception)
            {
                try
                {
                    var index = str1.Split(',').ToList().Count;
                    currentBudowa = this.budowy.FirstOrDefault<Budowa>((Budowa m) => m.Id == Convert.ToInt32(str1.Split(',')[index = 1]));
                }
                catch (Exception)
                {
                    try
                    {
                        int id = Help.GetBudowaPoNumerze(str1.Split(']')[0].TrimStart('['));
                        currentBudowa = this.budowy.FirstOrDefault<Budowa>((Budowa m) => m.Id == id);
                    }
                    catch (Exception)
                    {

                        currentBudowa = new Budowa(0, str1, "0");
                        serwis = currentBudowa.Nazwa;
                    }
                }

            }
            DashboardViewModel item = dvm;
            item.Brygadzista = item.Brygada[0];
            dvm.Tydzien = (Tydzien)base.Session["Tydzien"];
            if (dvm.Tydzien == null)
            {
                dvm.Tydzien = Help.GetTydzien(base.Response.Cookies["Tydzien"].Value);
                base.Session["Tydzien"] = dvm.Tydzien;
            }
            if (dvm.Tydzien == null)
            {
                dvm.Tydzien = Help.GetTydzien(Convert.ToInt32(collection["tydzien"]));
                base.Session["Tydzien"] = dvm.Tydzien;
                base.Response.Cookies.Add(new HttpCookie("Tydzien", dvm.Tydzien.getRokTydzien()));
            }
            string message = "";
            string colorMessage = "black";
            ((dynamic)base.ViewBag).WybranaBudowa = currentBudowa;
            ((dynamic)base.ViewBag).Budowy = this.budowy;
            base.Session["Tydzien"] = dvm.Tydzien;
            base.Session["DVM"] = dvm;
            base.Session["Budowa"] = currentBudowa;
            if (!(komenda == "0") && !(komenda == "") && !(komenda == "2"))
            {
                message = string.Format("zmieniono_budowe", new object[0]);
                colorMessage = "#4286f4";
                dvm.DniTygodnia = Help.GetDniTygodnia(dvm.Brygada.Count, dvm.Tydzien, dvm.Brygada[0].Id, dvm.Brygada, currentBudowa.Id, Convert.ToBoolean(czyInnaZmiana));
                ((dynamic)base.ViewBag).WybranaBudowa = currentBudowa;
                ((dynamic)base.ViewBag).Budowy = this.budowy;
                ((dynamic)base.ViewBag).Podpisy = (dynamic)Help.GetPodpisy(dvm, currentBudowa);
                base.Session["Tydzien"] = dvm.Tydzien;
                base.Session["DVM"] = dvm;
                base.Session["Budowa"] = currentBudowa;
                return base.RedirectToAction("Index2", "Dashboard", new { message = message, colorMessage = colorMessage, serwis = serwis, czyInnaZmiana = czyInnaZmiana, tabela = tabela });
            }
            pobiNTRANETDataContext _pobiNTRANETDataContext = new pobiNTRANETDataContext();
            Help.SaveWeekToDatabase(dvm, currentBudowa, base.Server.MapPath("~/Content/signatures"), imageData, base.Server.MapPath("~/Logi/Dashboard/"), dvm.Tydzien, Convert.ToBoolean(czyInnaZmiana));
            message = string.Format("Zapisano zmiany ({0})", DateTime.Now);
            colorMessage = "#00872B";
            ((dynamic)base.ViewBag).Message = message;
            ((dynamic)base.ViewBag).ColorMessage = colorMessage;
            ((dynamic)base.ViewBag).Podpisy = (dynamic)Help.GetPodpisy(dvm, currentBudowa);
            ((dynamic)base.ViewBag).Serwis = serwis;
            base.Session["Tydzien"] = dvm.Tydzien;
            base.Session["DVM"] = dvm;
            base.Session["Budowa"] = currentBudowa;

            if (tabela == "1")
            {
                Session["Link"] = string.Format(@"/Home/EditGodziny?id={0}&idpracownika={1}&dzien={2}&tabela=1", Session["CurrentId"], Session["Pracownik"], Session["Dzien"]);
                Session["CurrentId"] = null;
                Session["Pracownik"] = null;
                Session["Dzien"] = null;
                ViewBag.Tabela = "2";
            }

            return base.View("Index", dvm);
        }

        [Authorize]
        public ActionResult ShowMonth(int? idp, int? m, int? r, int? idb)
        {
            int miesiac = m == null ? DateTime.Now.Month : Convert.ToInt32(m);
            int rok = r == null ? DateTime.Now.Year : Convert.ToInt32(r);
            int idbrygadzisty = idb == null ? Help.GetIdIntranetFromDb(base.User.Identity.Name) : Convert.ToInt32(idb);
            int idpracownika = idp == null ? Help.GetBrygada(idbrygadzisty, true)[0].Id : Convert.ToInt32(idp);
            int daysCount = DateTime.DaysInMonth(rok, miesiac);

            List<DekadowkiMiesiacDlaPracownika> lista = new List<DekadowkiMiesiacDlaPracownika>();
            pobiNTRANETDataContext dc = new pobiNTRANETDataContext();
            for (int day = 1; day <= daysCount; day++)
            {
                DateTime today = new DateTime(rok, miesiac, day);
                int isoweek = Help.GetIso8601WeekOfYear(today);
                string isoweekString = isoweek < 10 ? "0" + isoweek : isoweek.ToString();
                string week = rok + "." + isoweekString;

                var dzien = new DekadowkiMiesiacDlaPracownika();
                dzien.Data = today.ToString("dd-MM-yyyy");
                dzien.Dzien = day;
                dzien.DzienTygodnia = Help.GetDzienTygodnia((int)today.DayOfWeek);
                dzien.DzienTygodniaInt = (int)today.DayOfWeek;
                dzien.RokTydzien = week;

                lista.Add(dzien);

                //int aktualnyDzien1 = day;
                //int dzienTygodnia1 = ((int)DateTime.Parse(Help.GetDayOfWeek(week, aktualnyDzien1)).DayOfWeek);
                //var dzien1 = new DekadowkiMiesiacDlaPracownika();
                //dzien1.Data = Help.GetDayOfWeek(week, dzienTygodnia1);
                //dzien1.Dzien = DateTime.Parse(dzien1.Data).Day;
                //dzien1.DzienTygodnia = Help.GetDzienTygodnia(dzienTygodnia1);

                //int aktualnyDzien2 = day + 1;
                //int dzienTygodnia2 = ((int)DateTime.Parse(Help.GetDayOfWeek(week, aktualnyDzien2)).DayOfWeek);
                //var dzien2 = new DekadowkiMiesiacDlaPracownika();
                //dzien2.Data = Help.GetDayOfWeek(week, dzienTygodnia2);
                //dzien2.Dzien = DateTime.Parse(dzien2.Data).Day;
                //dzien2.DzienTygodnia = Help.GetDzienTygodnia(dzienTygodnia2);

                //int aktualnyDzien3 = day + 2;
                //int dzienTygodnia3 = ((int)DateTime.Parse(Help.GetDayOfWeek(week, aktualnyDzien3)).DayOfWeek);
                //var dzien3 = new DekadowkiMiesiacDlaPracownika();
                //dzien3.Data = Help.GetDayOfWeek(week, dzienTygodnia3);
                //dzien3.Dzien = DateTime.Parse(dzien3.Data).Day;
                //dzien3.DzienTygodnia = Help.GetDzienTygodnia(dzienTygodnia3);

                //int aktualnyDzien4 = day + 3;
                //int dzienTygodnia4 = ((int)DateTime.Parse(Help.GetDayOfWeek(week, aktualnyDzien4)).DayOfWeek);
                //var dzien4 = new DekadowkiMiesiacDlaPracownika();
                //dzien4.Data = Help.GetDayOfWeek(week, dzienTygodnia4);
                //dzien4.Dzien = DateTime.Parse(dzien4.Data).Day;
                //dzien4.DzienTygodnia = Help.GetDzienTygodnia(dzienTygodnia4);

                //int aktualnyDzien5 = day + 4;
                //int dzienTygodnia5 = ((int)DateTime.Parse(Help.GetDayOfWeek(week, aktualnyDzien5)).DayOfWeek);
                //var dzien5 = new DekadowkiMiesiacDlaPracownika();
                //dzien5.Data = Help.GetDayOfWeek(week, dzienTygodnia5);
                //dzien5.Dzien = DateTime.Parse(dzien5.Data).Day;
                //dzien5.DzienTygodnia = Help.GetDzienTygodnia(dzienTygodnia5);

                //int aktualnyDzien6 = day + 5;
                //int dzienTygodnia6 = ((int)DateTime.Parse(Help.GetDayOfWeek(week, aktualnyDzien6)).DayOfWeek);
                //var dzien6 = new DekadowkiMiesiacDlaPracownika();
                //dzien6.Data = Help.GetDayOfWeek(week, dzienTygodnia6);
                //dzien6.Dzien = DateTime.Parse(dzien6.Data).Day;
                //dzien6.DzienTygodnia = Help.GetDzienTygodnia(dzienTygodnia6);

                //int aktualnyDzien7 = day + 6;
                //int dzienTygodnia7 = ((int)DateTime.Parse(Help.GetDayOfWeek(week, aktualnyDzien7)).DayOfWeek);
                //var dzien7 = new DekadowkiMiesiacDlaPracownika();
                //dzien7.Data = Help.GetDayOfWeek(week, dzienTygodnia7);
                //dzien7.Dzien = DateTime.Parse(dzien7.Data).Day;
                //dzien7.DzienTygodnia = Help.GetDzienTygodnia(dzienTygodnia7);

                //dzien1.Od = ""; dzien2.Od = ""; dzien3.Od = ""; dzien4.Od = ""; dzien5.Od = ""; dzien6.Od = ""; dzien7.Od = "";

                //dzien1.Do = ""; dzien2.Do = ""; dzien3.Do = ""; dzien4.Do = ""; dzien5.Do = ""; dzien6.Do = ""; dzien7.Do = "";

                //dzien1.Dojazd = ""; dzien2.Dojazd = ""; dzien3.Dojazd = ""; dzien4.Dojazd = ""; dzien5.Dojazd = ""; dzien6.Dojazd = ""; dzien7.Dojazd = "";

                //dzien1.Przestoje = ""; dzien2.Przestoje = ""; dzien3.Przestoje = ""; dzien4.Przestoje = ""; dzien5.Przestoje = ""; dzien6.Przestoje = ""; dzien7.Przestoje = "";

                //dzien1.NazwaProjektu = ""; dzien2.NazwaProjektu = ""; dzien3.NazwaProjektu = ""; dzien4.NazwaProjektu = ""; dzien5.NazwaProjektu = ""; dzien6.NazwaProjektu = ""; dzien7.NazwaProjektu = "";

                //dzien1.Dieta = ""; dzien2.Dieta = ""; dzien3.Dieta = ""; dzien4.Dieta = ""; dzien5.Dieta = ""; dzien6.Dieta = ""; dzien7.Dieta = "";

                //dzien1.Godziny100 = ""; dzien2.Godziny100 = ""; dzien3.Godziny100 = ""; dzien4.Godziny100 = ""; dzien5.Godziny100 = ""; dzien6.Godziny100 = ""; dzien7.Godziny100 = "";

                //dzien1.Godziny150 = ""; dzien2.Godziny150 = ""; dzien3.Godziny150 = ""; dzien4.Godziny150 = ""; dzien5.Godziny150 = ""; dzien6.Godziny150 = ""; dzien7.Godziny150 = "";

                //dzien1.Godziny200 = ""; dzien2.Godziny200 = ""; dzien3.Godziny200 = ""; dzien4.Godziny200 = ""; dzien5.Godziny200 = ""; dzien6.Godziny200 = ""; dzien7.Godziny200 = "";

                //dzien1.GodzinyNocne = ""; dzien2.GodzinyNocne = ""; dzien3.GodzinyNocne = ""; dzien4.GodzinyNocne = ""; dzien5.GodzinyNocne = ""; dzien6.GodzinyNocne = ""; dzien7.GodzinyNocne = "";

                //lista.Add(dzien1);
                //lista.Add(dzien2);
                //lista.Add(dzien3);
                //lista.Add(dzien4);
                //lista.Add(dzien5);
                //lista.Add(dzien6);
                //lista.Add(dzien7);
            }
            ViewBag.Lista = lista;
            ViewBag.Rok = rok;
            ViewBag.Miesiac = miesiac;
            ViewBag.Pracownik = Help.GetNameAndSurname(idpracownika);
            ViewBag.Idp = idpracownika;
            ViewBag.Idb = idbrygadzisty;

            return View();

        }

        //dzien - projekt - od - do - przestoje - dojazd - dieta - 100% - 150% - 200% - nocne
    }
}