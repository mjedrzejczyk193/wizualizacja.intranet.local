﻿using System;
using System.Web.Optimization;

namespace dekadowki.poburski.pl
{
    public class BundleConfig
    {
        public BundleConfig()
        {
        }

        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add((new ScriptBundle("~/bundles/jquery")).Include(new string[] { "~/Scripts/jquery-{version}.js", "~/Scripts/HoldOn.js", "~/Scripts/signature_pad.min.js" }));
            bundles.Add((new ScriptBundle("~/bundles/jqueryval")).Include("~/Scripts/jquery.validate*", new IItemTransform[0]));
            bundles.Add((new ScriptBundle("~/bundles/modernizr")).Include("~/Scripts/modernizr-*", new IItemTransform[0]));
            bundles.Add((new ScriptBundle("~/bundles/bootstrap")).Include(new string[] { "~/Scripts/bootstrap.js", "~/Scripts/respond.js", "~/Scripts/moment.js", "~/Scripts/bootstrap-datetimepicker.js" }));
            bundles.Add((new StyleBundle("~/Content/css")).Include(new string[] { "~/Content/bootstrap.css", "~/Content/site.css", "~/Content/snackbar.css", "~/Content/bootstrap-datetimepicker.css", "~/Content/HoldOn.css" }));
            bundles.Add((new StyleBundle("~/Content/datepicker")).Include(new string[] { "~/datepicker/css/bootstrap-datepicker3.min.css" }));
            bundles.Add((new ScriptBundle("~/bundles/datepicker")).Include(new string[] { "~/datepicker/js/bootstrap-datepicker.min.js", "~/datepicker/locales/bootstrap-datepicker.pl.min.js" }));
        }
    }
}