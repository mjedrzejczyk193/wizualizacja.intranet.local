﻿using System.Web;
using System.Web.Mvc;

namespace dekadowki.poburski.pl
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
